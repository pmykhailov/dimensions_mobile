package {
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.ProgressEvent;
    import flash.text.TextField;

    /**
     * Preloader class.
     * User: Paul Makarenko
     * Date: 04.11.13
     */
    [SWF(backgroundColor="0xCCCCCC", width="640", height="480", frameRate="30")]
    public class Preloader extends Sprite {

        [Embed(source="dimensions.swf")]
        private var assetClass:Class;


        private var tf:TextField;

         public function Preloader() {
            super();

            tf = new TextField();

            addChild(tf);

            loaderInfo.addEventListener(ProgressEvent.PROGRESS, onLoadingProgressHandler);
            loaderInfo.addEventListener(Event.COMPLETE, onLoadingCompleteHandler);
        }


        private function onLoadingProgressHandler(event: ProgressEvent): void {
            tf.text = int(event.bytesLoaded / event.bytesTotal) + "%";
        }


        private function onLoadingCompleteHandler(event: Event): void {
            var asset:MovieClip = new assetClass();
            addChild(asset);
        }

    }
}