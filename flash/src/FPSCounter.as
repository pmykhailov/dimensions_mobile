package{
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.utils.getTimer;

import flashx.textLayout.formats.TextAlign;

public class FPSCounter extends Sprite{
    private var last:uint = getTimer();
    private var ticks:uint = 0;
    private var tf:TextField;

    private var _chartData:Array = [];
    private var _maxRectAmount:int = 50;
    private var _maxRectHeight:int =  100;
    private var _rectWidth:int = 3;
    private var _targetFPS:int = 30;
    private var _chart:Sprite;
    private var _chartBitmap:Bitmap;
    private var _chartBitmapData:BitmapData;

    private var _frameRates:Array;

    private static var _instance:FPSCounter;
    
    public function FPSCounter() {

    }

    public static function get instance():FPSCounter {
        if (!_instance) {
            _instance = new FPSCounter();
        }

        return _instance;
    }

    public function startMonitor():void {
        _frameRates = [];
        addEventListener(Event.ENTER_FRAME, tick0);
    }

    public function stopMonitor():void {
        removeEventListener(Event.ENTER_FRAME, tick0);
    }

    public function get averengeMonitoredFPS():Number {
        var res:Number = 0;

        for (var i:int = 0; i < _frameRates.length; i++) {
            res += _frameRates[i];
        }

        return res / _frameRates.length;
    }

    public function get minMonitoredFPS():Number {
        var res:Number = _frameRates[1];

        for (var i:int = 0; i < _frameRates.length; i++) {
            if (_frameRates[i] < res) {
                res = _frameRates[i];
            }
        }

        return res;
    }

    public function get isLowPerfomanceDevice():Boolean {
        return true;
       // return minMonitoredFPS < 10;
        //return averengeMonitoredFPS < 21;
    }

    private function tick0(evt:Event):void {
        ticks++;
        var now:uint = getTimer();
        var delta:uint = now - last;
        if (delta >= 1000) {
            var fps:Number = ticks / delta * 1000;
            ticks = 0;
            last = now;

            _frameRates.push(fps);
        }
    }

    private function init(xPos:int=0, yPos:int=0, color:uint=0xFFFFFF, fillBackground:Boolean=true, backgroundColor:uint=0xFF0000):void {
        var format:TextFormat = new TextFormat();
        format.size = 20;
        format.align = TextAlign.CENTER;

        x = xPos;
        y = yPos;
        tf = new TextField();
        tf.defaultTextFormat = format;
        tf.textColor = color;
        tf.text = "----- fps";
        tf.selectable = false;
        tf.background = fillBackground;
        tf.backgroundColor = backgroundColor;
        //tf.autoSize = TextFieldAutoSize.LEFT;
        tf.width = _rectWidth*_maxRectAmount;
        tf.height = 25;

        addChild(tf);
        //width = tf.textWidth;
        //height = tf.textHeight;
        addEventListener(Event.ENTER_FRAME, tick);
        //addEventListener(Event.ENTER_FRAME, tick2);

        _chart = new Sprite();

        _chartBitmap = new Bitmap();
        _chartBitmapData = new BitmapData(_rectWidth*_maxRectAmount, _maxRectHeight);
        _chartBitmap.bitmapData = _chartBitmapData;
        _chartBitmap.y = tf.height;

        addChild(_chartBitmap);
    }


    private function tick(evt:Event):void {
        ticks++;
        var now:uint = getTimer();
        var delta:uint = now - last;
        if (delta >= 1000) {
            //trace(ticks / delta * 1000+" ticks:"+ticks+" delta:"+delta);
            var fps:Number = ticks / delta * 1000;
            tf.text = fps.toFixed(1) + " fps";
            ticks = 0;
            last = now;

            addData(fps);
        }
    }

    private function tick2(evt:Event):void {

        var now:uint = getTimer();
        var delta:uint = now - last;
        var fps:Number = 1000 / delta;

        tf.text = fps.toFixed(1) + " fps";

        last = now;

        addData(fps);
    }

    private function addData(fps:Number):void {
        _chartData.unshift(fps);
        if (_chartData.length > _maxRectAmount) {
            _chartData.length = _maxRectAmount;
        }
        drawChart();
    }

    private function drawChart():void {

        _chart.graphics.clear();
        _chart.graphics.lineStyle(0,0x000000,0);
        _chart.graphics.beginFill(0xFFFFFF,1);
        _chart.graphics.drawRect(0,0,_rectWidth*_maxRectAmount, _maxRectHeight);
        _chart.graphics.endFill();

        for (var i=0; i<_chartData.length; i++) {
            _chart.graphics.lineStyle(1,0x000000);
            _chart.graphics.beginFill(0xFF0000);
            _chart.graphics.drawRect(i*_rectWidth,_maxRectHeight - _chartData[i],_rectWidth,_chartData[i]);
            _chart.graphics.endFill();
        }

        _chart.graphics.lineStyle(1,0x000000);
        _chart.graphics.moveTo(0,_maxRectHeight - _targetFPS);
        _chart.graphics.lineTo(_maxRectAmount*_rectWidth,_maxRectHeight - _targetFPS);


        _chartBitmapData.fillRect(new Rectangle(0,0,_chartBitmapData.width,_chartBitmapData.height), 0xFFFFFF);
        _chartBitmapData.draw(_chart);
    }

}
}