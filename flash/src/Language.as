/**
 * Created by Pasha on 29.07.2016.
 */
package {
public class Language {
    public static const SCREEN_LEADERBOARD_CAPTION:String = "Leaderboard";
    public static const SCREEN_SETTINGS_CAPTION:String = "Settings";
    public static const SCREEN_GAME_RULES_CAPTION:String = "Instructions";

    public static const LEADERBOARD_LOADING_PROGRESS:String = "LOADING ...";
    public static const LEADERBOARD_LOADING_FAILED:String = "INTERNET CONNECTION \n IS MISSING";

    public static const CONGRATULATIONS:String = "Congratulations! <br/> New high score";
    public static const GAME_PAUSED:String = "GAME PAUSED";
}
}
