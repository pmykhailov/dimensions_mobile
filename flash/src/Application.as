package {
    import by.lord_xaoca.extensions.VirtualViewMapExtension;
    import by.lord_xaoca.helpers.StageReference;

import com.ad.AdMobManager;

import com.app.ApplicationConfig;

import com.game.GameConfig;
    import com.game_wrapper.GameWrapperConfig;
    import com.greensock.TweenLite;
import com.greensock.plugins.ColorMatrixFilterPlugin;
import com.greensock.plugins.EndArrayPlugin;
import com.greensock.plugins.GlowFilterPlugin;
    import com.greensock.plugins.TweenPlugin;
    import com.screens.ScreensConfig;

    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;

    import robotlegs.bender.bundles.mvcs.MVCSBundle;
    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.impl.Context;

    [SWF(backgroundColor="0xCCCCCC", width="720", height="1280", frameRate="30")]
    public class Application extends Sprite {

        private var _context: IContext;


        public function Application() {
            super();

            addEventListener(Event.ADDED_TO_STAGE, onAddedToStageHandler);
        }


        private function onAddedToStageHandler(event: Event): void {
            removeEventListener(Event.ADDED_TO_STAGE, onAddedToStageHandler);
            StageReference.init(stage);

            TweenPlugin.activate([GlowFilterPlugin,ColorMatrixFilterPlugin, EndArrayPlugin]);

            stage.align = StageAlign.TOP_LEFT;
            stage.scaleMode = StageScaleMode.EXACT_FIT;
            
            initAdMob();
            
            _context = new Context()
                    .install(MVCSBundle, VirtualViewMapExtension)
                    .configure(ApplicationConfig, ScreensConfig, GameWrapperConfig, GameConfig)
                    .configure(new ContextView(this));

        }

        private function initAdMob():void
        {
            AdMobManager.instance.init();
        }

    }
}
