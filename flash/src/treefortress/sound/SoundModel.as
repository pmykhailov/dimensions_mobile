﻿package treefortress.sound {

    public class SoundModel {

        public static const GROUP_MUSIC: String = "group_music";
        public static const GROUP_SFX: String = "group_sfx";

        public static const AMBIENCE:String = "Ambience";

        public static const BUTTON_CLICK:String = "FLA_button_click";
        public static const READY_SOUND:String = "ReadySound";
        public static const GO_SOUND:String = "GoSound";
        public static const MULTIPLIER_ACTIVATED:String = "MultiplierActivated";
        public static const MULTIPLIER_DEACTIVATED:String = "MultiplierDeactivated";

        public static const ITEMS_MATCHED:String = "FLA_grid_items_matched";
        public static const ITEM_FALL:String = "FLA_item_fall_sound";
        public static const ITEM_SELECT:String = "FLA_grid_item_select";
        public static const ITEM_UNSELECT:String = "FLA_grid_item_unselect";
        public static const TICK_SOUND:String = "TickSound";;
        public static const LEVEL_COMPLETESOUND:String = "LevelCompleteSound";

        public static const ALL_FX:Array = [BUTTON_CLICK, READY_SOUND, GO_SOUND, ITEMS_MATCHED, ITEM_FALL, ITEM_SELECT, ITEM_UNSELECT, TICK_SOUND, LEVEL_COMPLETESOUND, MULTIPLIER_ACTIVATED, MULTIPLIER_DEACTIVATED];
    }
}