package by.lord_xaoca.extensions {
    import by.lord_xaoca.events.ComponentEvent;
    import by.lord_xaoca.views.base.BaseEventDispatcher;

    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;

    /**
     * VirtualViewMap class.
     *
     * @author: Ivan Shaban
     * @date: 30.05.13 21:38
     */

    public class VirtualViewMap extends BaseEventDispatcher {

        [Inject]
        public var mediatorMap: IMediatorMap;


        [PostConstruct]
        public function init(): void {
            addStageListener(ComponentEvent.INITIALIZE, onComponentInitHandler);
            addStageListener(ComponentEvent.DESTROY, onComponentDestroyHandler);
        }


        private function onComponentInitHandler(event: ComponentEvent): void {
            mediatorMap.mediate(event.data);
        }


        private function onComponentDestroyHandler(event: ComponentEvent): void {
            mediatorMap.unmediate(event.data);
        }
    }
}
