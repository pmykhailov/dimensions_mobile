package by.lord_xaoca.extensions {
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.api.IExtension;

    /**
     * VirtualViewMapExtension class.
     *
     * @author: Ivan Shaban
     * @date: 30.05.13 21:29
     */

    public class VirtualViewMapExtension implements IExtension {
        [Inject]
        public var mediatorMap: IMediatorMap;


        public function extend(context: IContext): void {
            context.injector.map(VirtualViewMap).asSingleton();
            context.injector.getInstance(VirtualViewMap);
        }
    }
}
