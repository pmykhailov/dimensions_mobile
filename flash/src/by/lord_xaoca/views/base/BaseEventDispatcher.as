package by.lord_xaoca.views.base {

    import by.lord_xaoca.events.ComponentEvent;
    import by.lord_xaoca.helpers.ListenerHandler;
    import by.lord_xaoca.helpers.StageReference;
    import by.lord_xaoca.interfaces.IDestructableComponent;

    import flash.events.EventDispatcher;

    /**
     * Simple class to easy event handling.
     *
     * @author: Ivan Shaban
     */

    public class BaseEventDispatcher extends EventDispatcher implements IDestructableComponent {

        protected var _eventHandler: ListenerHandler;


        public function BaseEventDispatcher() {
            initialize();
        }


        override public function addEventListener(type: String, listener: Function, useCapture: Boolean = false, priority: int = 0, useWeakReference: Boolean = false): void {
            _eventHandler.addListenerTo(this, type, listener, false);
            super.addEventListener(type, listener, useCapture, priority, useWeakReference);
        }


        override public function removeEventListener(type: String, listener: Function, useCapture: Boolean = false): void {
            _eventHandler.removeListener(type, listener, false);
            super.removeEventListener(type, listener, useCapture);
        }


        public function addStageListener(type: String, listener: Function, useCapture: Boolean = false, priority: int = 0): void {
            _eventHandler.addListenerTo(StageReference.stage, type, listener, true, useCapture, priority);
        }


        public function removeStageListener(type: String, listener: Function): void {
            _eventHandler.removeListenerFrom(StageReference.stage, type, listener);
        }


        public function removeAllListeners(): void {
            _eventHandler.removeAllListeners();
        }


        public function destroy(): void {
            dispatchEvent(new ComponentEvent(ComponentEvent.DESTROY));

            removeAllListeners();
            _eventHandler = null;
        }


        /**
         * Override this method if you want to add any logic on element start up.
         */
        protected function initialize(): void {
            initEventHandler();
        }


        protected function initEventHandler(): void {
            _eventHandler = new ListenerHandler(this);
        }

    }
}
