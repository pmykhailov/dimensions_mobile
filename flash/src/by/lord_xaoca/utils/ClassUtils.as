package by.lord_xaoca.utils {

    import flash.system.ApplicationDomain;
    import flash.utils.getQualifiedClassName;

    /**
     * ClassUtils class.
     *
     * @author: ishaban
     * @date: 03.02.12 22:54
     */

    public class ClassUtils {

        public static function getClassByName (className:String):Class {
            return ApplicationDomain.currentDomain.hasDefinition(className) ? ApplicationDomain.currentDomain.getDefinition(className) as Class : null;
        }

        public static function getClassByInstance (instance:Object):Class {
            return getClassByName(getQualifiedClassName(instance));
        }

        public static function getInstanceByClass (classValue:Class, ...arguments):* {
            if (arguments.length > 10) {
                throw new Error('You have passed more arguments than the "construct" method accepts (accepts ten or less).');
            }
            //			trace("arguments", arguments.unshift() as Array, arguments.length);

            switch (arguments.length) {
                case 0 :
                    return new classValue();
                case 1 :
                    return new classValue(arguments[0]);
                case 2 :
                    return new classValue(arguments[0], arguments[1]);
                case 3 :
                    return new classValue(arguments[0], arguments[1], arguments[2]);
                case 4 :
                    return new classValue(arguments[0], arguments[1], arguments[2], arguments[3]);
                case 5 :
                    return new classValue(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
                case 6 :
                    return new classValue(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                case 7 :
                    return new classValue(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]);
                case 8 :
                    return new classValue(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7]);
                case 9 :
                    return new classValue(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], arguments[8]);
                case 10 :
                    return new classValue(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], arguments[8], arguments[9]);
            }
        }

        /**
         Dynamically constructs a Class.

         @param classValue: The Class to create.
         @param arguments: Up to ten arguments to the constructor.
         @return Returns the dynamically created instance of the Class specified by <code>classValue</code> parameter.
         @throws Error if you pass more arguments than this method accepts (accepts ten or less).
         @example
         <code>
         var bData:* = ClassUtil.construct("BitmapData", 200, 200);

         trace(bData is BitmapData, bData.width);
         </code>
         */
        public static function getInstanceByClassName (className:String, ...arguments):* {
            var classValue:Class = getClassByName(className);
            if (!classValue) {
                return null;
            }
            else {
                arguments.unshift(classValue);
                return ClassUtils.getInstanceByClass.apply(null, arguments);
            }
        }

    }
}
