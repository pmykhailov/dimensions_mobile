﻿package by.lord_xaoca.utils {

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.Graphics;
    import flash.display.PixelSnapping;
    import flash.display.Sprite;
    import flash.geom.Matrix;
    import flash.geom.Rectangle;

    public class Painter {
        public static function drawRoundedRect (width:Number, height:Number, bevel:Number = 0, color:uint = 0x000000, alpha:Number = 1):Sprite {
            var mc:Sprite = new Sprite();
            var g:Graphics = mc.graphics;
            g.beginFill(color, alpha);
            g.moveTo(width - bevel, height);
            g.curveTo(width, height, width, height - bevel);
            g.lineTo(width, bevel);
            g.curveTo(width, 0, width - bevel, 0);
            g.lineTo(bevel, 0);
            g.curveTo(0, 0, 0, bevel);
            g.lineTo(0, height - bevel);
            g.curveTo(0, height, bevel, height);
            g.lineTo(width - bevel, height);
            g.endFill();

            return mc;
        }

        public static function drawRoundedRectWithBorder (width:Number, height:Number, bevel:Number = 0, thickness:int = 2, rectColor:uint = 0x000000, lineColor:uint = 0xFFFFFF, rectAlpha:Number = 1, lineAlpha:Number = 1):Sprite {
            var mc:Sprite = new Sprite();
            var g:Graphics = mc.graphics;
            g.lineStyle(thickness, lineColor, lineAlpha, true);
            g.beginFill(rectColor, rectAlpha);
            g.moveTo(width - bevel, height);
            g.curveTo(width, height, width, height - bevel);
            g.lineTo(width, bevel);
            g.curveTo(width, 0, width - bevel, 0);
            g.lineTo(bevel, 0);
            g.curveTo(0, 0, 0, bevel);
            g.lineTo(0, height - bevel);
            g.curveTo(0, height, bevel, height);
            g.lineTo(width - bevel, height);
            g.endFill();
            return mc;
        }

        public static function drawAGradientRect (width:Number, height:Number, angle:Number = 90, colors:Array = null, alphas:Array = null, ratios:Array = null, matrix:Matrix = null, fillType:String = 'linear', spreadMethod:String = "pad", interpolationMethod:String = "rgb", focalPointRatio:Number = 0):Sprite {
            if (fillType != 'linear' && fillType != 'radial') {
                fillType = 'linear';
            }
            if (colors == null) {
                colors = [0x000000, 0x000000];
            }
            if (alphas == null) {
                alphas = [0, 1];
            }
            if (ratios == null) {
                ratios = [50, 255];
            }
            if (matrix == null) {
                matrix = new Matrix();
                matrix.createGradientBox(width, height, (angle / 180) * Math.PI);
            }

            var mc:Sprite = new Sprite();
            var g:Graphics = mc.graphics;
            g.beginGradientFill(fillType, colors, alphas, ratios, matrix, spreadMethod, interpolationMethod, focalPointRatio);
            g.drawRect(0, 0, width, height);
            g.endFill();
            return mc;
        }

        public static function drawARoundedGradientRect (width:Number, height:Number, angle:Number = 90, bevel:Number = 0, colors:Array = null, alphas:Array = null, ratios:Array = null, matrix:Matrix = null, fillType:String = 'linear', spreadMethod:String = "pad", interpolationMethod:String = "rgb", focalPointRatio:Number = 0):Sprite {
            if (fillType != 'linear' && fillType != 'radial') {
                fillType = 'linear';
            }
            if (colors == null) {
                colors = [0x000000, 0x000000];
            }
            if (alphas == null) {
                alphas = [0, 1];
            }
            if (ratios == null) {
                ratios = [50, 255];
            }
            if (matrix == null) {
                matrix = new Matrix();
                matrix.createGradientBox(width, height, (angle / 180) * Math.PI);
            }

            var mc:Sprite = new Sprite();
            var g:Graphics = mc.graphics;
            g.beginGradientFill(fillType, colors, alphas, ratios, matrix, spreadMethod, interpolationMethod, focalPointRatio);
            g.drawRoundRect(0, 0, width, height, bevel, bevel);
            g.endFill();
            return mc;
        }

        public static function drawLinedRect (width:Number, height:Number, bevel:int = 0, thickness:int = 1, color:uint = 0x000000, alpha:Number = 1):Sprite {
            var mc:Sprite = new Sprite();
            var g:Graphics = mc.graphics;
            g.lineStyle(thickness, color, alpha, true);
            g.moveTo(width - bevel, height);
            g.curveTo(width, height, width, height - bevel);
            g.lineTo(width, bevel);
            g.curveTo(width, 0, width - bevel, 0);
            g.lineTo(bevel, 0);
            g.curveTo(0, 0, 0, bevel);
            g.lineTo(0, height - bevel);
            g.curveTo(0, height, bevel, height);
            g.lineTo(width - bevel, height);
            g.endFill();

            return mc;
        }

        /**
         *
         * @param target
         * @param height % of original height
         * @param transparent
         * @param fillColor
         * @return
         */
        public static function drawReflection (target:DisplayObject, height:Number = 0.3, transparent:Boolean = false, fillColor:uint = 0xFFFFFF):Sprite {
            var targetW:Number;
            var targetH:Number;

            if (height != 0) {
                targetW = target.width;
                targetH = target.height * height;
            } else {
                targetW = target.width;
                targetH = target.height;
            }
            var mask_mc:Sprite = Painter.drawAGradientRect(targetW, targetH, 90, null, [0, 0.8], [30, 255]);
            mask_mc.cacheAsBitmap = true;
            mask_mc.y = target.height - targetH;

            var temp_bitmap:BitmapData = new BitmapData(target.width, target.height, transparent, fillColor);
            temp_bitmap.draw(target);

            var temp_bmd:Bitmap = new Bitmap(temp_bitmap, 'auto', true);
            temp_bmd.cacheAsBitmap = true;
            temp_bmd.mask = mask_mc;

            var temp_mc:Sprite = new Sprite();
            temp_mc.addChild(temp_bmd);
            temp_mc.addChild(mask_mc);
            temp_mc.alpha = 0.7;
            temp_mc.scaleY = -1;
            temp_mc.y = temp_mc.height;

            var tmp_mc:Sprite = new Sprite();
            tmp_mc.addChild(temp_mc);

            var new_bitmap:BitmapData = new BitmapData(targetW, targetH, transparent, fillColor);
            new_bitmap.draw(tmp_mc);

            var mc:Sprite = new Sprite();
            mc.addChild(new Bitmap(new_bitmap, 'auto', true));

            return mc;
        }

        public static function drawADoubleLine (width:Number, yPos:Number = 0):Sprite {
            var type:String = 'linear';
            var wcolors:Array = [0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF];
            var bcolors:Array = [0x535353, 0x535353, 0x535353, 0x535353];
            var alphas:Array = [0, .9, .9, 0];// .9];
            var ratios:Array = [0, 50, 205, 255];// 255];
            var dMatrix:Matrix = new Matrix();
            dMatrix.createGradientBox(width, 1);

            var line:Sprite = new Sprite();
            var g:Graphics = line.graphics;
            g.lineStyle(1, 0);
            g.lineGradientStyle(type, wcolors, alphas, ratios, dMatrix);
            g.moveTo(0, 1);
            g.lineTo(width, 1);

            g.lineStyle(1, 0);
            g.lineGradientStyle(type, bcolors, alphas, ratios, dMatrix);
            g.moveTo(0, 0);
            g.lineTo(width, 0);

            line.y = yPos;
            return line;
        }

        public static function rasterize (value:DisplayObject, replaceOriginal:Boolean = false, scaleX:Number = 1, scaleY:Number = 1):Bitmap {
            var bounds:Rectangle = value.getBounds(value);
            var m:Matrix = new Matrix();
            m.createBox(scaleX * value.scaleX, scaleY * value.scaleY, 0, -bounds.x, -bounds.y);

            var bitmapData:BitmapData = new BitmapData(value.width * scaleX, value.height * scaleY, true, 0x000000);
            bitmapData.draw(value, m);

            var bitmap:Bitmap = new Bitmap(bitmapData, PixelSnapping.AUTO, true);
            if (replaceOriginal) {
                bitmap.x = value.x;
                bitmap.y = value.y;
                bitmap.name = value.name;
                bitmap.filters = value.filters;

                var parent:DisplayObjectContainer = value.parent;
                if (parent) {
                    parent.addChildAt(bitmap, parent.getChildIndex(value));
                    parent.removeChild(value);
                }
            }
            return bitmap;
        }
    }

}