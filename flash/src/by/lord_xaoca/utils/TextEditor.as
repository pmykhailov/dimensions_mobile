﻿package by.lord_xaoca.utils {

    import flash.text.TextField;
    import flash.text.TextFormat;

    /**
     * ...
     * @author Ivan
     */
    public class TextEditor {
        //Font.registerFont(Trebuchet);														// указываем какой шрифт юзаем
        //public var embeddedFonts:Array = Font.enumerateFonts(false);
        private static var _embedFonts:Boolean = false;

        /**
         * Создаем тектовое поле.
         * @param    text Текст который будет отображен.
         * @param    color Цвет текста.
         * @param    size Размер текста.
         * @param    $auto Выравнивание текста.
         * @param    fontName Шрифт который используется.
         * @param    selectable Можно ли выделять текст и активен ли он для действий мыши.
         * @return Возвращает текстовое поле.
         */
        public static function getNewTF (text:String, color:uint = 0xFFFFFF, size:int = 12, autoSize:String = 'left', fontName:String = "Trebuchet MS", selectable:Boolean = false):TextField {
            var tempFormat:TextFormat = new TextFormat();
            tempFormat.font = fontName;// "Arial";// "Trebuchet MS";
            tempFormat.color = color;
            tempFormat.size = size;

            var tf:TextField = new TextField();
            tf.autoSize = autoSize;
            tf.selectable = selectable;
            tf.mouseEnabled = selectable;
            tf.embedFonts = _embedFonts;
            tf.defaultTextFormat = tempFormat;
            tf.text = text;
            return tf;
        }

        /**
         * Всегда ли при создании текстовоего поля используются встроенные шрифты.
         */
        public static function get embedFonts ():Boolean {
            return _embedFonts;
        }

        /**
         * Всегда ли при создании текстовоего поля используются встроенные шрифты.
         */
        public static function set embedFonts (value:Boolean):void {
            _embedFonts = value;
        }

        /**
         * Меняем какое то св-во текстового поля (в основном это св-ва текстформата).
         * @param    textField Текстовое поле к которому будут применены действия.
         * @param    propertyName Название св-ва которое надо изменить.
         * @param    value Новое значение св-ва.
         * @param    $defaultTextFormat Применить это изменении для текста в этом поле вообще или только для текущего содержимого (например при раскраске может потребоваться изменить цвет только для текущей области, а потом текст должен быть изначального цвета).
         */
        public static function changeTFProperty (textField:TextField, propertyName:String, value:*, useDefaultTextFormat:Boolean = false):void {
            var format:TextFormat = useDefaultTextFormat ? textField.defaultTextFormat : textField.getTextFormat();
            format[propertyName] = value;
            if (useDefaultTextFormat) {
                textField.defaultTextFormat = format;
            }
            else {
                textField.setTextFormat(format);
            }
        }

        /**
         * Клонируем какой то текстовое поле.
         * @param    value Текстовое поле которое надо клонировать.
         * @param    removeOriginal Требуется ли удалить оригинал из дисплей листа.
         * @return    Новое текстовое поле, с натсройками старого, листенеры не копируются.
         */
        public static function clone (value:TextField, removeOriginal:Boolean = true):TextField {
            var format:TextFormat = value.defaultTextFormat;
            var cloneFormat:TextFormat = new TextFormat(format.font, format.size, format.color, format.bold, format.italic, format.underline, format.url, format.target, format.align, format.leftMargin, format.rightMargin, format.indent, format.leading);
            var clone:TextField = new TextField();
            clone.defaultTextFormat = cloneFormat;
            clone.antiAliasType = value.antiAliasType;
            clone.autoSize = value.autoSize;
            clone.embedFonts = value.embedFonts;
            clone.multiline = value.multiline;
            clone.background = value.background;
            clone.backgroundColor = value.backgroundColor;
            clone.border = value.border;
            clone.borderColor = value.borderColor;
            clone.selectable = value.selectable;
            clone.text = value.text;
            clone.x = value.x;
            clone.y = value.y;
            clone.width = value.width;
            clone.height = value.height;
            clone.alpha = value.alpha;
            if (removeOriginal && value.parent) {
                value.parent.addChildAt(clone, value.parent.getChildIndex(value));
                value.parent.removeChild(value);
            }
            return clone;
        }

        /**
         * Заменяем один кусок строки на другой без учета регистра.
         * var str:String = " qwerTYqwerty Qwerty qWerty qwErty ...";
         var pattern:String = "qwerty";
         trace(recReplaceSrt(str, pattern, "11"));
         * @param    string Строка в которой будет производиться поиск и замена.
         * @param    pattern Значение которое ищем и заменяем.
         * @param    newValue Значение на которое будет замененно искомое.
         * @return Новая строка с учетом изменений.
         */
        public static function recReplaceStr (string:String, pattern:String, newValue:String):String {
            var sIndex:int = string.toLowerCase().indexOf(pattern.toLowerCase());
            if (sIndex == -1) {
                return string;
            }
            var eIndex:int = sIndex + pattern.length;
            var result:String = string.substring(0, sIndex) + newValue + recReplaceStr(string.substring(eIndex, string.length), pattern, newValue);
            return result;
        }

        /**
         * Раскрашиваем подстроку в другой цвет, только для первого значения. Пример использования:
         * var tf:TextField = new TextField();
         tf.htmlText = paintHTML_Rec("wrtegcetgvet", "t");
         addChild(tf);
         * @param    value Строка в которой будет производиться поиск.
         * @param    pattern Значение которое будет выделено другим цветом.
         * @param    color Значение цвета которым будет подсвечены слова.
         * @return Строка с хтмл-тегами.
         */
        public static function paintHTML (value:String, pattern:String, color:String = "00CCFF"):String // красим только первый символ
        {
            var sIndex:int = value.toLowerCase().indexOf(pattern.toLowerCase());
            if (sIndex == -1) {
                return value;
            }
            var eIndex:int = sIndex + pattern.length;
            var result:String = value.substring(0, sIndex) + "<font color=\"#" + color + "\">" + value.substring(sIndex, eIndex) + "</font>" + value.substring(eIndex, value.length - 1);
            return result;
        }

        /**
         * Раскрашиваем подстроку в другой цвет, для ВСЕХ вхождений подстроки. Пример использования:
         * var tf:TextField = new TextField();
         tf.htmlText = paintHTML_Rec("wrtegcetgvet", "t");
         addChild(tf);
         * @param    value Строка в которой будет производиться поиск.
         * @param    pattern Значение которое будет выделено другим цветом.
         * @param    color Значение цвета которым будет подсвечены слова.
         * @return Строка с хтмл-тегами.
         */
        public static function paintHTML_Rec (value:String, pattern:String, color:String = "00CCFF"):String // красим только первый символ
        {
            var sIndex:int = value.toLowerCase().indexOf(pattern.toLowerCase());
            if (sIndex == -1) {
                return value;
            }
            var eIndex:int = sIndex + pattern.length;
            var result:String = value.substring(0, sIndex) + "<font color=\"#" + color + "\">" + value.substring(sIndex, eIndex) + "</font>" + paintHTML_Rec(value.substring(eIndex, value.length), pattern);
            return result;
        }
    }

}