package by.lord_xaoca.robotlegs2 {

    import flash.events.Event;
    import flash.events.IEventDispatcher;

    /**
     * BaseActor class.
     *
     * @author: Ivan Shaban
     * @date: 29.05.13 22:33
     */

    public class BaseActor {
        [Inject]
        public var dispatcher: IEventDispatcher;


        protected function dispatch(event: Event): void {
            if (dispatcher && dispatcher.hasEventListener(event.type)) {
                dispatcher.dispatchEvent(event);
            }
        }
    }
}
