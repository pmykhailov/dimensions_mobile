package by.lord_xaoca.robotlegs2 {

    import by.lord_xaoca.events.ComponentEvent;
    import by.lord_xaoca.helpers.StageReference;
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import flash.display.DisplayObject;

    /**
     * BaseView class. Self-registered class that contains all view logic.
     *
     * @author: Ivan Shaban
     * @date: 06.04.13 15:24
     */

    public class BaseView extends BaseDisplayObjectContainer {

        public function BaseView(view: DisplayObject) {
            super(view);

            registerMediator();
        }


        override public function destroy(): void {
            unregisterMediator();
            super.destroy();
        }


        protected function registerMediator(): void {
            StageReference.stage.dispatchEvent(new ComponentEvent(ComponentEvent.INITIALIZE, this));
        }


        protected function unregisterMediator(): void {
            StageReference.stage.dispatchEvent(new ComponentEvent(ComponentEvent.DESTROY, this));
        }

    }
}
