package by.lord_xaoca.ui.ir {

    import by.lord_xaoca.interfaces.IItemRenderer;
    import by.lord_xaoca.ui.BaseUIElement;

    import flash.display.Sprite;

    /**
     * BaseItemRenderer class.
     *
     * @author: Ivan Shaban
     * @date: 18.01.12 18:33
     */

    public class BaseItemRenderer extends BaseUIElement implements IItemRenderer {

        protected var _selected: Boolean;
        protected var _id: int;


        public function BaseItemRenderer(view: Sprite) {
            super(view);

            isButtonBehaviour = true;
        }


        public function get selected(): Boolean {
            return _selected;
        }


        public function set selected(value: Boolean): void {
            _selected = value;
        }


        public function get id(): int {
            return _id;
        }


        public function set id(value: int): void {
            _id = value;
        }


        override protected function initialize(): void {
            super.initialize();

            initButtons();
            initLabels();
        }


        protected function initLabels(): void {
            // ABSTRACT
        }


        protected function initButtons(): void {
            // ABSTRACT
        }

    }
}
