package by.lord_xaoca.ui.ir {

    import flash.display.MovieClip;
    import flash.display.Sprite;

    /**
     * RadioGroupItemRenderer class.
     *
     * @author Ivan Shaban
     * @date 16.01.2012 14:49
     */
    public class RadioGroupItemRenderer extends BaseItemRenderer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _bg:MovieClip;
        protected var _labelTF:TLFTextField;
        protected var _selector:Sprite;
        protected var _selectorBG:Sprite;
        protected var _data:Object;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function RadioGroupItemRenderer () {
            super(new View_RadioItemRenderer());
        }

        // ------------------ PROPERTIES -------------------------------

        /* INTERFACE com.popover.library.interfaces.IItemRenderer */

        override public function get selected ():Boolean {
            return _selector.visible;
        }

        override public function set selected (value:Boolean):void {
            _selector.visible = value;
        }

        override public function get data ():Object {
            return _data;
        }

        override public function set data (value:Object):void {
            _data = value;
            _labelTF.text = value.title;
            updatePlacement();
        }

        // ------------------ PUBLIC METHODS ---------------------------

        override public function updatePlacement ():void {
            if (Globals.useMirroring) {
                _labelTF.x = 0;
                _selectorBG.x = _labelTF.x + _labelTF.width + 1;
                _selector.x = _selectorBG.x + int((_selectorBG.width - _selector.width) / 2);
                _bg.width = _selectorBG.x + _selectorBG.width;
            } else {
                _selectorBG.x = 0
                _selector.x = _selectorBG.x + int((_selectorBG.width - _selector.width) / 2);
                _labelTF.x = _selectorBG.x + _selectorBG.width + 1;
                _bg.width = _labelTF.x + _labelTF.width;
            }
        }

        // ------------------ PROTECTED METHODS ------------------------

        override protected function _initView (view:Sprite):void {
            super._initView(view);

            mouseChildren = false;

            _bg = getMovieClip("bg");
            _bg.alpha = 0;

            _selector = getMovieClip("selected");
            _selector.visible = false;

            _selectorBG = getSprite("simple_btn");
        }

        override protected function initLabels ():void {
            _labelTF = TextUtils.initTLFProperties(getTLFTextField("label"));
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------
    }
}