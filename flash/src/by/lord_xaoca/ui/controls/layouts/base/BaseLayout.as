package by.lord_xaoca.ui.controls.layouts.base {

    import by.lord_xaoca.interfaces.ILayoutElement;
    import by.lord_xaoca.ui.controls.layouts.ILayout;

    import flash.display.DisplayObjectContainer;
    import flash.events.Event;
    import flash.events.EventDispatcher;

    /**
     * BaseLayout class.
     *
     * @author Ivan Shaban
     * @date 31.10.2011 15:24
     */

    [Event(name="resize", type="flash.events.Event")]

    public class BaseLayout extends EventDispatcher implements ILayout {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _container:DisplayObjectContainer;
        protected var _items:Vector.<ILayoutElement>;

        /**
         * Spacing between elements
         */
        protected var _hSpacing:int;
        protected var _vSpacing:int;

        protected var _leftIndent:int;
        protected var _topIndent:int;
        protected var _rightIndent:int;
        protected var _bottomIndent:int;

        protected var _hAlign:String;
        protected var _vAlign:String;

        protected var _direction:String;

        /**
         * Horizontal offset between elements
         */
        protected var _hOffset:int;

        /**
         * Vertical offset between elements
         */
        protected var _vOffset:int;

        /**
         * Start point for counting. For list-based components it can be -1
         */
        protected var _startIndex:int;

        protected var _maxHeightInRow:int;
        protected var _maxWidthInColumn:int;

        /**
         * Indicate should we call "refresh" method after every change or not.
         * Analog "lock" \ "unlock" method of BitmapData class.
         */
        protected var _isBlocked:Boolean;
        protected var _useFixedSpacing:Boolean;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function BaseLayout (direction:String) {
            _direction = direction;
            _items = new Vector.<ILayoutElement>;
        }

        // ------------------ PROPERTIES -------------------------------

        public function get container ():DisplayObjectContainer {
            return _container;
        }

        public function set container (value:DisplayObjectContainer):void {
            _container = value;
            refresh();
        }

        public function get hSpacing ():int {
            return _hSpacing;
        }

        public function set hSpacing (value:int):void {
            _hSpacing = value;
            refresh();
        }

        public function get vSpacing ():int {
            return _vSpacing;
        }

        public function set vSpacing (value:int):void {
            _vSpacing = value;
            refresh();
        }

        public function get leftIndent ():int {
            return _leftIndent;
        }

        public function set leftIndent (value:int):void {
            _leftIndent = value;
            refresh();
        }

        public function get topIndent ():int {
            return _topIndent;
        }

        public function set topIndent (value:int):void {
            _topIndent = value;
            refresh();
        }

        public function get rightIndent ():int {
            return _rightIndent;
        }

        public function set rightIndent (value:int):void {
            _rightIndent = value;
            refresh();
        }

        public function get bottomIndent ():int {
            return _bottomIndent;
        }

        public function set bottomIndent (value:int):void {
            _bottomIndent = value;
            refresh();
        }

        public function get hAlign ():String {
            return _hAlign;
        }

        public function set hAlign (value:String):void {
            _hAlign = value;
            refresh();
        }

        public function get vAlign ():String {
            return _vAlign;
        }

        public function set vAlign (value:String):void {
            _vAlign = value;
            refresh();
        }

        public function get direction ():String {
            return _direction;
        }

        public function set direction (value:String):void {
            _direction = value;
            refresh();
        }

        public function get isBlocked ():Boolean {
            return _isBlocked;
        }

        public function get maxHeightInRow ():int {
            return _maxHeightInRow;
        }

        public function get maxWidthInColumn ():int {
            return _maxWidthInColumn;
        }

        public function get useFixedSpacing ():Boolean {
            return _useFixedSpacing;
        }

        public function set useFixedSpacing (value:Boolean):void {
            _useFixedSpacing = value;
            refresh();
        }

        public function get startIndex ():int {
            return _startIndex;
        }

        public function set startIndex (value:int):void {
            _startIndex = value;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        public function addElement (value:ILayoutElement, doRefresh:Boolean = true):ILayoutElement {
            if (!_container) {
                trace("Setup container at first.");
                return null;
            }

            removeElement(value, false);
            var index:int = _items.push(value) - 1;
            _container.addChild(value.view);

            if (doRefresh) {
                refresh();
            } else {
                placeItem(value, _startIndex + index);
            }

            return value;
        }

        public function removeElement (value:ILayoutElement, doRefresh:Boolean = true):ILayoutElement {
            if (!_container) {
                trace("Setup container at first.");
                return null;
            }

            var index:int = _items.indexOf(value);
            if (index != -1) {
                _items.splice(index, 1);
            }

            if (doRefresh) {
                refresh();
            }

            if (_container.contains(value.view)) {
                _container.removeChild(value.view);
            }

            return value;
        }

        public function refresh ():void {
            if (_isBlocked) {
                return;
            }

            reset();
            var len:int = _items.length;
            for (var i:int = 0; i < len; i++) {
                if (_maxHeightInRow < _items[i].height) {
                    _maxHeightInRow = _items[i].height;
                }

                if (_maxWidthInColumn < _items[i].width) {
                    _maxWidthInColumn = _items[i].width;
                }
            }
            for (i = 0; i < len; i++) {
                placeItem(_items[i], _startIndex + i);
            }
            dispatchEvent(new Event(Event.RESIZE));
        }

        public function reset ():void {
            _vOffset = _topIndent;
            _hOffset = _leftIndent;
            _maxHeightInRow = 0;
            _maxWidthInColumn = 0;
        }

        public function lock ():void {
            _isBlocked = true;
        }

        public function unlock ():void {
            _isBlocked = false;
            refresh();
        }

        public function clear ():void {
            _items.length = 0;
            reset();
        }

        // ------------------ PROTECTED METHODS ------------------------

        protected function placeItem (value:ILayoutElement, index:int):void {
            trace("It is abstract method. Override it!");
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}