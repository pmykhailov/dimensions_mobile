package by.lord_xaoca.ui.controls.layouts.usual {

    import by.lord_xaoca.interfaces.ILayoutElement;
    import by.lord_xaoca.ui.controls.layouts.LayoutDirection;
    import by.lord_xaoca.ui.controls.layouts.base.BaseLayout;

    /**
     * HorizontalLayout class.
     *
     * @author Ivan Shaban
     * @date 31.10.2011 19:33
     */
    public class HorizontalLayout extends BaseLayout {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function HorizontalLayout () {
            super(LayoutDirection.HORIZONTAL);
        }

        // ------------------ PROPERTIES -------------------------------

        override public function set vSpacing (value:int):void {
            trace("Your cannot use \"vSpacing\" in this class.")
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function placeItem (value:ILayoutElement, index:int):void {
            if (_useFixedSpacing) {
                value.x = _leftIndent + index * _hSpacing;
            } else {
                if (index == -1) {
                    value.x = (value.width + _hSpacing) * -1;
                } else {
                    value.x = _hOffset;
                    _hOffset += Math.round(value.width + _hSpacing);
                }
            }
            value.y = _topIndent;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}