package by.lord_xaoca.ui.controls.layouts.movable {

    /**
     * MVerticalLayout class.
     *
     * @author: Ivan Shaban
     * @date: 21.12.11 15:01
     */

    public class MVerticalLayout extends VerticalLayout {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function placeItem (value:ILayoutElement, index:int):void {
            super.placeItem(value, index);
            value.x = Globals.useMirroring ? Math.round(_maxWidthInColumn - value.width) : _hOffset;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
