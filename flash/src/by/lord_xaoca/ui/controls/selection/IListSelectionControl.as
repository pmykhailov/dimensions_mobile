package by.lord_xaoca.ui.controls.selection {

    /**
     * IListSelectionControl interface.
     *
     * @author: Ivan Shaban
     * @date: 10.05.12 17:16
     */
    public interface IListSelectionControl extends ISelectionControl {
        function get firstItemIndex ():int;

        function get isFirstIndexChanged ():Boolean;

        function get size ():uint;

        function set size (value:uint):void;

        /**
         * Refreshing itemrenderer's data, without content scrolling.
         * @param value
         */
        function setPosition (value:uint):void;
    }
}
