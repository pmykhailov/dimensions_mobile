package by.lord_xaoca.ui.controls.selection {

    import by.lord_xaoca.interfaces.IDestructableComponent;
    import by.lord_xaoca.interfaces.IItemRenderer;
    import by.lord_xaoca.ui.controls.containers.base.BaseInteractiveContainer;

    import flash.events.IEventDispatcher;

    /**
     * ISelectionControl interface.
     *
     * @author: Ivan Shaban
     * @date: 27.04.12 16:35
     */
    public interface ISelectionControl extends IEventDispatcher, IDestructableComponent {
        function get isDeselectable ():Boolean;

        function set isDeselectable (value:Boolean):void;

        function get isSelectable ():Boolean;

        function set isSelectable (value:Boolean):void;

        function get selectedItem ():IItemRenderer;

        function get selectedItemID ():int;

        function get selectedItemData ():Object;

        function get selectionEventType ():String;

        function set selectionEventType (value:String):void;

        function get deselectionEventType ():String;

        function set deselectionEventType (value:String):void;

        function clear ():void;

        function setContainer (container:BaseInteractiveContainer):void;

        function setSelectionByIndex (value:int):void;

        function init ():void;

        function addIRListener (type:String, listener:Function):void;
    }
}
