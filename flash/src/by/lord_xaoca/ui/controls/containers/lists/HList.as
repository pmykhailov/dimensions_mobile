package by.lord_xaoca.ui.controls.containers.lists {

    import by.lord_xaoca.ui.controls.layouts.usual.HorizontalLayout;
    import by.lord_xaoca.ui.controls.selection.BaseListSelectionControl;

    import flash.geom.Rectangle;

    /**
     * VList class.
     *
     * @author Ivan Shaban
     * @date 15.10.2011 13:59
     */
    [Event(name="change", type="flash.events.Event")]

    public class HList extends BaseList {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function HList(itemRendererClass: Class, size: int, ...itemRendererArguments) {
            super(new HorizontalLayout(), new BaseListSelectionControl(size), itemRendererClass, itemRendererArguments);
        }


        // ------------------ PROPERTIES -------------------------------

        override public function set height(value: Number): void {
            var len: int = _items.length;
            for (var i: int = 0; i < len; i++) {
                _items[i].height = value;
            }
            var rect: Rectangle = _cont.scrollRect || new Rectangle(0, 0, width, 10);
            rect.height = value;
            _cont.scrollRect = rect;
            _height = _itemHeight = value;
        }


        // ------------------ PUBLIC METHODS ---------------------------

        override public function setContentPosition(xValueByRatio: Number, yValueByRatio: Number): void {
            xValueByRatio = Math.max(Math.min(xValueByRatio, 1), 0);
            yValueByRatio = Math.max(Math.min(yValueByRatio, 1), 0);

            var itemRealWidth: int = _layout.useFixedSpacing ? _layout.hSpacing : _itemWidth + _layout.hSpacing;
            setPosition((contentWidth - width) * xValueByRatio / itemRealWidth);

            var rect: Rectangle = _cont.scrollRect;
            rect.x = ((contentWidth - width) * xValueByRatio) % itemRealWidth;
            rect.y = height * yValueByRatio;
            _cont.scrollRect = rect;
        }


        // ------------------ PROTECTED METHODS ------------------------

        override protected function initSizeProperties(): void {
            var realItemWidth: int = _layout.useFixedSpacing ? _layout.hSpacing : _itemWidth + _layout.hSpacing;
            var lastMargin: int = _layout.useFixedSpacing ? 0 : _layout.hSpacing;

            _width = realItemWidth * _typedSelectionControl.size - lastMargin;
            _height = _itemHeight;
            _contentWidth = _data ? realItemWidth * _data.length - lastMargin : 0;
            _contentHeight = _itemHeight;
        }


        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}