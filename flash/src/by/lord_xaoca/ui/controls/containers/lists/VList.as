package by.lord_xaoca.ui.controls.containers.lists {

    import by.lord_xaoca.ui.controls.layouts.usual.VerticalLayout;
    import by.lord_xaoca.ui.controls.selection.BaseListSelectionControl;

    import flash.geom.Rectangle;

    /**
     * VList class.
     *
     * @author Ivan Shaban
     * @date 15.10.2011 13:59
     */
    [Event(name="change", type="flash.events.Event")]

    public class VList extends BaseList {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function VList(itemRendererClass: Class, size: int, ...itemRendererArguments) {
            super(new VerticalLayout(), new BaseListSelectionControl(size), itemRendererClass, itemRendererArguments);
        }


        // ------------------ PROPERTIES -------------------------------

        /**
         * CHECKME: maybe should use  _layout.maxHeightInRow \  _layout.maxWidthInColumn accordingly.
         */
            //        override public function get height(): Number { return itemRealHeight * _typedSelectionControl.size - _layout.vSpacing; }

            //        override public function get width(): Number { return _itemWidth; }

        override public function set width(value: Number): void {
            var len: int = _items.length;
            for (var i: int = 0; i < len; i++) {
                _items[i].width = value;
            }
            var rect: Rectangle = _cont.scrollRect || new Rectangle(0, 0, 10, height);
            rect.width = value;
            _cont.scrollRect = rect;
            _width = _itemWidth = value;
        }


        /*
         override public function get contentHeight(): Number { return itemRealHeight * _data.length - _layout.vSpacing; }


         override public function get contentWidth(): Number { return _itemWidth; }
         */

        // ------------------ PUBLIC METHODS ---------------------------

        override public function setContentPosition(xValueByRatio: Number, yValueByRatio: Number): void {
            xValueByRatio = Math.max(xValueByRatio, 0);
            xValueByRatio = Math.min(xValueByRatio, 1);
            yValueByRatio = Math.max(yValueByRatio, 0);
            yValueByRatio = Math.min(yValueByRatio, 1);

            var itemRealHeight: int = _layout.useFixedSpacing ? _layout.vSpacing : _itemHeight + _layout.vSpacing;
            setPosition((contentHeight - height) * yValueByRatio / itemRealHeight);

            var rect: Rectangle = _cont.scrollRect;
            rect.x = width * xValueByRatio;
            rect.y = ((contentHeight - height) * yValueByRatio) % itemRealHeight;
            _cont.scrollRect = rect;
        }


        // ------------------ PROTECTED METHODS ------------------------

        override protected function initSizeProperties(): void {
            var itemRealHeight: int = _layout.useFixedSpacing ? _layout.vSpacing : _itemHeight + _layout.vSpacing;
            var lastMargin: int = _layout.useFixedSpacing ? 0 : _layout.vSpacing;

            _width = _itemWidth;
            _height = itemRealHeight * _typedSelectionControl.size - lastMargin;
            _contentWidth = _itemWidth;
            _contentHeight = _data ? itemRealHeight * _data.length - lastMargin : 0;
        }


        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}