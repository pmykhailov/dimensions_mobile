package by.lord_xaoca.ui.controls.containers.base {

    import flash.display.Sprite;

    /**
     * BaseMovableInteractiveContainer class.
     *
     * @author: Ivan Shaban
     * @date: 11.11.11 11:30
     */

    public class BaseMovableInteractiveContainer extends BaseInteractiveContainer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function BaseMovableInteractiveContainer (view:Sprite, layout:ILayout, selectionControl:ISelectionControl, itemRendererClass:Class, itemRendererArguments:Array = null) {
            super(view, layout, selectionControl, itemRendererClass, itemRendererArguments);
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function _getItemIndex (currentID:int):int {
            return Globals.useMirroring ? _data.length - 1 - currentID : currentID;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
