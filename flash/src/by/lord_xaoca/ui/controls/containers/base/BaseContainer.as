package by.lord_xaoca.ui.controls.containers.base {

    import by.lord_xaoca.interfaces.ILayoutElement;
    import by.lord_xaoca.ui.controls.layouts.ILayout;
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import flash.display.Graphics;
    import flash.display.Sprite;
    import flash.events.Event;

    /**
     * BaseContainer class.
     *
     * @author: Ivan Shaban
     * @date: 10.11.11 10:46
     */

    public class BaseContainer extends BaseDisplayObjectContainer implements ILayoutElement {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _layout:ILayout;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function BaseContainer (view:Sprite, layout:ILayout) {
            _layout = layout;

            super(view);
        }

        // ------------------ PROPERTIES -------------------------------

        public function get layout ():ILayout {
            return _layout;
        }

        override public function set isDebugMode (value:Boolean):void {
            _isDebugMode = value;
            updateBGSize();
        }

        // ------------------ PUBLIC METHODS ---------------------------

        public function refresh ():void {
            _layout.refresh();
            updateBGSize();
        }

        public function add (value:ILayoutElement):ILayoutElement {
            return _layout.addElement(value);
        }

        public function remove (value:ILayoutElement):ILayoutElement {
            return _layout.removeElement(value);
        }

        // ------------------ PROTECTED METHODS ------------------------

        override protected function initialize ():void {
            super.initialize();

            initLayout();
        }

        protected function initLayout ():void {
            _layout.container = view;
            _layout.addEventListener(Event.RESIZE, onLayoutResizeHandler);
        }

        protected function updateBGSize ():void {
            var g:Graphics = _view.graphics;
            g.clear();
            g.beginFill(0x00CCFF, _isDebugMode ? 0.3 : 0);
            g.drawRect(0, 0, _layout.leftIndent + width + _layout.rightIndent, _layout.topIndent + height + _layout.bottomIndent);
            g.endFill();
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        protected function onLayoutResizeHandler (event:Event):void {
            updateBGSize();
        }

        // ------------------ END CLASS --------------------------------

    }
}
