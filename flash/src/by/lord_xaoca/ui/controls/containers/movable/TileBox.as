package by.lord_xaoca.ui.controls.containers.movable {

    import flash.display.Sprite;

    /**
     * TileBox class.
     *
     * @author: Ivan Shaban
     * @date: 10.11.11 11:33
     */

    public class TileBox extends BaseInteractiveContainer implements IMovableContent {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function TileBox (itemRendererClass:Class, rowsCount:int, direction:String = null, ...itemRendererArguments) {
            super(new Sprite(),
                  new MTileLayout(rowsCount, direction ? direction : LayoutDirection.HORIZONTAL),
                  new BaseSelectionControl(),
                  itemRendererClass,
                  itemRendererArguments);
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        public function updatePlacement ():void {
            var selectedItemIndex:int = _selection.selectedItemID;
            _selection.clear();

            _fillData();

            if (selectedItemIndex != -1) {
                _selection.setSelectionByIndex(selectedItemIndex);
            }
        }

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
