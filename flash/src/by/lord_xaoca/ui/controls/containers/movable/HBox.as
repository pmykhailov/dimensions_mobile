package by.lord_xaoca.ui.controls.containers.movable {

    import flash.display.Sprite;

    /**
     * HBox class.
     *
     * @author: Ivan Shaban
     * @date: 10.11.11 11:36
     */

    public class HBox extends BaseMovableInteractiveContainer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function HBox (itemRendererClass:Class, ...itemRendererArguments) {
            super(new Sprite(),
                  new HorizontalLayout(),
                  new MovableSelectionControl(),
                  itemRendererClass,
                  itemRendererArguments);
        }

        // ------------------ PROPERTIES -------------------------------

        override public function get height ():Number {
            return _itemHeight ? _itemHeight : _layout.maxHeightInRow;
        }

        override public function set height (value:Number):void {
            itemHeight = value;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
