package by.lord_xaoca.ui.controls.containers.movable {

    import by.lord_xaoca.interfaces.IScroll;
    import by.lord_xaoca.ui.controls.containers.base.BaseScrollableContainer;

    import flash.display.DisplayObject;

    /**
     * ScrollableContainer class.
     *
     * @author: Ivan Shaban
     * @date: 22.11.11 11:09
     */

    public class ScrollableContainer extends BaseScrollableContainer {

        private var _contentAutoIndent: Boolean;


        public function ScrollableContainer(content: DisplayObject, vertivalScroll: IScroll = null, horizontalScroll: IScroll = null) {
            super(content, vertivalScroll, horizontalScroll);
        }


        public function get contentAutoIndent(): Boolean {
            return _contentAutoIndent;
        }


        public function set contentAutoIndent(value: Boolean): void {
            _contentAutoIndent = value;
        }


        /*override protected function _checkScrollsPosition(): void {
         if (!_scrollsAutoPosition) {
         return;
         }
         if (_vertivalScroll) {
         _vertivalScroll.x = _width - _scrollRightIndent - _vertivalScroll.width;
         _vertivalScroll.y = _scrollTopIndent;
         }
         if (_horizontalScroll) {
         _horizontalScroll.x = _scrollLeftIndent;
         _horizontalScroll.y = _height - _scrollBottomIndent - _horizontalScroll.height;
         }
         }


         override protected function _checkContentPosition(): void {
         super._checkContentPosition();
         if (_contentAutoIndent) {
         // +2 - because our scroll has 2 px indent
         _scrollableContainer.x = 0;
         }
         }*/

    }
}
