package by.lord_xaoca.ui.controls.containers.boxes {

    import by.lord_xaoca.ui.controls.containers.base.BaseInteractiveContainer;
    import by.lord_xaoca.ui.controls.layouts.LayoutDirection;
    import by.lord_xaoca.ui.controls.layouts.usual.TileLayout;
    import by.lord_xaoca.ui.controls.selection.BaseSelectionControl;

    import flash.display.Sprite;

    /**
     * TileBox class.
     *
     * @author: Ivan Shaban
     * @date: 10.11.11 11:33
     */

    public class TileBox extends BaseInteractiveContainer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function TileBox (itemRendererClass:Class, columnsCount:int, direction:String = null, ...itemRendererArguments) {
            super(new Sprite(),
                  new TileLayout(columnsCount, direction ? direction : LayoutDirection.HORIZONTAL),
                  new BaseSelectionControl(),
                  itemRendererClass,
                  itemRendererArguments);
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
