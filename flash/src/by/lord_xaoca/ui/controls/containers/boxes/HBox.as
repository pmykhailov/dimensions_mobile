package by.lord_xaoca.ui.controls.containers.boxes {

    import by.lord_xaoca.ui.controls.containers.base.BaseInteractiveContainer;
    import by.lord_xaoca.ui.controls.layouts.usual.HorizontalLayout;
    import by.lord_xaoca.ui.controls.selection.BaseSelectionControl;

    import flash.display.Sprite;

    /**
     * HBox class.
     *
     * @author: Ivan Shaban
     * @date: 10.11.11 11:36
     */

    public class HBox extends BaseInteractiveContainer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function HBox (itemRendererClass:Class, ...itemRendererArguments) {
            super(new Sprite(),
                  new HorizontalLayout(),
                  new BaseSelectionControl(),
                  itemRendererClass,
                  itemRendererArguments);
        }

        // ------------------ PROPERTIES -------------------------------

        override public function get height ():Number {
            return itemHeight;
        }

        override public function set height (value:Number):void {
            itemHeight = value;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
