package by.lord_xaoca.ui.components.scrolls {

    import by.lord_xaoca.helpers.StageReference;
    import by.lord_xaoca.interfaces.IScroll;
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

import com.share.ApplicationDimensions;

import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;

    /**
     * BaseScroll class.
     *
     * @author: Ivan Shaban
     * @date: 14.11.11 15:03
     */

    [Event(name="change", type="flash.events.Event")]
    [Event(name="startDrag", type="flash.events.Event")]
    [Event(name="stopDrag", type="flash.events.Event")]

    public class BaseScroll extends BaseDisplayObjectContainer implements IScroll {

        public static const START_DRAG: String = "startDrag";
        public static const STOP_DRAG: String = "stopDrag";
        protected const SCROLL_MIN_HEIGTH: int = 50;
        protected const SCROLL_MIN_WIDTH: int = 20;
        protected static var _isDragged: Boolean;

        public static function get isDragged(): Boolean {
            return BaseScroll._isDragged;
        }


        protected var _scroll: Sprite;
        protected var _bar: Sprite;
        protected var _value: Number = 0;
        protected var _useMouseWheel: Boolean;
        protected var _needToUpdate: Boolean;                                // should we dispatch change events if value is changed or not
        protected var _percentsPerStep: Number = 0.05;
        protected var _topIndent: int;
        protected var _bottomIndent: int;
        protected var _leftIndent: int;
        protected var _rightIndent: int;


        public function BaseScroll(view: Sprite, useMouseWheel: Boolean = false) {
            super(view)
            /*initView(view);
             isButton = true;*/

            this.useMouseWheel = useMouseWheel;
        }


        override public function get height(): Number {
            return _bar.height;
        }


        override public function set height($value: Number): void {
            _bar.height = int($value);
        }


        override public function get width(): Number {
            return _bar.width;
        }


        override public function set width($value: Number): void {
            _bar.width = int($value);
        }


        public function get topIndent(): int {
            return _topIndent;
        }


        public function set topIndent($value: int): void {
            _topIndent = $value;
            checkScrollPosition();
        }


        public function get bottomIndent(): int {
            return _bottomIndent;
        }


        public function set bottomIndent($value: int): void {
            _bottomIndent = $value;
            checkScrollPosition();
        }


        public function get leftIndent(): int {
            return _leftIndent;
        }


        public function set leftIndent($value: int): void {
            _leftIndent = $value;
            checkScrollPosition();
        }


        public function get rightIndent(): int {
            return _rightIndent;
        }


        public function set rightIndent($value: int): void {
            _rightIndent = $value;
            checkScrollPosition();
        }


        public function get useMouseWheel(): Boolean {
            return _useMouseWheel;
        }


        public function set useMouseWheel(value: Boolean): void {
            _useMouseWheel = value;

            if (value) {
                addStageListener(MouseEvent.MOUSE_WHEEL, onWheelHandler);
            } else {
                removeStageListener(MouseEvent.MOUSE_WHEEL, onWheelHandler);
            }
        }


        public function get value(): Number {
            return _value;
        }


        public function set value($value: Number): void {
            if ($value < 0) {
                $value = 0;
            }
            if ($value > 1) {
                $value = 1;
            }
            if ($value == _value) {
                return;
            }

            _value = $value;
            updateScrollPositionByValue();

            if (_needToUpdate) {
                _needToUpdate = false;
                dispatchEvent(new Event(Event.CHANGE));
            }
        }


        public function get enable(): Boolean {
            return _view.mouseChildren;
        }


        public function set enable($value: Boolean): void {
            _view.mouseChildren = $value;
        }


        public function get scrollWidth(): int {
            return _scroll.width;
        }


        public function set scrollWidth($value: int): void {
            if ($value < SCROLL_MIN_WIDTH) {
                $value = SCROLL_MIN_WIDTH
            }
            if ($value > width - _leftIndent - _rightIndent) {
                $value = width - _leftIndent - _rightIndent;
            }

            _scroll.width = $value;
            updateScrollPositionByValue();
        }


        public function get scrollHeight(): int {
            return _scroll.height;
        }


        public function set scrollHeight($value: int): void {
            if ($value < SCROLL_MIN_HEIGTH) {
                $value = SCROLL_MIN_HEIGTH;
            }
            if ($value > height - _topIndent - _bottomIndent) {
                $value = height - _topIndent - _bottomIndent;
            }

            _scroll.height = $value;
            updateScrollPositionByValue();
        }


        public function get scrollWidthByRatio(): Number {
            return _scroll.width / (width - _leftIndent - _rightIndent);
        }


        public function set scrollWidthByRatio($value: Number): void {
            scrollWidth = (width - _leftIndent - _rightIndent) * $value;
        }


        public function get scrollHeightByRatio(): Number {
            return _scroll.height / (height - _topIndent - _bottomIndent);
        }


        public function set scrollHeightByRatio($value: Number): void {
            scrollHeight = (height - _topIndent - _bottomIndent) * $value;
        }


        public function get percentsPerStep(): int {
            return _percentsPerStep * 100;
        }


        public function set percentsPerStep($value: int): void {
            _percentsPerStep = $value / 100;
        }


        public function updateByMouseWheel(deltaY: int): void {
            _needToUpdate = true;

            checkScrollPosition();
            updateValueByScrollPosition();
        }


        override protected function initView(): void {
            _view.mouseEnabled = false;

            _scroll = _view.getChildByName("scroll_mc") as Sprite;
            _scroll.buttonMode = true;
            _scroll.addEventListener(MouseEvent.MOUSE_DOWN, onStartDragHandler);

            _bar = _view.getChildByName("bar_mc") as Sprite;
            _bar.addEventListener(MouseEvent.CLICK, onBarClickHandler);
        }


        protected function updateValueByScrollPosition(): void {
            // ABSTRACT
            if (_needToUpdate) {
                _needToUpdate = false;
                dispatchEvent(new Event(Event.CHANGE));
            }
        }


        protected function updateScrollPositionByValue(): void {
            // ABSTRACT
        }


        protected function checkScrollPosition(): void {
            // ABSTRACT
        }


        protected function onStartDragHandler(event: MouseEvent): void {
            // ABSTRACT
            BaseScroll._isDragged = true;
            addStageListener(MouseEvent.MOUSE_UP, onStopDragHandler);
            addStageListener(MouseEvent.MOUSE_MOVE, onSliderMoveHandler);

            dispatchEvent(new Event(BaseScroll.START_DRAG));
        }


        protected function onStopDragHandler(event: MouseEvent): void {
            // ABSTRACT
            BaseScroll._isDragged = false;
            removeStageListener(MouseEvent.MOUSE_UP, onStopDragHandler);
            removeStageListener(MouseEvent.MOUSE_MOVE, onSliderMoveHandler);
            dispatchEvent(new Event(BaseScroll.STOP_DRAG));
        }


        protected function onSliderMoveHandler(event: MouseEvent): void {
            // ABSTRACT
            if (view.stage.mouseX > ApplicationDimensions.WIDTH || view.stage.mouseY > ApplicationDimensions.HEIGHT || view.stage.mouseY < 0 || view.stage.mouseX < 0) {
                StageReference.stage.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_UP));
                return;
            }
            _needToUpdate = true;
            updateValueByScrollPosition();
        }


        protected function onWheelHandler(event: MouseEvent): void {
            updateByMouseWheel(event.delta);
        }


        protected function onBarClickHandler(event: MouseEvent): void {
            // ABSTRACT
            _needToUpdate = true;
            checkScrollPosition();
            updateValueByScrollPosition();
            dispatchEvent(new Event(BaseScroll.STOP_DRAG));
        }

    }
}
