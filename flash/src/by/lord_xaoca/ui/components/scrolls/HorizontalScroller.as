package by.lord_xaoca.ui.components.scrolls {

    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    /**
     * VerticalScroller class.
     *
     * @author: Ivan Shaban
     * @date: 14.11.11 15:31
     */

    public class HorizontalScroller extends BaseScroll {
        // ================== Static section ===============================
        // ------------------ Fields -----------------------------------

        // ------------------ Methods ----------------------------------

        // ================== Class section ================================
        // ------------------ Fields -----------------------------------

        // ------------------ Constructor ------------------------------
        public function HorizontalScroller (view:Sprite, useMouseWheel:Boolean = false) {
            super(view, useMouseWheel);
        }

        // ------------------ Properties -------------------------------

        // ------------------ Public methods ---------------------------

        override public function updateByMouseWheel (deltaX:int):void {
            _scroll.x -= (_bar.width - _scroll.width - _leftIndent - _rightIndent) * _percentsPerStep * deltaX;

            super.updateByMouseWheel(deltaX);
        }

        // ------------------ Protected methods ------------------------

        override protected function updateValueByScrollPosition ():void {
            var newValue:Number = ((_scroll.x - _leftIndent) / (_bar.width - _scroll.width - _leftIndent - _rightIndent));
            if (_value == newValue) {
                return;
            }
            _value = isNaN(newValue) ? 0 : newValue;

            super.updateValueByScrollPosition();
        }

        override protected function updateScrollPositionByValue ():void {
            _scroll.x = _leftIndent + int(_value * (_bar.width - _scroll.width - _leftIndent - _rightIndent));
        }

        override protected function checkScrollPosition ():void {
            if (_scroll.x < _leftIndent) {
                _scroll.x = _leftIndent;
            }
            if (_scroll.x > _bar.width - _scroll.width - _rightIndent) {
                _scroll.x = _bar.width - _scroll.width - _rightIndent;
            }
        }

        // ------------------ Private methods --------------------------

        // ------------------ Interface methods ------------------------

        // ------------------ Event handlers ---------------------------

        override protected function onStartDragHandler (event:MouseEvent):void {
            _scroll.startDrag(false, new Rectangle(_leftIndent, _scroll.y, _bar.width - _scroll.width - _leftIndent - _rightIndent, 0));

            super.onStartDragHandler(event);
        }

        override protected function onStopDragHandler (event:MouseEvent):void {
            _scroll.stopDrag();

            super.onStopDragHandler(event);
        }

        override protected function onBarClickHandler (event:MouseEvent):void {
            var point:Point = _view.globalToLocal(new Point(event.stageX, event.stageY));
            _scroll.x = point.x - _scroll.width / 2;

            super.onBarClickHandler(event);
        }
    }
}
