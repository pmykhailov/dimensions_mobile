package by.lord_xaoca.ui.components.scrolls {

    import by.lord_xaoca.interfaces.IMouseWheelObserver;
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import com.greensock.plugins.AutoAlphaPlugin;
    import com.greensock.plugins.TweenPlugin;

    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    /**
     * ...
     * @author Ivan Shaban
     * @date 12.05.2011 17:38
     */
    [Event(name="change", type="flash.events.Event")]

    public class VerticalScroll extends BaseDisplayObjectContainer implements IMouseWheelObserver /* implements IScroll*/ {

        public static const START_DRAG: String = "startDrag";
        public static const STOP_DRAG: String = "stopDrag";

        protected static var _isDragged: Boolean;

        public static function get isDragged(): Boolean {
            return VerticalScroll._isDragged;
        }


        protected const SCROLL_MIN_HEIGTH: int = 20;

        protected var _slider: Sprite;
        protected var _bar: Sprite;
        protected var _minValue: Number;
        protected var _maxValue: Number;
        protected var _value: Number;
        protected var _useMouseWheel: Boolean;
        protected var _isMouseOver: Boolean;
        protected var _isShows: Boolean = true;
        protected var _percentsPerStep: Number = 0.05;
        protected var _needToUpdate: Boolean; // should we dispatch change events if value is changed or not

        /**
         * Устаревший класс. Подлежит замене на com.cardgames.temp.games.ui.scrolls.PopoverVerticalScroll
         * @param    view
         * @param    minValue
         * @param    maxValue
         * @param    useMouseWheel
         */
        public function VerticalScroll(view: Sprite, minValue: Number = 0, maxValue: Number = 1, useMouseWheel: Boolean = false) {
            TweenPlugin.activate([AutoAlphaPlugin]);

            _minValue = minValue;
            _maxValue = maxValue;

            initView(view);
            isButton = true;
            value = minValue;

            if (useMouseWheel) {
                GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_WHEEL, onWheelHandler);
            }
        }


        public function get enable(): Boolean {
            return _view.mouseChildren;
        }


        public function set enable($value: Boolean): void {
            _view.mouseChildren = $value;
        }


        public function get value(): Number {
            return _value;
        }


        public function set value($value: Number): void {
            if ($value > _maxValue) {
                $value = _maxValue;
            }
            if ($value < _minValue) {
                $value = _minValue;
            }
            if ($value == _value) {
                return;
            }

            _value = $value;
            _slider.y = 1 + (_value - _minValue) / (_maxValue - _minValue) * (_bar.height - _slider.height - 2);

            if (_needToUpdate) {
                _needToUpdate = false;
                dispatchEvent(new Event(Event.CHANGE));
            }
        }


        override public function get height(): Number {
            return _bar.height;
        }


        override public function set height($value: Number): void {
            _bar.height = int($value);
        }


        override public function get width(): Number {
            return _bar.width;
        }


        override public function set width($value: Number): void {
            _bar.width = $value;
        }


        public function get useMouseWheel(): Boolean {
            return _useMouseWheel;
        }


        public function set useMouseWheel($value: Boolean): void {
            _useMouseWheel = $value;

            if ($value) {
                GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_WHEEL, onWheelHandler);
            } else {
                GlobalDispatcher.removeCustomEventListener(MouseEvent.MOUSE_WHEEL, onWheelHandler);
            }
        }


        public function get isShows(): Boolean {
            return _isShows;
        }


        public function set isShows($value: Boolean): void {
            _isShows = $value;
            visible = $value;
        }


        public function get minValue(): Number {
            return _minValue;
        }


        public function get maxValue(): Number {
            return _maxValue;
        }


        public function get percentsPerStep(): int {
            return _percentsPerStep * 100;
        }


        public function set percentsPerStep($value: int): void {
            _percentsPerStep = $value / 100;
        }


        // ------------------ PUBLIC METHODS ---------------------------

        public function refresh(minValue: Number, maxValue: Number): void {
            _minValue = minValue;
            _maxValue = maxValue;
            _needToUpdate = true;
            value = minValue;
        }


        public function setScrollHeight(percent: Number): void {
            if (percent < 0) {
                percent = 0;
            }
            if (percent > 1) {
                percent = 1;
            }
            if ((_bar.height - 2) * percent > SCROLL_MIN_HEIGTH) {
                _slider.height = int((_bar.height - 2) * percent);
            } else {
                _slider.height = SCROLL_MIN_HEIGTH;
            }
            calculateValue();
        }


        public function show(): void {
            if (_isShows) {
                return;
            }
            _isShows = true;
            MouseWheelDispatcher.add(this);
            TweenLite.to(this, 0.5, {autoAlpha: 1});
        }


        public function hide(): void {
            if (!_isShows) {
                return;
            }
            _isShows = false;
            MouseWheelDispatcher.remove(this);
            TweenLite.to(this, 0.5, {autoAlpha: 0});
        }


        /* INTERFACE com.popover.library.utils.observers.IMouseWheelObserver */
        public function updateByMouseWheel(deltaY: int): void {
            _slider.y -= (_bar.height - _slider.height - 2) * _percentsPerStep * deltaY;
            if (_slider.y < 1) {
                _slider.y = 1;
            }
            if (_slider.y > _bar.height - _slider.height - 1) {
                _slider.y = _bar.height - _slider.height - 1;
            }
            _needToUpdate = true;
            calculateValue();
        }


        // ------------------ PROTECTED METHODS ------------------------

        private function initView(view: Sprite): void {
            _view = view;
            _view.mouseEnabled = false;
            _view.addEventListener(MouseEvent.ROLL_OVER, onOverHandler);
            _view.addEventListener(MouseEvent.ROLL_OUT, onOutHandler);

            _slider = _view.getChildByName("scroll_mc") as Sprite;
            _slider.buttonMode = true;
            _slider.addEventListener(MouseEvent.MOUSE_DOWN, onStartDragHandler);

            _bar = _view.getChildByName("bar_mc") as Sprite;
            _bar.alpha = 0.7;
            _bar.addEventListener(MouseEvent.CLICK, onBarClickHandler);
        }


        public function calculateValue(): void {
            var newValue: Number = _minValue + (_maxValue - _minValue) * ((_slider.y - 1) / (_bar.height - _slider.height - 2));
            if (_value == newValue) {
                return;
            }
            _value = newValue;

            if (_needToUpdate) {
                _needToUpdate = false;
                dispatchEvent(new Event(Event.CHANGE));
            }
        }


        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        private function onStartDragHandler(event: MouseEvent): void {
            /**
             * _slider.height - 2
             * -1 top margin, -1 bottom margin
             */
            _slider.startDrag(false, new Rectangle(_slider.x, 1, 0, _bar.height - _slider.height - 2));
            VerticalScroll._isDragged = true;

            GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_UP, onStopDragHandler);
            GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_MOVE, onSliderMoveHandler);
            dispatchEvent(new Event(VerticalScroll.START_DRAG));
        }


        private function onStopDragHandler(event: MouseEvent): void {
            _slider.stopDrag();
            VerticalScroll._isDragged = false;

            GlobalDispatcher.removeCustomEventListener(MouseEvent.MOUSE_UP, onStopDragHandler);
            GlobalDispatcher.removeCustomEventListener(MouseEvent.MOUSE_MOVE, onSliderMoveHandler);
            if (!_isMouseOver) {
                TweenLite.to(_bar, 0.3, {alpha: 0.7});
            }
            dispatchEvent(new Event(VerticalScroll.STOP_DRAG));
        }


        private function onSliderMoveHandler(event: MouseEvent): void {
            _needToUpdate = true;
            calculateValue();
        }


        private function onWheelHandler(event: MouseEvent): void {
            _slider.y -= event.delta;
            if (_slider.y < 1) {
                _slider.y = 1;
            }
            if (_slider.y > _bar.height - _slider.height - 1) {
                _slider.y = _bar.height - _slider.height - 1;
            }

            _needToUpdate = true;
            calculateValue();
        }


        private function onOverHandler(event: MouseEvent): void {
            _isMouseOver = true;
            if (VerticalScroll._isDragged) {
                return;
            }
            TweenLite.to(_bar, 0.3, {alpha: 1});
        }


        private function onOutHandler(event: MouseEvent): void {
            _isMouseOver = false;
            if (VerticalScroll._isDragged) {
                return;
            }
            TweenLite.to(_bar, 0.3, {alpha: 0.7});
        }


        private function onBarClickHandler(event: MouseEvent): void {
            var point: Point = _view.globalToLocal(new Point(event.stageX, event.stageY));
            _needToUpdate = true;
            value = (point.y - _slider.height / 2) / (_bar.height - _slider.height - 2);
        }


        // ------------------ END CLASS --------------------------------
    }
}
