package by.lord_xaoca.ui.components {

    import flash.display.Bitmap;
    import flash.display.DisplayObject;
    import flash.display.Loader;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.net.URLRequest;
    import flash.system.LoaderContext;
    import flash.system.Security;

    /**
     * ImageViewer class.
     *
     * @author Ivan Shaban
     * @date 12.10.2011 11:46
     */

    [Event(name="complete", type="flash.events.Event")]

    dynamic public class ImageViewer extends GUIAdapter {

        // ------------------ STATIC VARIABLES -------------------------

        static public const SCALE_MODE_EXACT_FIT:String = "exactFit";
        static public const SCALE_MODE_NO_SCALE:String = "noScale";
        static public const SCALE_MODE_SHOW_ALL:String = "showAll";

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        public var useBulkLoader:Boolean;

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _imageHolder:Sprite;
        protected var _defaultImage:Sprite;
        protected var _loader:Loader;
        protected var _bloader:BulkLoader;
        protected var _width:int;
        protected var _height:int;
        protected var _scaleMode:String = SCALE_MODE_SHOW_ALL;
        protected var _imageURL:String;

        protected var _isDefaultImageScalable:Boolean;
        protected var _center:Boolean;
        protected var _mask:Sprite;
        protected var _bg:Sprite;
        protected var _debugMc:Sprite;
        protected var _preloader:MovieClip;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function ImageViewer (view:DisplayObject, width:int, height:int, isDefaultImageScalable:Boolean = false) {
            this.view = view as Sprite;
            this._width = width;
            this._height = height;
            this._isDefaultImageScalable = isDefaultImageScalable;

            __initMask();
            __initConts();
            __initBG();
            __initPreloader();
            __initLoader();
        }

        // ------------------ PROPERTIES -------------------------------

        public function get imageURL ():String {
            return _imageURL;
        }

        public function set imageURL (value:String):void {
            _imageURL = value;
        }

        public function get scaleMode ():String {
            return _scaleMode;
        }

        public function set scaleMode (value:String):void {
            _scaleMode = value;
        }

        override public function get width ():Number {
            return _width;
        }

        override public function get height ():Number {
            return _height;
        }

        public function get isDebugMode ():Boolean {
            return _debugMc != null;
        }

        public function set isDebugMode (value:Boolean):void {
            if (isDebugMode) {
                removeChild(_debugMc);
                _debugMc = null;
            }
            if (!value) {
                return;
            }
            _debugMc = Painter.drawRoundedRect(_width, _height, 0, 0x00ccff, 0.7);
            addChild(_debugMc);
        }

        public function get isAvatar ():Boolean {
            return _bg.visible;
        }

        public function set isAvatar (value:Boolean):void {
            _bg.visible = value;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        public function loadContent (url:String, center:Boolean = true):void {
            // if there is no link we just show default image and load nothing
            if (_defaultImage) {
                _defaultImage.visible = url == null || url.length == 0;
            }

            removeImage();
            _imageURL = null;

            if (!url || !url.length) {
                return;
            }

            _imageURL = url;
            _center = center;

            if (useBulkLoader) {
                if (_bloader.getBitmap(_imageURL)) {
                    setImage(_bloader.getBitmap(_imageURL))
                } else {
                    _bloader.add(_imageURL).addEventListener(Event.COMPLETE, onImageLoadHandler);
                    _bloader.start();

                    _preloader.play();
                    addChild(_preloader);
                }
            } else {
                _loader.load(new URLRequest(_imageURL), new LoaderContext(true));
                _preloader.play();
                addChild(_preloader);
            }
        }

        public function unloadContent ():void {
            if (_defaultImage) {
                _defaultImage.visible = true;
            }

            if (_imageURL && _bloader.isRunning && _bloader.get(_imageURL)) {
                var loadingItem:LoadingItem = _bloader.get(_imageURL);
                loadingItem.stop();
                loadingItem.removeEventListener(Event.COMPLETE, onImageLoadHandler);
            }

            if (_loader.contentLoaderInfo.bytesLoaded < _loader.contentLoaderInfo.bytesTotal) {
                _loader.close();
            } else {
                _loader.unload();
            }

            removeImage();
            _imageURL = null;
        }

        public function setSizes (widthValue:int, heightValue:int):void {
            _width = widthValue;
            _height = heightValue;

            _mask.width = widthValue;
            _mask.height = heightValue;

            _bg.width = widthValue;
            _bg.height = heightValue;

            _scaleDefaultImage();
            // default preloader width is 32px and value of scaleX == 22.2%
            _preloader.scaleX = _width / 32 * 0.14;
            _preloader.scaleY = _preloader.scaleX;
            _preloader.x = int((widthValue - _preloader.width) / 2);
            _preloader.y = int((heightValue - _preloader.height) / 2);

            if (!_imageHolder.numChildren) {
                return;
            }
            if (_imageHolder.width < _imageHolder.height) {
                _imageHolder.width = _width;
                _imageHolder.scaleY = _imageHolder.scaleX;
            } else {
                _imageHolder.height = _height;
                _imageHolder.scaleX = _imageHolder.scaleY;
            }
        }

        public function setImage (value:Bitmap, center:Boolean = true):void {
            if (_defaultImage) {
                _defaultImage.visible = false;
            }

            removeImage();

            value.smoothing = true;
            _imageHolder.addChild(value);

            if (_scaleMode == SCALE_MODE_SHOW_ALL) {
                if (_imageHolder.width < _imageHolder.height) {
                    _imageHolder.width = _width;
                    _imageHolder.scaleY = _imageHolder.scaleX;
                } else {
                    _imageHolder.height = _height;
                    _imageHolder.scaleX = _imageHolder.scaleY;
                }
            }

            if (!(_center || center)) {
                return;
            }

            _center = false;
            _imageHolder.x = int((_width - _imageHolder.width) / 2);
            _imageHolder.y = int((_height - _imageHolder.height) / 2);
        }

        public function removeImage ():void {
            if (contains(_preloader)) {
                _preloader.stop();
                removeChild(_preloader);
            }

            if (_imageHolder) {
                while (_imageHolder.numChildren) {
                    _imageHolder.removeChildAt(0);
                }
            }
            _view.removeEventListener(Event.ENTER_FRAME, onCheckChildAllowsParent);
        }

        // ------------------ PRIVATE METHODS --------------------------

        private function __initMask ():void {
            _mask = Painter.drawRoundedRect(_width, _height, 0, 0x00ccff, 0.3);
            addChild(_mask);
        }

        private function __initConts ():void {
            _defaultImage = getMovieClip("default");
            _scaleDefaultImage();
            _imageHolder = getMovieClip("imageHolder");
            _imageHolder.mask = _mask;
        }

        private function __initBG ():void {
            _bg = getMovieClip("bg") || Painter.drawRoundedRect(_width, _height, 0, 0xFFFFFF, 0);
            _bg.width = _width;
            _bg.height = _height;
        }

        private function __initPreloader ():void {
            _preloader = new View_Preloader();
            // default preloader width is 32px and value of scaleX == 13.9%
            _preloader.scaleX = _width / 32 * 0.139;
            _preloader.scaleY = _preloader.scaleX;
            _preloader.x = int((width - _preloader.width) / 2);
            _preloader.y = int((height - _preloader.height) / 2);
        }

        private function __initLoader ():void {
            _loader = new Loader();
            _loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoadHandler);
            _loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, dispatchEvent);
            _loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onImageErrorHandler);

            _bloader = BulkLoader.getLoader("mainLoader");
        }

        protected function _scaleDefaultImage ():void {
            if (!_defaultImage) {
                return;
            }
            if (_isDefaultImageScalable) {
                _defaultImage.x = 0;
                _defaultImage.y = 0;
                _defaultImage.width = width;
                _defaultImage.height = height;
            } else {
                _defaultImage.x = int((width - _defaultImage.width) / 2);
                _defaultImage.y = int((height - _defaultImage.height) / 2);
            }
        }

        // ------------------ EVENT HANDLERS ---------------------------

        private function onImageLoadHandler (event:Event):void {
            if (useBulkLoader) {
                event.currentTarget.removeEventListener(Event.COMPLETE, onImageLoadHandler);
                setImage(_bloader.getBitmap(_imageURL));
                dispatchEvent(new Event(Event.COMPLETE));
            } else {
                // if image has been redirected then loading new crossdomain and after it use image.
                // if link hasn't "https://" that mmeans we load some game asset, not the avatar.
                if (_imageURL && _imageURL != _loader.contentLoaderInfo.url && (_imageURL.indexOf("https://") != -1 || _imageURL.indexOf("http://") != -1)) {
                    Security.loadPolicyFile(_loader.contentLoaderInfo.url + "crossdomain.xml");
                    _view.addEventListener(Event.ENTER_FRAME, onCheckChildAllowsParent);
                } else {
                    setImage(_loader.content as Bitmap);
                    dispatchEvent(new Event(Event.COMPLETE));
                }
            }
        }

        protected function onCheckChildAllowsParent (event:Event):void {
            if (_loader.contentLoaderInfo.childAllowsParent) {
                setImage(_loader.content as Bitmap);
                _view.removeEventListener(Event.ENTER_FRAME, onCheckChildAllowsParent);
                dispatchEvent(new Event(Event.COMPLETE));
            }
        }

        private function onImageErrorHandler (event:* /* ErrorEvent or IOErrorEvent */):void {
            trace("ImageViewer.onImageErrorHandler:", event.text)
        }

        // ------------------ END CLASS --------------------------------

    }
}
