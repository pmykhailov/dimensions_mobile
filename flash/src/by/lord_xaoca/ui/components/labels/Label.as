package by.lord_xaoca.ui.components.labels {

    import flash.display.MovieClip;
    import flash.display.Sprite;

    /**
     * Label class.
     *
     * @author: Ivan Shaban
     * @date: 08.12.11 19:11
     */

    public class Label extends GUIAdapter {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        protected var _labelTF:TLFTextField;
        protected var _bg:MovieClip;

        protected var _hIndent:int;
        protected var _vIndent:int;

        protected var _minWidth:int;
        protected var _maxWidth:int;

        protected var _width:int;                                  // anticipated magnitude if assigned from outside.
        protected var _isAllowVerticalResize:Boolean;

        // ------------------ CONSTRUCTOR ------------------------------

        public function Label (view:Sprite, text:String = null) {
            _view = view;

            _labelTF = TextUtils.initTLFProperties(getTLFTextField("label"));
            _labelTF.direction = Globals.direction;

            _bg = getMovieClip("bg");

            if (text != null) {
                this.text = text;
            }
        }

        // ------------------ PROPERTIES -------------------------------

        public function get text ():String {
            return _labelTF.text;
        }

        public function set text (value:String):void {
            _labelTF.text = value;
            __checkSize();

            visible = value != "";
        }

        public function get hIndent ():int {
            return _hIndent;
        }

        public function set hIndent (value:int):void {
            _hIndent = value;
            __checkSize();
        }

        public function get vIndent ():int {
            return _vIndent;
        }

        public function set vIndent (value:int):void {
            _vIndent = value;
            __checkSize();
        }

        public function get minWidth ():int {
            return _minWidth;
        }

        public function set minWidth (value:int):void {
            _minWidth = value;
            __checkSize();
        }

        public function get maxWidth ():int {
            return _maxWidth;
        }

        public function set maxWidth (value:int):void {
            _maxWidth = value;
            __checkSize();
        }

        public function get isAllowVerticalResize ():Boolean {
            return _isAllowVerticalResize;
        }

        public function set isAllowVerticalResize (value:Boolean):void {
            _isAllowVerticalResize = value;
            __checkSize();
        }

        override public function get height ():Number {
            return _bg.height;
        }

        override public function set height (value:Number):void {
            _bg.height = value;
            isAllowVerticalResize = false;
        }

        public function get textField ():TLFTextField {
            return _labelTF;
        }

        public function set textField (value:TLFTextField):void {
            _labelTF = value;
        }

        override public function get width ():Number {
            return _width || _bg.width;
        }

        override public function set width (value:Number):void {
            _bg.width = _width = value;
            __checkSize();
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        protected function __checkSize ():void {
            _labelTF.wordWrap = false;
            _labelTF.multiline = false;

            if (minWidth && (hIndent * 2 + _labelTF.width) < minWidth) {
                _bg.width = minWidth;
                _labelTF.x = (_bg.width - _labelTF.width) / 2;
                _labelTF.y = vIndent;

                if (_isAllowVerticalResize) {
                    _bg.height = vIndent * 2 + _labelTF.height - 3;     // in that case we make some more indents, then usual.
                }
            } else if (maxWidth && (hIndent * 2 + _labelTF.width) > maxWidth) {
                _labelTF.width = maxWidth;
                _labelTF.wordWrap = true;
                _labelTF.multiline = true;
                _labelTF.x = hIndent;
                _labelTF.y = vIndent + 1;

                _bg.width = maxWidth + 2 * hIndent + 2;
                _bg.height = vIndent * 2 + _labelTF.height;             // in that case we make some more indents, then usual.
            } else {
                _bg.width = _width || _labelTF.width + hIndent * 2 + 1;
                _labelTF.x = Globals.useMirroring ? (width - hIndent - _labelTF.width) : hIndent;
                _labelTF.y = vIndent;

                if (_isAllowVerticalResize) {
                    _bg.height = vIndent * 2 + _labelTF.height - 3;     // in that case we make some more indents, then usual.
                }
            }
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------
    }
}
