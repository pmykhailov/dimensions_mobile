package by.lord_xaoca.ui.components.stepper {

    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.events.TimerEvent;
    import flash.text.TextFieldType;
    import flash.utils.Timer;

    /**
     * NumberStepper class.
     *
     * @author Ivan Shaban
     * @date 14.10.2011 18:17
     */

    /**
     * On value change.
     */
    [Event(name="change", type="flash.events.Event")]
    [Event(name="changeByButton", type="flash.events.Event")]

    public class NumberStepper extends BaseUIElement implements IObserver {

        // ------------------ STATIC VARIABLES -------------------------

        static public const CHANGE_BY_BUTTON:String = "changeByButton";

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _upperButton:Sprite;
        protected var _lowerButton:Sprite;
        protected var _bg:Sprite;
        protected var _labelTF:TLFTextField;

        protected var _value:uint;
        protected var _step:uint = 1;

        protected var _minValue:uint;
        protected var _maxValue:uint;
        protected var _delay:Number;
        protected var _lastChangeValue:int;
        protected var _timer:Timer;
        protected var _editable:Boolean;
        protected var _plusValue:Boolean;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function NumberStepper (view:Sprite, delay:Number = 0.5, minValue:uint = 0, maxValue:uint = 1000000000/*uint.MAX_VALUE*/) {
            _delay = delay;
            _minValue = minValue;
            _maxValue = uint.MAX_VALUE;
            maxValue;

            super(view);
        }

        // ------------------ PROPERTIES -------------------------------

        public function get step ():uint {
            return _step;
        }

        public function set step ($value:uint):void {
            _step = $value;
        }

        public function get value ():uint {
            return _value;
        }

        public function set value ($value:uint):void {
            if ($value < _minValue) {
                $value = _minValue;
            }
            if ($value > _maxValue) {
                $value = _maxValue;
            }
            _value = $value;

            if (!_labelTF) {
                return;
            }

            _labelTF.removeEventListener(Event.CHANGE, onTFChangeHandler);
            _labelTF.text = _value.toString();
            _labelTF.x = _bg.x + int((_bg.width - _labelTF.width) / 2);
            _labelTF.addEventListener(Event.CHANGE, onTFChangeHandler);
        }

        public function get minValue ():uint {
            return _minValue;
        }

        public function set minValue ($value:uint):void {
            _minValue = $value;
            if ($value < _minValue) {
                $value = _minValue;
            }
        }

        public function get maxValue ():uint {
            return _maxValue;
        }

        public function set maxValue ($value:uint):void {
            _maxValue = $value;
            //_labelTF.maxChars = $value.toString().length;
            if ($value > _maxValue) {
                $value = _maxValue;
            }
        }

        public function get editable ():Boolean {
            return _editable;
        }

        public function set editable ($value:Boolean):void {
            _editable = $value;
            _labelTF.mouseEnabled = $value;
            _labelTF.selectable = $value;
            _labelTF.type = $value ? TextFieldType.INPUT : TextFieldType.DYNAMIC;
        }

        public function get delay ():Number {
            return _delay;
        }

        public function set delay ($value:Number):void {
            _delay = $value;
        }

        public function get lastChangeValue ():uint {
            return _lastChangeValue;
        }

        public function get hideControl ():Boolean {
            return !contains(_upperButton) && !contains(_lowerButton);
        }

        public function set hideControl ($value:Boolean):void {
            if ($value) {
                removeChild(_upperButton);
                removeChild(_lowerButton);

                var index:int = _movableItems.indexOf(_bg);
                if (index != -1) {
                    _movableItems.splice(index, 1);
                }
                _bg.x = 0;
            } else {
                addChild(_upperButton);
                addChild(_lowerButton);
            }
        }

        // ------------------ PUBLIC METHODS ---------------------------

        public function updateByEnterframe ():void {
            _lastChangeValue = _plusValue ? step : -step;
            if (value == minValue && _lastChangeValue < 0) {
                return;
            }
            if (value == maxValue && _lastChangeValue > 0) {
                return;
            }
            value += _lastChangeValue;
            dispatchEvent(new Event(Event.CHANGE));
        }

        // ------------------ PROTECTED METHODS ------------------------

        override protected function _initOwnMethods ():void {
            super._initOwnMethods();

            _bg = getMovieClip("bg");

            _initTimer();
            _initButtons();
            _initLabel();

            value = 0;
        }

        private function _initButtons ():void {
            _upperButton = getMovieClip("upper");
            _upperButton.buttonMode = true;
            _upperButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressHandler);

            _lowerButton = getMovieClip("lower");
            _lowerButton.buttonMode = true;
            _lowerButton.addEventListener(MouseEvent.MOUSE_DOWN, onButtonPressHandler);
        }

        private function _initTimer ():void {
            _timer = new Timer(_delay * 1000, 1);
            _timer.addEventListener(TimerEvent.TIMER, onTimerHandler);
        }

        private function _initLabel ():void {
            _labelTF = TextUtils.initTLFProperties(getTLFTextField("label"));
            _labelTF.type = TextFieldType.DYNAMIC;
            _labelTF.restrict = "0-9";
            _labelTF.selectable = false;
            _labelTF.addEventListener(Event.CHANGE, onTFChangeHandler);
        }

        override protected function _initMovableItems ():void {
            super._initMovableItems();

            _movableItems.push(_upperButton, _lowerButton, _labelTF, _bg)
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        private function onTimerHandler (event:TimerEvent):void {
            GlobalDispatcher.add(this);
        }

        private function onTFChangeHandler (event:Event):void {
            if (_labelTF.length > String(_maxValue).length + 1) {
                value = _maxValue;
            } else {
                value = int(_labelTF.text);
            }
            // 8 is default y value of TF, when we input some text from keyboard tf is displayed higher than required
            // to avoid it we setup y value for 2 pixels lower
            _labelTF.y = 10;
            dispatchEvent(new Event(Event.CHANGE));
        }

        private function onButtonPressHandler (event:MouseEvent):void {
            switch (event.currentTarget) {
                case _upperButton:
                    _plusValue = true;
                    break;
                case _lowerButton:
                    _plusValue = false;
                    break;
            }
            dispatchEvent(new Event(CHANGE_BY_BUTTON));
            GlobalDispatcher.addCustomEventListener(MouseEvent.MOUSE_UP, onButtonReleaseHandler);
            _timer.reset();
            _timer.start();
            updateByEnterframe();
        }

        private function onButtonReleaseHandler (event:MouseEvent):void {
            _timer.reset();
            GlobalDispatcher.remove(this);
            GlobalDispatcher.removeCustomEventListener(MouseEvent.MOUSE_UP, onButtonReleaseHandler);
        }

        // ------------------ END CLASS --------------------------------

    }
}
