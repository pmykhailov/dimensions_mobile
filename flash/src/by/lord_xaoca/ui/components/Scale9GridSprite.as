package by.lord_xaoca.ui.components {

    import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    import org.bytearray.display.ScaleBitmap;

    /**
     * Scale9GridSprite class.
     *
     * @author Ivan Shaban
     * @date 27.10.2011 12:20
     */
    public class Scale9GridSprite extends Sprite {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _bitmap: ScaleBitmap;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function Scale9GridSprite(target: DisplayObject, autoReplace: Boolean = true) {
            _initBitmap(target);
            _initProperties(target);

            if (autoReplace && target.parent) {
                target.parent.addChildAt(this, target.parent.getChildIndex(target));
                target.parent.removeChild(target);
            }
        }


        // ------------------ PROPERTIES -------------------------------

        public function get bitmap(): ScaleBitmap {
            return _bitmap;
        }


        override public function set width(value: Number): void {
            _bitmap.width = value;
        }


        override public function set height(value: Number): void {
            _bitmap.height = value;
        }


        override public function get scale9Grid(): Rectangle {
            return _bitmap.scale9Grid;
        }


        override public function set scale9Grid(innerRectangle: Rectangle): void {
            _bitmap.scale9Grid = innerRectangle;
        }


        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        protected function _initBitmap(target: DisplayObject): void {
            var originalScale: Point = new Point(target.scaleX, target.scaleY);
            target.scaleX = target.scaleY = 1;

            var bmd: BitmapData = new BitmapData(target.width, target.height, true, 0xFFFFFF);
            bmd.draw(target);

            target.scaleX = originalScale.x;
            target.scaleY = originalScale.y;

            _bitmap = new ScaleBitmap(bmd);
            _bitmap.scale9Grid = target.scale9Grid;
            _bitmap.width = target.width;
            _bitmap.height = target.height;

            addChild(_bitmap);
        }


        protected function _initProperties(target: DisplayObject): void {
            x = target.x;
            y = target.y;
            name = target.name;
            filters = target.filters.concat();
        }


        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}