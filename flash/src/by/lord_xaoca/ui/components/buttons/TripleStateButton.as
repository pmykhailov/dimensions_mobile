package by.lord_xaoca.ui.components.buttons {

    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import flash.display.DisplayObjectContainer;
    import flash.events.MouseEvent;

    /**
     * DoubleStateButton class.
     *
     * @author: Ivan Shaban
     * @date: 18.06.13 18:49
     */

    public class TripleStateButton extends BaseDisplayObjectContainer {

        private var _state1Button: MovieClipButton;
        private var _state2Button: MovieClipButton;
        private var _state3Button: MovieClipButton;

        public static const STATE_1: uint = 1
        public static const STATE_2: uint = 2
        public static const STATE_3: uint = 3

        private var _currentState: uint = STATE_1;


        public function TripleStateButton(view: DisplayObjectContainer) {
            super(view);

            isButtonBehaviour = true;
            gotoState(_currentState);
            //            isSelected = false;
        }


        public function get currentState(): uint {
            return _currentState;
        }


        public function gotoState(state: uint): void {
            _currentState = state;

            if (_currentState == STATE_1) {
                _state1Button.visible = true;
                _state2Button.visible = false;
                _state3Button.visible = false;
            }
            else if (_currentState == STATE_2) {
                _state1Button.visible = false;
                _state2Button.visible = true;
                _state3Button.visible = false;
            }
            else if (_currentState == STATE_3) {
                _state1Button.visible = false;
                _state2Button.visible = false;
                _state3Button.visible = true;
            }
        }


        override protected function initView(): void {
            super.initView();

            _state1Button = new MovieClipButton(getMovieClip("state1"));
            _state2Button = new MovieClipButton(getMovieClip("state2"));
            _state3Button = new MovieClipButton(getMovieClip("state3"));

            addViewListener(MouseEvent.CLICK, onClickHandler, false, 100);
        }


        private function onClickHandler(event: MouseEvent): void {
            var newState: uint

            if (_currentState == STATE_3) {
                newState = STATE_1
            }
            else {
                newState = _currentState + 1;
            }

            gotoState(newState);
        }

    }
}
