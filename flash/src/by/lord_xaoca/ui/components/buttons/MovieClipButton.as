package by.lord_xaoca.ui.components.buttons {

    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    /**
     * MovieClipButton class.
     *
     * @author Ivan Shaban
     * @date 24.09.2011 14:57
     */

    [Event(name="rollOver", type="flash.events.MouseEvent")]
    [Event(name="rollOut", type="flash.events.MouseEvent")]
    [Event(name="click", type="flash.events.MouseEvent")]
    [Event(name="mouseDown", type="flash.events.MouseEvent")]

    public class MovieClipButton extends BaseDisplayObjectContainer {

        // ------------------ STATIC VARIABLES -------------------------

        public static const STATE_NORMAL: uint = 1;
        public static const STATE_DISABLE: uint = 2;
        public static const STATE_SELECTED: uint = 3;

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _state: uint = STATE_NORMAL;
        protected var _isOver: Boolean = false;
        protected var _isPressed: Boolean = false;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function MovieClipButton(view: DisplayObject) {
            super(view);

            isButtonBehaviour = true;
        }


        // ------------------ PROPERTIES -------------------------------

        public function get state(): uint {
            return _state;
        }


        public function set state(value: uint): void {
            _state = value;
            switch (_state) {
                case STATE_NORMAL:
                    setNormalState();
                    break;
                case STATE_DISABLE:
                    setDisableState();
                    break;
                case STATE_SELECTED:
                    setSelectedState();
                    break;
            }
        }


        protected function get mc(): MovieClip {
            return _view as MovieClip;
        }


        // ------------------ PUBLIC METHODS ---------------------------

        override public function destroy(): void {
            isButtonBehaviour = false;

            if (view.parent) {
                view.parent.removeChild(view);
            }

            super.destroy();
        }


        // ------------------ PROTECTED METHODS ------------------------

        override protected function initView(): void {
            super.initView();

            mouseChildren = false;
            buttonMode = true;
            addViewListener(MouseEvent.ROLL_OVER, onOverHandler);
            addViewListener(MouseEvent.ROLL_OUT, onOutHandler);
            addViewListener(MouseEvent.MOUSE_DOWN, onPressHandler);
            mc.stop();
        }


        protected function setNormalState(): void {
            mouseEnabled = true;
            mc.gotoAndStop("up");
        }


        protected function setDisableState(): void {
            mouseEnabled = false;
        }


        protected function setSelectedState(): void {
            mouseEnabled = false;
            mc.gotoAndStop("select");
        }


        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        protected function onOverHandler(event: MouseEvent): void {
            _isOver = true;
            if (_isPressed || state == MovieClipButton.STATE_SELECTED) {
                return;
            }

            mc.gotoAndStop("over");
        }


        protected function onOutHandler(event: MouseEvent): void {
            _isOver = false;
            if (_isPressed || state == MovieClipButton.STATE_SELECTED ||     state == MovieClipButton.STATE_DISABLE) {
                return;
            }

            mc.gotoAndStop("up");
        }


        protected function onPressHandler(event: MouseEvent): void {
            _isPressed = true;
            mc.gotoAndStop("down");

            addStageListener(MouseEvent.MOUSE_UP, onReleaseHandler);
        }


        protected function onReleaseHandler(event: MouseEvent): void {
            removeStageListener(MouseEvent.MOUSE_UP, onReleaseHandler);
            _isPressed = false;
            if (state == MovieClipButton.STATE_SELECTED || state == MovieClipButton.STATE_DISABLE) {
                return;
            }

            mc.gotoAndStop(_isOver ? "over" : "up");
        }


        // ------------------ END CLASS --------------------------------

    }
}
