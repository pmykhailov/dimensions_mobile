package by.lord_xaoca.ui.components.buttons {

    import flash.display.DisplayObject;
    import flash.text.TextFieldAutoSize;

    import flashx.textLayout.elements.TextFlow;
    import flashx.textLayout.formats.TextLayoutFormat;

    /**
     * ScalableLabelButton class.
     *
     * @author: Ivan Shaban
     * @date: 03.02.12 15:25
     */

    public class ScalableLabelButton extends LabelButton {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _autoScale:Boolean = true;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function ScalableLabelButton (view:DisplayObject) {
            super(view);
        }

        // ------------------ PROPERTIES -------------------------------

        public function get autoScale ():Boolean {
            return _autoScale;
        }

        public function set autoScale (value:Boolean):void {
            _autoScale = value;
            checkPositions();
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function checkPositions ():void {
            if (_autoSize) {
                _labelTF.autoSize = TextFieldAutoSize.LEFT;
                if (_labelTF.x * 2 + _labelTF.width < minWidth) {
                    _bg.width = minWidth;
                    _labelTF.x = _bg.width - _labelTF.width >> 1;
                } else {
                    _labelTF.x = _xMargin;
                    _bg.width = _labelTF.x * 2 + _labelTF.width;
                }
            } else if (_center) {
                _labelTF.x = _bg.width - _labelTF.width >> 1;
            } else if (_autoScale) {
                var textFlow:TextFlow = _labelTF.textFlow;
                var textLayoutFormat:TextLayoutFormat = new TextLayoutFormat(textFlow.hostFormat);
                textLayoutFormat.fontSize = 11;                                 // default font size for View_GreenLabelButton
                textFlow.hostFormat = textLayoutFormat;
                textFlow.flowComposer.updateAllControllers();

                while (_labelTF.width > _bg.width - _xMargin * 2) {
                    textLayoutFormat.fontSize--;
                    textFlow.hostFormat = textLayoutFormat;
                    textFlow.flowComposer.updateAllControllers();
                }

                _labelTF.x = _bg.width - _labelTF.width >> 1;
                _labelTF.y = _bg.height - _labelTF.height >> 1;// + 2;
            }
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
