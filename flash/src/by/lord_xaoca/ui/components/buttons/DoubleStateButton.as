package by.lord_xaoca.ui.components.buttons {

    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import flash.display.DisplayObjectContainer;
    import flash.events.MouseEvent;

    /**
     * DoubleStateButton class.
     *
     * @author: Ivan Shaban
     * @date: 18.06.13 18:49
     */

    public class DoubleStateButton extends BaseDisplayObjectContainer {

        private var _selectedButton:MovieClipButton;
        private var _deselectedButton:MovieClipButton;

        public function DoubleStateButton (view:DisplayObjectContainer) {
            super(view);

            isButtonBehaviour = true;
            isSelected = false;
        }

        public function get isSelected ():Boolean {
            return _selectedButton.visible;
        }

        public function set isSelected (value:Boolean):void {
            _selectedButton.visible = value;
            _deselectedButton.visible = !value;
        }

        override protected function initView ():void {
            super.initView();

            _selectedButton = new MovieClipButton(getMovieClip("selected"));
            _deselectedButton = new MovieClipButton(getMovieClip("deselected"));

            addViewListener(MouseEvent.CLICK, onClickHandler, false, 100);
        }

        private function onClickHandler (event:MouseEvent):void {
            isSelected = !isSelected;
        }
    }
}
