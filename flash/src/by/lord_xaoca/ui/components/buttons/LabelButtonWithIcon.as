package by.lord_xaoca.ui.components.buttons {

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.text.TextFieldAutoSize;

    /**
     * LabelButtonWithIcon class.
     *
     * @author: Ivan Shaban
     * @date: 17.04.12 11:41
     */

    public class LabelButtonWithIcon extends LabelButton {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // Reference to icon
        protected var _icon:Sprite;

        // spacing between labelTF and icon
        protected var _spacing:int;

        // Indicates if the icon is positionned left or right of the label
        protected var _isLeftIcon:Boolean;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function LabelButtonWithIcon (view:DisplayObject, spacing:int = 7, isLeftIcon:Boolean = true) {
            _spacing = spacing;
            _isLeftIcon = isLeftIcon;

            super(view);
        }

        // ------------------ PROPERTIES -------------------------------

        public function get spacing ():int {
            return _spacing;
        }

        public function set spacing (value:int):void {
            _spacing = value;
        }

        /**
         * Returns a reference to the icon
         */
        public function get icon ():Sprite {
            return _icon;
        }

        override public function set width (value:Number):void {
            _bg.width = value / scaleX;
            checkPositions();
        }

        override public function set height (value:Number):void {
            _bg.height = value / scaleY;
            checkPositions();
        }

        public function get isLeftIcon ():Boolean {
            return _isLeftIcon;
        }

        public function set isLeftIcon (value:Boolean):void {
            _isLeftIcon = value;
            checkPositions();
        }

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        override protected function initView ():void {
            super.initView();

            _icon = getSprite("icon");
            _icon.mouseChildren = false;
            _icon.mouseEnabled = false;
        }

        override protected function checkPositions ():void {
            // autosize property has higher execute priority
            if (_autoSize) {
                checkPositionOnAutoSize();
            } else if (_center) {
                checkPositionOnCenter();
            }
        }

        protected function checkPositionOnAutoSize ():void {
            _labelTF.autoSize = TextFieldAutoSize.LEFT;

            var currentWidth:int = _xMargin * 2 + _labelTF.width + _spacing + _icon.width;
            if (minWidth && currentWidth < minWidth) {
                _bg.width = minWidth;
                if (_isLeftIcon) {
                    _icon.x = _bg.width - (_icon.width + _spacing + _labelTF.width) >> 1;
                    _labelTF.x = _icon.x + _icon.width + _spacing;
                } else {
                    _labelTF.x = _bg.width - (_icon.width + _spacing + _labelTF.width) >> 1;
                    _icon.x = _labelTF.x + _labelTF.width + _spacing;
                }
            } else {
                if (_isLeftIcon) {
                    _icon.x = _xMargin;
                    _labelTF.x = _icon.x + _icon.width + _spacing;
                } else {
                    _labelTF.x = _xMargin;
                    _icon.x = _labelTF.x + _labelTF.width + spacing;
                }
                _bg.width = currentWidth;
            }
            _icon.y = _bg.height - _icon.height >> 1;
        }

        protected function checkPositionOnCenter ():void {
            _labelTF.autoSize = TextFieldAutoSize.LEFT;

            if (_isLeftIcon) {
                _icon.x = _bg.width - (_icon.width + _spacing + _labelTF.width) >> 1;
                _labelTF.x = _icon.x + _icon.width + _spacing;
            } else {
                _labelTF.x = _bg.width - (_icon.width + _spacing + _labelTF.width) >> 1;
                _icon.x = _labelTF.x + _labelTF.width + _spacing;
            }
            _icon.y = _bg.height - _icon.height >> 1;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
