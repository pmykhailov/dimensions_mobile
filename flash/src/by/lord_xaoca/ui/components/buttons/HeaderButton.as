package by.lord_xaoca.ui.components.buttons {

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.events.MouseEvent;

    /**
     * HeaderButton class.
     *
     * @author Ivan Shaban
     * @date 27.10.2011 17:12
     */
    public class HeaderButton extends LabelButton {

        // ------------------ STATIC VARIABLES -------------------------

        protected static const ARROW_MARGIN:int = 0;

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        protected var _upperArrow:Sprite;
        protected var _lowerButton:Sprite;

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function HeaderButton (view:DisplayObject) {
            TweenPlugin.activate([TintPlugin]);
            super(view, false);
        }

        // ------------------ PROPERTIES -------------------------------

        override public function set label (value:String):void {
            _labelTF.text = value;

            _reInitItemPosition(_checkPosition);
        }

        override public function get state ():String {
            return super.state;
        }

        override public function set state (value:String):void {
            if (value == _state) {
                return;
            }
            _state = value;

            switch (_state) {
                case LabelButton.STATE_DISABLED:
                {
                    mouseEnabled = false;
                    _upperArrow.visible = false;
                    _lowerButton.visible = false;
                    break;
                }

                case LabelButton.STATE_NORMAL:
                {
                    mouseEnabled = true;
                    _lowerButton.visible = false;
                    _upperArrow.visible = false;
                    break;
                }

                case LabelButton.STATE_SELECTED:
                {
                    mouseEnabled = true;
                    _lowerButton.visible = true;
                    _upperArrow.visible = false;
                    break;
                }

                default:
                {
                }
            }
        }

        public function get toUpper ():Boolean {
            return _upperArrow.visible;
        }

        // ------------------ PUBLIC METHODS ---------------------------

        override public function destroy ():void {
            _view.removeEventListener(MouseEvent.ROLL_OVER, onOverHandler);
            _view.removeEventListener(MouseEvent.ROLL_OUT, onOutHandler);
            _view.removeEventListener(MouseEvent.CLICK, onClickHandler);
            _upperArrow = null;
            _lowerButton = null;

            super.destroy();
        }

        // ------------------ PROTECTED METHODS ------------------------

        override protected function initView ():void {
            super.initView();
            _view.addEventListener(MouseEvent.ROLL_OVER, onOverHandler);
            _view.addEventListener(MouseEvent.ROLL_OUT, onOutHandler);
            _view.addEventListener(MouseEvent.CLICK, onClickHandler);
        }

        protected function _initArrows ():void {
            _upperArrow = getMovieClip("upperArrow");
            _upperArrow.visible = false;

            _lowerButton = getMovieClip("lowerArrow");
            _lowerButton.visible = false;
        }

        override protected function _initOwnMethods ():void {
            _initArrows();
            isButton = true;
        }

        override protected function _initMovableItems ():void {
            super._initMovableItems();

            _movableItems.push(_upperArrow, _lowerButton);
        }

        protected function _checkPosition ():void {
            if (_autoSize) {
                _bg.width = _labelTF.x * 2 + _labelTF.width + HeaderButton.ARROW_MARGIN + _upperArrow.width;
            } else if (_center) {
                _labelTF.x = int((_bg.width - (_labelTF.width + HeaderButton.ARROW_MARGIN + _upperArrow.width)) / 2);
            }
            _upperArrow.x = _labelTF.x + _labelTF.width + HeaderButton.ARROW_MARGIN;
            _lowerButton.x = _upperArrow.x;
        }

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        protected function onClickHandler (event:MouseEvent):void {
            if (selected) {
                _lowerButton.visible = !_lowerButton.visible;
                _upperArrow.visible = !_upperArrow.visible;
            }
        }

        protected function onOverHandler (event:MouseEvent):void {
            TweenLite.to(_labelTF, 0.2, { tint: 0xFFFFFF });
            TweenLite.to(_upperArrow, 0.2, { tint: 0xFFFFFF });
            TweenLite.to(_lowerButton, 0.2, { tint: 0xFFFFFF });
        }

        protected function onOutHandler (event:MouseEvent):void {
            TweenLite.to(_labelTF, 0.2, { tint: 0xF1E6BA });
            TweenLite.to(_upperArrow, 0.2, { tint: 0xF1E6BA });
            TweenLite.to(_lowerButton, 0.2, { tint: 0xF1E6BA });
        }

        // ------------------ END CLASS --------------------------------

    }
}
