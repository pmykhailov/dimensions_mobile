package by.lord_xaoca.ui.components.buttons {

    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import flash.display.DisplayObject;
    import flash.events.MouseEvent;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;

    /**
     * LabelButton class.
     *
     * @author Ivan Shaban
     * @date 11.10.2011 15:29
     */

    [Event(name="click", type="flash.events.MouseEvent")]

    public class LabelButton extends BaseDisplayObjectContainer {

        public static const LABEL_MARGIN: int = 4;
        public static const MIN_BUTTON_WIDTH: int = 80;
        public static const STATE_DISABLED: String = "disabled";
        public static const STATE_NORMAL: String = "enabled";
        public static const STATE_SELECTED: String = "selected";
        protected var _state: String = LabelButton.STATE_NORMAL;
        protected var _labelTF: TextField;
        protected var _bg: MovieClipButton;
        protected var _autoSize: Boolean;
        protected var _center: Boolean;
        protected var _data: Object;
        protected var _minWidth: int;
        /**
         * Margin from left side to labelTF
         */
        protected var _xMargin: int = LabelButton.LABEL_MARGIN;


        public function LabelButton(view: DisplayObject) {
            super(view);

            isButtonBehaviour = true;
        }


        override public function get width(): Number {
            return _bg.width * scaleX;
        }


        override public function set width(value: Number): void {
            _bg.width = value / scaleX;

            centerLabel();
        }


        override public function get height(): Number {
            return _bg.height * scaleY;
        }


        override public function set height(value: Number): void {
            _bg.height = value / scaleY;
            _labelTF.y = _bg.height - _labelTF.height >> 1;
        }


        override public function get buttonMode(): Boolean {
            return super.buttonMode;
        }


        override public function set buttonMode(value: Boolean): void {
            super.buttonMode = value;
            _bg.buttonMode = value;
        }


        public function get data(): Object {
            return _data;
        }


        public function set data(value: Object): void {
            _data = value;
        }


        public function get label(): String {
            return _labelTF.text;
        }


        public function set label(value: String): void {
            _labelTF.text = value;
            checkPositions();
        }


        public function get autoSize(): Boolean {
            return _autoSize;
        }


        public function set autoSize(value: Boolean): void {
            if (value == _autoSize) {
                return;
            }
            _autoSize = value;
            checkPositions();
        }


        public function get center(): Boolean {
            return _center;
        }


        public function set center(value: Boolean): void {
            if (value == _center) {
                return;
            }
            _center = value;
            centerLabel();
        }


        public function get state(): String {
            return _state;
        }


        public function set state(value: String): void {
            if (value == _state) {
                return;
            }
            _state = value;

            switch (_state) {
                case LabelButton.STATE_DISABLED:
                    setDisableState();
                    break;
                case LabelButton.STATE_NORMAL:
                    setNormalState();
                    break;
                case LabelButton.STATE_SELECTED:
                    setSelectedState();
                    break;
            }
        }


        public function get enabled(): Boolean {
            return state == LabelButton.STATE_NORMAL;
        }


        public function set enabled(value: Boolean): void {
            state = value ? LabelButton.STATE_NORMAL : LabelButton.STATE_DISABLED;
        }


        public function get selected(): Boolean {
            return state == LabelButton.STATE_SELECTED;
        }


        public function set selected(value: Boolean): void {
            state = value ? LabelButton.STATE_SELECTED : LabelButton.STATE_NORMAL;
        }


        public function get minWidth(): int {
            return _minWidth;
        }


        public function set minWidth(value: int): void {
            _minWidth = value;
            checkPositions();
        }


        public function get xMargin(): int {
            return _xMargin;
        }


        public function set xMargin(value: int): void {
            _xMargin = value;
            checkPositions();
        }


        public function set filters(value: Array): void {
            _view.filters = value;
        }


        override public function destroy(): void {
            if (view.parent) {
                view.parent.removeChild(view);
            }

            _bg.removeEventListener(MouseEvent.CLICK, dispatchEvent);
            _bg.destroy();
            _bg = null;
            _labelTF = null;

            super.destroy();
        }


        override protected function initialize(): void {
            initBG();

            super.initialize();

            initLabel();
        }


        override protected function initView(): void {
            buttonMode = true;
            mouseEnabled = false;

            _minWidth = LabelButton.MIN_BUTTON_WIDTH;
        }


        protected function initLabel(): void {
            _labelTF = getTextField("label");
            _labelTF.embedFonts = true;
            _labelTF.mouseEnabled = false;
            _labelTF.mouseWheelEnabled = false;
            _labelTF.multiline = false;
            _labelTF.wordWrap = false;
        }


        protected function initBG(): void {
            _bg = new MovieClipButton(getMovieClip("simpleButton"));
        }


        protected function centerLabel(): void {
            if (_center) {
                _labelTF.x = _bg.width - _labelTF.width >> 1;
            }
        }


        protected function setNormalState(): void {
            alpha = 1;

            _bg.state = MovieClipButton.STATE_NORMAL;
        }


        protected function setDisableState(): void {
            alpha = 0.5;

            _bg.state = MovieClipButton.STATE_DISABLE;
        }


        protected function setSelectedState(): void {
            _bg.state = MovieClipButton.STATE_SELECTED;
        }


        protected function checkPositions(): void {
            if (_autoSize) {
                _labelTF.autoSize = TextFieldAutoSize.LEFT;
                if (minWidth && _xMargin * 2 + _labelTF.width < minWidth) {
                    _bg.width = minWidth;
                    _labelTF.x = _bg.width - _labelTF.width >> 1;
                } else {
                    _labelTF.x = _xMargin;
                    _bg.width = Math.round(_xMargin * 2 + _labelTF.width);
                }
            } else if (_center) {
                _labelTF.x = _bg.width - _labelTF.width >> 1;
            }
        }

    }
}
