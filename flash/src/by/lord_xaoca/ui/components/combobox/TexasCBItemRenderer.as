package by.lord_xaoca.ui.components.combobox {

    /**
     * ...
     * @author Vladimir Ivanchenko
     */
    public class TexasCBItemRenderer extends ComboBoxItemRenderer {

        // ------------------ STATIC VARIABLES -------------------------

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        // ------------------ CONSTRUCTOR ------------------------------

        public function TexasCBItemRenderer () {
            _selectedColor = 0x695355;
            _overColor = 0xD6CDAB;
            super();
        }

        // ------------------ PROPERTIES -------------------------------

        // ------------------ PUBLIC METHODS ---------------------------

        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        // ------------------ EVENT HANDLERS ---------------------------

        // ------------------ END CLASS --------------------------------

    }
}
