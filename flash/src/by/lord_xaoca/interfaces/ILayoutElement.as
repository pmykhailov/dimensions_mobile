package by.lord_xaoca.interfaces {

    /**
     * ILayoutElement interface.
     *
     * @author: Ivan Shaban
     * @date: 19.03.12 20:17
     */
    public interface ILayoutElement extends IMovableComponent, IResizableComponent, IVirtualObject {
    }
}
