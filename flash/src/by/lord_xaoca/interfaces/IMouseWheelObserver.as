package by.lord_xaoca.interfaces {
    /**
     * IMouseWheelObserver interface.
     *
     * @author Ivan Shaban
     * @date 29.10.2011 14:28
     */
    public interface IMouseWheelObserver {

        function updateByMouseWheel(deltaY: int): void

    }
}