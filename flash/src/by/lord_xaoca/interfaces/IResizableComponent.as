package by.lord_xaoca.interfaces {

    /**
     * IResizableComponent interface.
     *
     * @author Ivan Shaban
     * @date 15.10.2011 14:06
     */
    public interface IResizableComponent {
        function get width ():Number

        function set width (value:Number):void

        function get height ():Number

        function set height (value:Number):void
    }
}