package by.lord_xaoca.interfaces {

    import flash.display.Sprite;

    /**
     * IVirtualObject interface.
     *
     * @author Ivan Shaban
     * @date 15.10.2011 14:22
     */
    public interface IVirtualObject {
        function get view ():Sprite

        function set view (value:Sprite):void
    }
}