package by.lord_xaoca.interfaces {

    import flash.events.IEventDispatcher;

    /**
     * ISelectable interface.
     *
     * @author Ivan Shaban
     * @date 15.10.2011 13:33
     */
    public interface IItemRenderer extends ILayoutElement, IEventDispatcher {

        function get selected ():Boolean

        function set selected (value:Boolean):void

        function get data ():Object

        function set data (value:Object):void

        function get id ():int

        function set id (value:int):void

        //function get labelPropName(): String;

        //function set labelPropName(value: String): void;

        //function get valuePropName(): String;

        //function set valuePropName(value: String): void;
    }
}