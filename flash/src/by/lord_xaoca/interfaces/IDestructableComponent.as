package by.lord_xaoca.interfaces {

    /**
     * IDestructableComponent interface.
     *
     * @author Ivan Shaban
     * @date 15.10.2011 14:20
     */
    public interface IDestructableComponent {
        function destroy ():void
    }
}