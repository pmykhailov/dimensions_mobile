package by.lord_xaoca.helpers {
    import flash.display.Bitmap;
    import flash.display.Loader;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.IEventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.events.SecurityErrorEvent;
    import flash.events.TimerEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.system.LoaderContext;
    import flash.system.Security;
    import flash.utils.Dictionary;
    import flash.utils.Timer;

    /**
     * Loader Helper class,
     * loads data, callbacks onComplete and onError and cleanups itself.
     *
     * @author: Ivan Shaban
     */

    public class LoaderHelper {

        // ------------------ STATIC VARIABLES -------------------------

        // to prevent loaders from auto GC
        private static const _ALL_LOADERS: Dictionary = new Dictionary();

        /**
         * BitmapData store. to get fast access to early loaded images.
         */
        private static const _CACHE: Object = {};

        // ------------------ STATIC METHODS ---------------------------

        // ------------------ PUBLIC VARIABLES -------------------------

        // ------------------ PROTECTED VARIABLES ----------------------

        // ------------------ PRIVATE VARIABLES ------------------------

        private var _url: String;
        private var _loader: IEventDispatcher;


        public function set url(value: String): void {
            _url = value;
        }


        private var _onCompleteHandler: Function;
        private var _onErrorHandler: Function;
        private var _loaderContext: LoaderContext;
        private var _isContentDO: Boolean;
        private var _timeout: int = 0;
        private var _timer: Timer;

        // ------------------ CONSTRUCTOR ------------------------------

        /**
         *
         * @param url
         * @param onCompleteHandler function. returns data: DisplayObject or *.
         * @param onErrorHandler function. returns errorMessage:String.
         * @param isContentDO Is content is DisplayObject (true) or is it raw data such as XML, String (false)
         * example:
         *
         * new LoaderHelper("myUrl", onLoadComplete, onLoadFailed, false)
         *         .timeout(GlobalSettings.TIMEOUT_CONTENT_FAILED) // optional
         *         .addLoaderContext(new LoaderContext())    // optional
         *         .load();
         *
         */
        public function LoaderHelper(url: String, onCompleteHandler: Function, onErrorHandler: Function = null, isContentDO: Boolean = true) {
            LoaderHelper._ALL_LOADERS[this] = this;
            _url = url;
            _onCompleteHandler = onCompleteHandler;
            _onErrorHandler = onErrorHandler || _defaultErrorHandler;
            _isContentDO = isContentDO;

            if (_onCompleteHandler == null) {
                throw Error("Incorrect argument value.");
            }
        }


        // ------------------ PROPERTIES -------------------------------

        public function get url(): String {
            return _url;
        }


        // ------------------ PUBLIC METHODS ---------------------------

        public function addLoaderContext(value: LoaderContext): LoaderHelper {
            _loaderContext = value;
            return this;
        }


        public function timeout(msec: int): LoaderHelper {
            _timeout = msec;
            return this;
        }


        public function load(): LoaderHelper {
            if (!url) {
                _onErrorHandler("There is no URL.");
                destroy();
                return this;
            }
            if (LoaderHelper._CACHE[url]) {
                _onCompleteHandler(new Bitmap(LoaderHelper._CACHE[url].clone()));
                destroy();
                return this;
            }

            if (!_loader) {
                _loader = _isContentDO ? startDOLoader() : startDataLoader();

                if (_timeout > 0) {
                    _timer = new Timer(_timeout);
                    _timer.addEventListener(TimerEvent.TIMER, onLoadTimeoutHandler);
                    _timer.start();
                }
            }
            return this;
        }


        public function destroy(): void {
            if (_timer) {
                _timer.stop();
                _timer.removeEventListener(TimerEvent.TIMER, onLoadTimeoutHandler);
                _timer = null;
            }

            _loader && removeListeners(_loader);
            _onCompleteHandler = null;
            _onErrorHandler = null;
            _loaderContext = null;
            _url = null;
            _loader = null;

            delete LoaderHelper._ALL_LOADERS[this];
        }


        // ------------------ PROTECTED METHODS ------------------------

        // ------------------ PRIVATE METHODS --------------------------

        private function startDataLoader(): IEventDispatcher {
            var loader: URLLoader = addListeners(new URLLoader()) as URLLoader;
            loader.load(new URLRequest(_url));
            return loader;
        }


        private function startDOLoader(): IEventDispatcher {
            var loader: Loader = new Loader();
            addListeners(loader.contentLoaderInfo);
            loader.load(new URLRequest(_url), _loaderContext);
            return loader;
        }


        private function addListeners(target: IEventDispatcher): IEventDispatcher {
            target.addEventListener(IOErrorEvent.IO_ERROR, onLoadErrorHandler);
            target.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoadErrorHandler);
            target.addEventListener(Event.COMPLETE, onLoadCompleteHandler);
            return target;
        }


        private function removeListeners(target: IEventDispatcher): void {
            target.removeEventListener(IOErrorEvent.IO_ERROR, onLoadErrorHandler);
            target.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoadErrorHandler);
            target.removeEventListener(Event.COMPLETE, onLoadCompleteHandler);
        }


        private function _defaultErrorHandler(message: String): void {
            trace(">>", this, message);
        }


        private function complete(event: Event): void {
            if (_isContentDO && event.currentTarget.content is Bitmap) {
                LoaderHelper._CACHE[url] = (event.currentTarget.content as Bitmap).bitmapData;
            }

            if (_onCompleteHandler != null) {
                _onCompleteHandler(_isContentDO ? event.currentTarget.content : event.currentTarget.data);
            }
        }


        private function hasRedirected(event: Event): Boolean {
            // if image has been redirected then loading new crossdomain and after it use image.
            // if link hasn't "https://" that mmeans we load some game asset, not the avatar.
            var loader: Loader = _loader as Loader;
            if (url && url != loader.contentLoaderInfo.url && (url.indexOf("https://") != -1 || url.indexOf("http://") != -1)) {
                Security.loadPolicyFile(loader.contentLoaderInfo.url + "crossdomain.xml");
                loader.addEventListener(Event.ENTER_FRAME, onCheckChildAllowsParent);
                return true;
            }
            return false;
        }


        // ------------------ EVENT HANDLERS ---------------------------

        private function onLoadErrorHandler(event: ErrorEvent): void {
            _onErrorHandler(event.text);
            destroy();
        }


        private function onLoadTimeoutHandler(event: TimerEvent): void {
            try {
                if (_loader is URLLoader) {
                    URLLoader(_loader).close();
                }
                else if (_loader is Loader) {
                    Loader(_loader).close();
                }
            }
            catch (error: Error) {
            }
            _onErrorHandler("load timeout");
            destroy();
        }


        private function onLoadCompleteHandler(event: Event): void {
            if (hasRedirected(event)) {
                return;
            }
            complete(event);
            destroy();
        }


        protected function onCheckChildAllowsParent(event: Event): void {
            var loader: Loader = _loader as Loader;
            if (!loader) {
                loader.removeEventListener(Event.ENTER_FRAME, onCheckChildAllowsParent);
                _onErrorHandler(this + " >> bad redirect happens.");
                return;
            }

            if (loader.contentLoaderInfo.childAllowsParent) {
                loader.removeEventListener(Event.ENTER_FRAME, onCheckChildAllowsParent);
                complete(event);
                destroy();
            }
        }


        // ------------------ END CLASS --------------------------------

    }
}
