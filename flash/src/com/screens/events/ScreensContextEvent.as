package com.screens.events {
    import flash.events.Event;

    /**
     * ScreensContextEvent class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensContextEvent extends Event {

        public static const INIT: String = "initScreensLayer";

        public static const INIT_SOUNDS: String = "initSounds";
        public static const VOLUME_VALUE_UPDATED: String = "volumeValueUpdated";
        public static const MUTE_VALUE_UPDATED: String = "muteValueUpdated";

        // Screens general
        public static const SHOW_SCREEN: String = "showScreen";

        // Screens common
        public static const GET_LEADERBOARD_DATA: String = "gteLeaderboardData";

        // Events from different screens
        public static const SHOW_LEVEL_COMPLETE_SCREEN:String = "showLevelCompleteScreen";

        // Enter name screen
        public static const NAME_ENTERED: String = "nameEntered";
        // Loading screen
        public static const RECONNECT: String = "reconnect";
        public static const LOADING_COMPLETE_ANIMTION_COMPLETE: String = "loadingCompleteAnimationComplete";
        // Start screen
        public static const MAIN_MENU_PLAY: String = "mainMenuPlay";
        public static const MAIN_MENU_LEADERBOARD: String = "mainMenuLeaderboard";
        public static const MAIN_MENU_SETTINGS: String = "mainMenuSettings";
        public static const MAIN_MENU_GAME_RULES: String = "mainMenuGameRules";
        public static const MAIN_MENU_MORE_GAMES: String = "mainMenuMoreGames";
        // Level complete screen
        public static const HOME: String = "home";
        // Leaderboard screen
        public static const LEADERBOARD_CLOSE: String = "LeaderboardClose";
        // Settings screen
        public static const SETTINGS_CLOSE: String = "SettingsClose";
        public static const VOLUME_VALUE_CHANGED: String = "volumeValueChanged";
        public static const NAME_CHANGED: String = "nameChanged";
        // Game rules screen
        public static const GAME_RULES_CLOSE: String = "GameRulesClose";
        // Header
        public static const MUTE_VALUE_CHANGED: String = "muteValueChanged";
        public static const BACK_TO_MAIN_MENU: String = "backToMainMenu";
        // Alert
        public static const SHOW_ALERT: String = "ShowAlert";
        public static const HIDE_ALERT: String = "HideAlert";
        public static const ALERT_CONFIRM: String = "AlertConfirm";
        public static const ALERT_CANCEL: String = "AlertCancel";

        // Game paused
        public static const GAME_PAUSED_CONTINUE: String = "GamePausedContinue";
        public static const GAME_PAUSED_HOME: String = "GamePausedHome";
        public static const GAME_PAUSED_HIDE: String = "GamePausedHide";

        private var _data: Object;


        public function ScreensContextEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new ScreensContextEvent(type, _data, bubbles, cancelable);
        }
    }
}