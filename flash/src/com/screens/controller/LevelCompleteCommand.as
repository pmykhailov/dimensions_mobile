package com.screens.controller {
import com.app.events.ApplicationEvent;
import com.app.model.service.RequestVO;
import com.game_wrapper.event.GameWrapperControllEvent;
    import com.game_wrapper.model.level.LevelModel;
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
    import com.screens.view.layer.ScreensLayerView;
    import com.screens.view.screens.enum.ScreensTypeEnum;
    import com.screens.view.screens.game.GameScreen;

    import flash.display.MovieClip;
    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;

public class LevelCompleteCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var screensLayer: ScreensLayerView;
        [Inject]
        public var levelModel: LevelModel;
        [Inject]
        public var contextView: ContextView;
        [Inject]
        public var applicationModel: ApplicationModel;
        [Inject]
        public var context:IContext;

        public function LevelCompleteCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            // Change background
            var bg: MovieClip = contextView.view.getChildByName("background") as MovieClip;
            bg.gotoAndStop("_deactivated");

            // Mem score
            var scores: Array = applicationModel.userModel.localScoresHistory;

            scores.push({score: levelModel.score, time: levelModel.timePlayed});
            scores.sortOn("score", Array.DESCENDING | Array.NUMERIC);
            if (scores.length > applicationModel.maxSavedScores) {
                scores.length = applicationModel.maxSavedScores;
            }

            applicationModel.userModel.localScoresHistory = scores;
            dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.SAVE_MODEL_TO_SHARED_OBJECT));

            context.detain(this);

            applicationModel.userModel.lastScore = levelModel.score;
            applicationModel.userModel.lastTime = levelModel.timePlayed;

            applicationModel.httpService.submitScore(new RequestVO(onSucessHandler, onErrorHandler), applicationModel.userModel.id, levelModel.score, levelModel.timePlayed);

            // Show level complete screen or congratulations screen after which will be shown level complete screen
            dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.REMOVE_GAME_WRAPPER, (screensLayer.activeScreen as GameScreen).gameContainer));


            //dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.START}));

            if (scores[0].score == levelModel.score && levelModel.score > 0 && scores.length > 1) {
                dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type:ScreensTypeEnum.CONGRATULATIONS}));
            } else {
                dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_LEVEL_COMPLETE_SCREEN));
            }

        }

        private function onSucessHandler(data:Object):void {
            context.release(this);
        }

        private function onErrorHandler():void {
            context.release(this);
        }
    }
}
