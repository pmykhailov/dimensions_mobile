package com.screens.controller.sounds {
    import by.lord_xaoca.utils.ClassUtils;

    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;

import flash.desktop.NativeApplication;
import flash.events.Event;

import flash.events.IEventDispatcher;
import flash.media.SoundMixer;
import flash.media.SoundTransform;

import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    public class InitSoundsCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;

        [Inject]
        public var applicationModel: ApplicationModel;


        override public function execute(): void {

            Ambience;
            FLA_button_click;
            FLA_grid_items_matched;
            FLA_item_fall_sound;
            FLA_grid_item_select;
            FLA_grid_item_unselect;
            ReadySound;
            GoSound;
            TickSound;
            LevelCompleteSound;
            MultiplierActivated;
            MultiplierDeactivated;

            var sounds: Array = SoundModel.ALL_FX;

            var n: int = sounds.length;
            for (var i: int = 0; i < n; i++) {
                _addSound(sounds[i], SoundModel.GROUP_SFX);
            }


            _addSound(SoundModel.AMBIENCE, SoundModel.GROUP_MUSIC);

            SoundAS.group(SoundModel.GROUP_MUSIC).playLoop(SoundModel.AMBIENCE, 0.5);

            SoundAS.group(SoundModel.GROUP_MUSIC).masterVolume = applicationModel.settingsModel.musicVolume;
            SoundAS.group(SoundModel.GROUP_SFX).masterVolume = applicationModel.settingsModel.soundVolume;

            NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, activate);
            NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, deactivate);
        }


        private function _addSound(soundClassName: String, group:String): void {
            SoundAS.group(group).addSound(soundClassName, ClassUtils.getInstanceByClassName(soundClassName));
        }

        private function activate(e: Event): void {
            SoundMixer.soundTransform = new SoundTransform(1);
        }

        private function deactivate(e: Event): void {
            SoundMixer.soundTransform = new SoundTransform(0);
        }

    }
}
