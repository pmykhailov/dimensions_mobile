package com.screens.controller.windows.game_paused {
import com.app.events.ApplicationEvent;
import com.game_wrapper.model.level.LevelModel;
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;
import com.screens.view.windows.game_paused.GamePausedView;

import flash.events.IEventDispatcher;

import org.swiftsuspenders.Injector;

import robotlegs.bender.bundles.mvcs.Command;

    /**
     * QuitAlertCancelCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class GamePausedContinueCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;

        [Inject]
        public var levelModel: LevelModel;

        [Inject]
        public var injector: Injector;


        public function GamePausedContinueCommand() {
            super();
        }

        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new GameWrapperControllEvent(ScreensContextEvent.GAME_PAUSED_HIDE));

            if (levelModel.isPaused) {

                dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.RESUME_GAME));
                dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.RESUME_READY_GO_ANIMATION));
            }
        }
    }
}