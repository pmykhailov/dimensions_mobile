﻿package com.screens.controller.windows.game_paused {
import com.app.model.ApplicationModel;
import com.game_wrapper.event.GameWrapperContextEvent;
    import com.screens.view.windows.alert.AlertView;
    import com.game_wrapper.event.GameWrapperControllEvent;
import com.screens.view.windows.game_paused.GamePausedView;

import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    /**
     * ShowQuitAlertCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class ShowGamePausedWindowCommand extends Command {

        [Inject]
        public var contextView: ContextView;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var applicationModel:ApplicationModel;

        public function ShowGamePausedWindowCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var gamePausedView: GamePausedView = new GamePausedView();
            injector.map(GamePausedView).toValue(gamePausedView);

            gamePausedView.data = {musicVolume:applicationModel.settingsModel.musicVolume, soundVolume:applicationModel.settingsModel.soundVolume};

            contextView.view.addChild(gamePausedView.view);
        }
    }
}