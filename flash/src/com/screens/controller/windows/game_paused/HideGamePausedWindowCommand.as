﻿package com.screens.controller.windows.game_paused {
import com.app.events.ApplicationEvent;
import com.game_wrapper.event.GameWrapperContextEvent;
    import com.screens.view.windows.alert.AlertView;
    import com.game_wrapper.event.GameWrapperControllEvent;
import com.screens.view.windows.game_paused.GamePausedView;

import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    /**
     * ShowQuitAlertCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class HideGamePausedWindowCommand extends Command {

        [Inject]
        public var contextView: ContextView;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var gamePausedView: GamePausedView;


        public function HideGamePausedWindowCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            injector.unmap(GamePausedView);
            gamePausedView.destroy();
            dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.SAVE_MODEL_TO_SHARED_OBJECT));
        }
    }
}