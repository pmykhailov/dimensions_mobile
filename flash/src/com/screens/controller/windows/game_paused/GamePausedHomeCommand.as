package com.screens.controller.windows.game_paused {
import com.app.events.ApplicationEvent;
import com.game_wrapper.model.level.LevelModel;
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;
import com.screens.view.screens.enum.ScreensTypeEnum;
import com.screens.view.windows.game_paused.GamePausedView;

import flash.display.MovieClip;

import flash.events.IEventDispatcher;

import org.swiftsuspenders.Injector;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;

/**
     * QuitAlertCancelCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class GamePausedHomeCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;

        [Inject]
        public var levelModel: LevelModel;

        [Inject]
        public var injector: Injector;

        [Inject]
        public var contextView: ContextView;


        public function GamePausedHomeCommand() {
            super();
        }

        override public function execute(): void {
            super.execute();

            var bg:MovieClip = contextView.view.getChildByName("background") as MovieClip;
            bg.gotoAndStop("_deactivated");


            dispatcher.dispatchEvent(new GameWrapperControllEvent(ScreensContextEvent.GAME_PAUSED_HIDE));

            dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.STOP_TIME_CALCULATING_COMMAND));
            dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.REMOVE_GAME_WRAPPER));

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type:ScreensTypeEnum.START}));
        }
    }
}