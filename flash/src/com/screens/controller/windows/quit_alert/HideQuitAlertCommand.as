package com.screens.controller.windows.quit_alert {
    import com.screens.view.windows.alert.AlertView;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * HideQuitAlertCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class HideQuitAlertCommand extends Command {

        [Inject]
        public var alertView: AlertView;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var dispatcher:IEventDispatcher;

        public function HideQuitAlertCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            injector.unmap(AlertView);
            alertView.destroy();
        }
    }
}