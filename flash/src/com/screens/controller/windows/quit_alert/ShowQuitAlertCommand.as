﻿package com.screens.controller.windows.quit_alert {
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.screens.view.windows.alert.AlertView;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    /**
     * ShowQuitAlertCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class ShowQuitAlertCommand extends Command {

        [Inject]
        public var contextView: ContextView;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var dispatcher:IEventDispatcher;

        public function ShowQuitAlertCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var alertView: AlertView = new AlertView();
            injector.map(AlertView).toValue(alertView);

            contextView.view.addChild(alertView.view);
        }
    }
}