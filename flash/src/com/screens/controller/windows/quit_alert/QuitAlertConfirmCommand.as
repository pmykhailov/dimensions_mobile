package com.screens.controller.windows.quit_alert {
    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.enum.ScreensTypeEnum;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.display.MovieClip;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    /**
     * QuitAlertCancelCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class QuitAlertConfirmCommand extends Command {

        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var contextView: ContextView;

        public function QuitAlertConfirmCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var bg:MovieClip = contextView.view.getChildByName("background") as MovieClip;
            bg.gotoAndStop("_deactivated");

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.HIDE_ALERT));

            dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.STOP_TIME_CALCULATING_COMMAND));
            dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.REMOVE_GAME_WRAPPER));

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type:ScreensTypeEnum.START}));
        }
    }
}