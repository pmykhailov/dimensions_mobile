package com.screens.controller.windows.quit_alert {
    import com.game_wrapper.model.level.LevelModel;
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * QuitAlertCancelCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class QuitAlertCancelCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var levelModel: LevelModel;


        public function QuitAlertCancelCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.HIDE_ALERT));

            if (!levelModel.isPaused) {

                dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.RESUME_GAME));
                dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.RESUME_READY_GO_ANIMATION));
            }
        }
    }
}