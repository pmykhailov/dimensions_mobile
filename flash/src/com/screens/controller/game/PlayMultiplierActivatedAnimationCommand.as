package com.screens.controller.game {
    import flash.display.MovieClip;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    /**
     * PlayMultiplierActivatedAnimationCommand class.
     * User: Paul Makarenko
     * Date: 15.11.13
     */
    public class PlayMultiplierActivatedAnimationCommand extends Command {

        [Inject]
        public var contextView: ContextView;

        public function PlayMultiplierActivatedAnimationCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var bg:MovieClip = contextView.view.getChildByName("background") as MovieClip;
            bg.gotoAndPlay("_activate");
        }
    }
}