package com.screens.controller.screens.level_complete {
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
import com.screens.view.screens.enum.ScreensTypeEnum;

import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class HomeCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;

        public function HomeCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.START}));
        }
    }
}