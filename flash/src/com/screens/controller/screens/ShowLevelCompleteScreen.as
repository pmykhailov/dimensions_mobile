package com.screens.controller.screens {
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * ShowLevelCompleteScreen class.
     * User: Paul Makarenko
     * Date: 16.11.13
     */
    public class ShowLevelCompleteScreen extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var applicationModel: ApplicationModel;


        public function ShowLevelCompleteScreen() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var score:Number = applicationModel.userModel.lastScore;
            var time:Number = applicationModel.userModel.lastTime;

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.LEVEL_COMPLETE, data: {score: score, time:time, scores: applicationModel.userModel.localScoresHistory}}));
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.GET_LEADERBOARD_DATA));
        }
    }
}