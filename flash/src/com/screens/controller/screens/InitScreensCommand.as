package com.screens.controller.screens {
import com.greensock.TweenLite;
import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
    import com.screens.view.layer.ScreensLayerView;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.display.Bitmap;
    import flash.display.MovieClip;
    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    /**
     * InitScreensCommand class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class InitScreensCommand extends Command {

        [Inject]
        public var contextView: ContextView;
        [Inject]
        public var screensLayerView: ScreensLayerView;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var settingsModel: ApplicationModel;


        public function InitScreensCommand() {
            super();
        }


        override public function execute(): void {
            var background:MovieClip = new View_Background();
            background.name = "background";
            contextView.view.addChild(background);

            contextView.view.addChild(screensLayerView.view);

        }
    }
}