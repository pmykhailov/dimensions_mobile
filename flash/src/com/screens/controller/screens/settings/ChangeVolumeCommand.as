/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 21.10.13
 * Time: 19:45
 * To change this template use File | Settings | File Templates.
 */
package com.screens.controller.screens.settings {
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class ChangeVolumeCommand extends Command {

        [Inject]
        public var event: ScreensContextEvent;
        [Inject]
        public var applicationModel: ApplicationModel;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function ChangeVolumeCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();


            if (event.data.hasOwnProperty("musicVolume")) applicationModel.settingsModel.musicVolume = event.data.musicVolume as Number;
            if (event.data.hasOwnProperty("soundVolume")) applicationModel.settingsModel.soundVolume = event.data.soundVolume as Number;

            SoundAS.group(SoundModel.GROUP_MUSIC).masterVolume = applicationModel.settingsModel.musicVolume;
            SoundAS.group(SoundModel.GROUP_SFX).masterVolume = applicationModel.settingsModel.soundVolume;
        }
    }
}
