package com.screens.controller.screens.settings {
import com.app.events.ApplicationEvent;
import com.app.model.service.RequestVO;
import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * LevelSelectionScreenOpenCommand class.
     * User: Paul Makarenko
     * Date: 12.10.13
     */
    public class SettingsCloseCommand extends Command {

        [Inject]
        public var settingsModel:ApplicationModel;
        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var applicationModel:ApplicationModel;

        public function SettingsCloseCommand() {
            super();
        }


        override public function execute():void {
            super.execute();

            applicationModel.httpService.updateUserData(new RequestVO(onSuccess, onError), applicationModel.userModel.id, applicationModel.userModel.name);

            dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.SAVE_MODEL_TO_SHARED_OBJECT));
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.START}));
        }

        private function onSuccess(data:Object):void {

        }

        private function onError():void {

        }
    }
}