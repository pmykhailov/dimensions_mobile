/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 21.10.13
 * Time: 19:45
 * To change this template use File | Settings | File Templates.
 */
package com.screens.controller.screens.settings {
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class ChangeNameCommand extends Command {

        [Inject]
        public var event: ScreensContextEvent;
        [Inject]
        public var applicationModel: ApplicationModel;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function ChangeNameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            applicationModel.userModel.name = event.data.name;
        }
    }
}
