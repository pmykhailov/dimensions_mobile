package com.screens.controller.screens.game_rules {
    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * GameRulesCloseCommand class.
     * User: Paul Makarenko
     * Date: 08.11.13
     */
    public class GameRulesCloseCommand extends Command {

        [Inject]
        public var dispatcher:IEventDispatcher;

        public function GameRulesCloseCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.START}));
        }
    }
}