/**
 * Created by Pasha on 27.07.2016.
 */
package com.screens.controller.screens.common {
import com.app.model.ApplicationModel;
import com.app.model.service.RequestVO;
import com.screens.events.ScreensContextEvent;
import com.screens.view.layer.ScreensLayerView;
import com.screens.view.leaderboard.vo.LeaderboardItemVO;
import com.screens.view.leaderboard.vo.PeroidVO;
import com.screens.view.screens.enum.ScreensTypeEnum;
import com.screens.view.screens.leaderboard.LeaderboardScreen;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class GetLeaderboardDataCommand extends Command {

    [Inject]
    public var dispatcher: IEventDispatcher;

    [Inject]
    public var applicationModel: ApplicationModel;

    [Inject]
    public var context:IContext;

    [Inject]
    public var screensLayerView: ScreensLayerView;

    public function GetLeaderboardDataCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        context.detain(this);

        applicationModel.httpService.getLeaderboard(new RequestVO(onUserCreated, onErrorHandler), applicationModel.userModel.id);
    }

    private function onUserCreated(data:Object):void {
        context.release(this);

        // Parse
        var xml:XML = new XML(data as String);
        var data:Object;
        var peroidVO:PeroidVO;
        var peroidVOs:Array = [];
        var items:Array;
        var leaderboardData:XML;
        var itemsData:XML;
        var leaderboardItemVO:LeaderboardItemVO;
        var myResult:LeaderboardItemVO;
        var isHighlighted:Boolean;
        var userRank:int;
        var userHighScore:int;
        var userTimeSpend:int;

        for (var i:int = 0; i < xml.leaderboard.length(); i++) {
            leaderboardData = xml.leaderboard[i];

            userRank = int(leaderboardData.@user_rank);
            userHighScore = int(leaderboardData.@user_high_score);
            userTimeSpend = int(leaderboardData.@user_time_spend);

            items = [];

            for (var j:int = 0; j < leaderboardData.item.length(); j++) {
                itemsData = leaderboardData.item[j];

                isHighlighted = (userRank == j+1);

                leaderboardItemVO = new LeaderboardItemVO(j+1, itemsData.@name, int(itemsData.@high_score), int(itemsData.@time_spend),isHighlighted);

                items[j] = leaderboardItemVO;
            }

            myResult = null;

            if (userRank != -1 && (userRank > leaderboardData.item.length())) {
                myResult = new LeaderboardItemVO(userRank, applicationModel.userModel.name, userHighScore, userTimeSpend);
            }

            peroidVO = new PeroidVO(leaderboardData.@type, items, myResult);

            peroidVOs[i] = peroidVO;
        }

        // Set to screen
        screensLayerView.activeScreen.data = {periods:peroidVOs};

    }

    private function onErrorHandler():void {
        context.release(this);
        screensLayerView.activeScreen["onLoadingFailed"]();
    }
}
}
