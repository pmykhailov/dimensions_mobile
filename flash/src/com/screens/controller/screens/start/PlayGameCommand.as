package com.screens.controller.screens.start {
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
    import com.screens.view.layer.ScreensLayerView;
    import com.screens.view.screens.enum.ScreensTypeEnum;
    import com.screens.view.screens.game.GameScreen;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class PlayGameCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var screensLayer: ScreensLayerView;
        [Inject]
        public var applicationModel: ApplicationModel;

        public function PlayGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            if (applicationModel.gameRulesWereShown){
                dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.GAME}));

                var data: Object = {container: (screensLayer.activeScreen as GameScreen).gameContainer, highScore:applicationModel.userModel.highScore};
                dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.ADD_GAME_WRAPPER, data))

                dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.START_GAME));
                dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.GAME_MOUSE_CLICKABLE, false));

                dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.PLAY_READY_GO_ANIMATION));
            }else{
                applicationModel.gameRulesWereShown = true;
                applicationModel.soService.saveData("gameRulesWereShown", applicationModel.gameRulesWereShown);
                applicationModel.soService.flush();

                dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.TUTORIAL}));
            }

        }
    }
}
