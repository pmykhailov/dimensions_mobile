package com.screens.controller.screens.start {
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * ShowLevelCompleteScreen class.
     * User: Paul Makarenko
     * Date: 16.11.13
     */
    public class LeaderboardCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;

        public function LeaderboardCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.LEADERBOARD}));
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.GET_LEADERBOARD_DATA));
        }
    }
}