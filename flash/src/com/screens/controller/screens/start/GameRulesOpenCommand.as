package com.screens.controller.screens.start {
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * GameRulesOpenCommand class.
     * User: Paul Makarenko
     * Date: 08.11.13
     */
    public class GameRulesOpenCommand extends Command {

        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var applicationModel:ApplicationModel;

        public function GameRulesOpenCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            applicationModel.gameRulesWereShown = true;
            applicationModel.soService.saveData("gameRulesWereShown", applicationModel.gameRulesWereShown);
            applicationModel.soService.flush();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.GAME_RULES}));
        }
    }
}