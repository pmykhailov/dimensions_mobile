package com.screens.controller.screens.start {
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * LevelSelectionScreenOpenCommand class.
     * User: Paul Makarenko
     * Date: 12.10.13
     */
    public class SettingsOpenCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var applicationModel: ApplicationModel;


        public function SettingsOpenCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.SETTINGS, data: {soundVolume: applicationModel.settingsModel.soundVolume, musicVolume: applicationModel.settingsModel.musicVolume, name:applicationModel.userModel.name}}));
        }
    }
}