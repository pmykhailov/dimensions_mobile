/**
 * Created by Pasha on 07.08.2015.
 */
package com.screens.controller.screens.start {
import flash.net.URLRequest;
import flash.net.navigateToURL;

import robotlegs.bender.bundles.mvcs.Command;

public class MoreScreenCommand extends Command {


    public function MoreScreenCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var link:String = "https://play.google.com/store/apps/developer?id=Studio+MxM";
        navigateToURL(new URLRequest(link));
    }
}
}
