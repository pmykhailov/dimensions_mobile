package com.screens.controller.header {
import com.app.events.ApplicationEvent;
import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * ChangeMuteVolumeCommand class.
     * User: Paul Makarenko
     * Date: 07.11.13
     */
    public class ChangeMuteCommand extends Command {

        [Inject]
        public var event: ScreensContextEvent;
        [Inject]
        public var applicationModel: ApplicationModel;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function ChangeMuteCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            SoundAS.group(SoundModel.GROUP_MUSIC).masterVolume = applicationModel.settingsModel.musicVolume;
            SoundAS.group(SoundModel.GROUP_SFX).masterVolume = applicationModel.settingsModel.soundVolume;

            dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.SAVE_MODEL_TO_SHARED_OBJECT));
        }
    }
}