package com.screens.controller.header {
    import com.game_wrapper.model.level.LevelModel;
    import com.screens.events.ScreensContextEvent;
    import com.screens.view.layer.ScreensLayerView;
    import com.screens.view.screens.enum.ScreensTypeEnum;
    import com.screens.view.screens.game.GameScreen;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * BaseToMainMenuCommand class.
     * User: Paul Makarenko
     * Date: 08.11.13
     */
    public class BaseToMainMenuCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var screensLayerView: ScreensLayerView;
        [Inject]
        public var injector:Injector;


        public function BaseToMainMenuCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            if (screensLayerView.activeScreen is GameScreen) {
                var levelModel: LevelModel = injector.getInstance(LevelModel) as LevelModel;

                if (!levelModel.isPaused) {
                    dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.PAUSE_GAME));
                    dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.PAUSE_READY_GO_ANIMATION));
                }

                dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_ALERT));
            } else {
                dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.START}));
            }
        }
    }
}