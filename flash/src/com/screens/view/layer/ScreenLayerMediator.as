package com.screens.view.layer {
    import com.screens.events.ScreensContextEvent;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * ScreenLayerMediator class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreenLayerMediator extends Mediator {

        [Inject]
        public var _screensLayer:ScreensLayerView;

        public function ScreenLayerMediator() {
            super();
        }

        override public function initialize(): void {
            super.initialize();

            addContextListener(ScreensContextEvent.SHOW_SCREEN, onShowScreenHandler);

            addViewListener(ScreensContextEvent.SHOW_SCREEN, dispatch);

            // Enter name screen
            addViewListener(ScreensContextEvent.NAME_ENTERED, dispatch);

            // Loading screen
            addViewListener(ScreensContextEvent.RECONNECT, dispatch);
            addViewListener(ScreensContextEvent.LOADING_COMPLETE_ANIMTION_COMPLETE, dispatch);

            // Start screen events
            addViewListener(ScreensContextEvent.MAIN_MENU_PLAY, dispatch);
            addViewListener(ScreensContextEvent.MAIN_MENU_LEADERBOARD, dispatch);
            addViewListener(ScreensContextEvent.MAIN_MENU_SETTINGS, dispatch);
            addViewListener(ScreensContextEvent.MAIN_MENU_GAME_RULES, dispatch);
            addViewListener(ScreensContextEvent.MAIN_MENU_MORE_GAMES, dispatch);

            // Level complete screen
            addViewListener(ScreensContextEvent.HOME, dispatch);

            // Leaderboard screen
            addViewListener(ScreensContextEvent.LEADERBOARD_CLOSE, dispatch);

            // Settings screen events
            addViewListener(ScreensContextEvent.SETTINGS_CLOSE, dispatch);
            addViewListener(ScreensContextEvent.VOLUME_VALUE_CHANGED, dispatch);
            addViewListener(ScreensContextEvent.NAME_CHANGED, dispatch);

            // Game rules screen events
            addViewListener(ScreensContextEvent.GAME_RULES_CLOSE, dispatch);

            // Congratulations screen
            addViewListener(ScreensContextEvent.SHOW_LEVEL_COMPLETE_SCREEN, dispatch);
        }

        private function onShowScreenHandler(event: ScreensContextEvent): void {
            _screensLayer.showScreen(event.data);
        }
    }
}