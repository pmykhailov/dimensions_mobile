package com.screens.view.layer {
    import by.lord_xaoca.robotlegs2.BaseView;

    import com.screens.events.ScreensContextEvent;
    import com.screens.view.factory.ScreensFactory;
    import com.screens.view.screens.base.BaseScreen;

    import flash.display.Sprite;

    /**
     * ScreensManager class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensLayerView extends BaseView {

        private var _activeScreen: BaseScreen;
        private var _factory: ScreensFactory;


        public function ScreensLayerView() {
            super(new Sprite());
        }


        public function get activeScreen(): BaseScreen {
            return _activeScreen;
        }


        public function showScreen(screenData: Object): void {
            if (_activeScreen) {
                //_activeScreen.hide();
                _activeScreen.destroy();
            }

            _activeScreen = _factory.createScreen(screenData.type);
            if (screenData.hasOwnProperty("data")) {
                _activeScreen.data = screenData.data;
            }

            addActiveScreenListeners();
            //_activeScreen.show();

            addChild(_activeScreen.view);
        }


        override protected function initialize(): void {
            super.initialize();

            _factory = new ScreensFactory();
        }


        private function addActiveScreenListeners(): void {
            _activeScreen.addEventListener(ScreensContextEvent.SHOW_SCREEN, dispatchEvent);

            // Enter name screen events
            _activeScreen.addEventListener(ScreensContextEvent.NAME_ENTERED, dispatchEvent);

            // Loading screen events
            _activeScreen.addEventListener(ScreensContextEvent.RECONNECT, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.LOADING_COMPLETE_ANIMTION_COMPLETE, dispatchEvent);

            // Start screen events
            _activeScreen.addEventListener(ScreensContextEvent.MAIN_MENU_PLAY, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.MAIN_MENU_LEADERBOARD, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.MAIN_MENU_SETTINGS, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.MAIN_MENU_GAME_RULES, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.MAIN_MENU_MORE_GAMES, dispatchEvent);

            // Level screen events
            _activeScreen.addEventListener(ScreensContextEvent.HOME, dispatchEvent);

            // Leaderboard screen events
            _activeScreen.addEventListener(ScreensContextEvent.LEADERBOARD_CLOSE, dispatchEvent);

            // Settings screen events
            _activeScreen.addEventListener(ScreensContextEvent.SETTINGS_CLOSE, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.VOLUME_VALUE_CHANGED, dispatchEvent);
            _activeScreen.addEventListener(ScreensContextEvent.NAME_CHANGED, dispatchEvent);

            // Game rules screen events
            _activeScreen.addEventListener(ScreensContextEvent.GAME_RULES_CLOSE, dispatchEvent);

            // Congratulations screen
            _activeScreen.addEventListener(ScreensContextEvent.SHOW_LEVEL_COMPLETE_SCREEN, dispatchEvent);
        }
    }
}