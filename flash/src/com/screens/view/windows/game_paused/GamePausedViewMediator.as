package com.screens.view.windows.game_paused {
import com.screens.view.windows.alert.*;
    import com.screens.events.ScreensContextEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * AlertMediator class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class GamePausedViewMediator extends Mediator {

        [Inject]
        public var gamePaused:GamePausedView;

        public function GamePausedViewMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();

            addViewListener(ScreensContextEvent.GAME_PAUSED_HOME, dispatch);
            addViewListener(ScreensContextEvent.GAME_PAUSED_CONTINUE, dispatch);
            addViewListener(ScreensContextEvent.VOLUME_VALUE_CHANGED, dispatch);
        }
    }
}