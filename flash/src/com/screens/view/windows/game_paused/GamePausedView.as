package com.screens.view.windows.game_paused {
    import by.lord_xaoca.robotlegs2.BaseView;
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
import com.screens.view.screens.settings.SoundSettings;

import flash.events.MouseEvent;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * Alert class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class GamePausedView extends BaseView {

        private var _continueButton: LabelButton;
        private var _homeButton: LabelButton;
        private var _soundSettings:SoundSettings;


        public function GamePausedView() {
            super(new View_GamePaused());
        }


        public function set data(value:Object):void {
            _soundSettings.data = value;
        }

        override protected function initView(): void {
            super.initView();

            getTextField("text").text = Language.GAME_PAUSED;

            _continueButton = new LabelButton(getMovieClip("continue"));
            _continueButton.label = "CONTINUE";
            _continueButton.addViewListener(MouseEvent.CLICK, onContinueClickHandler);

            _homeButton = new LabelButton(getMovieClip("home"));
            _homeButton.label = "HOME";
            _homeButton.addViewListener(MouseEvent.CLICK, onHomeClickHandler);

            _soundSettings = new SoundSettings(getSprite("soundSettings"));
            _soundSettings.addEventListener(ScreensContextEvent.VOLUME_VALUE_CHANGED, dispatchEvent);
        }


        private function onContinueClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.GAME_PAUSED_CONTINUE));
        }


        private function onHomeClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.GAME_PAUSED_HOME));
        }

        override public function destroy():void {
            super.destroy();

            _continueButton.removeViewListener(MouseEvent.CLICK, onContinueClickHandler);
            _homeButton.removeViewListener(MouseEvent.CLICK, onHomeClickHandler);

            _soundSettings.removeViewListener(ScreensContextEvent.VOLUME_VALUE_CHANGED, dispatchEvent);
        }
    }
}