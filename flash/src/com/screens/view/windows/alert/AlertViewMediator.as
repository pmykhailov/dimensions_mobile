package com.screens.view.windows.alert {
    import com.screens.events.ScreensContextEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * AlertMediator class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class AlertViewMediator extends Mediator {

        [Inject]
        public var alert:AlertView;

        public function AlertViewMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();

            addViewListener(ScreensContextEvent.ALERT_CONFIRM, dispatch);
            addViewListener(ScreensContextEvent.ALERT_CANCEL, dispatch);
        }
    }
}