package com.screens.view.windows.alert {
    import by.lord_xaoca.robotlegs2.BaseView;
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;

    import flash.events.MouseEvent;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * Alert class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class AlertView extends BaseView {

        private var _confirmButton: LabelButton;
        private var _cancelButton: LabelButton;


        public function AlertView() {
            super(new View_Alert());
        }


        override protected function initView(): void {
            super.initView();

            getTextField("text").text = "DO YOU REALLY WANT TO QUIT?";

            _confirmButton = new LabelButton(getMovieClip("confirmButton"));
            _confirmButton.label = "YES";
            _confirmButton.addViewListener(MouseEvent.CLICK, onConfirmClickHandler);

            _cancelButton = new LabelButton(getMovieClip("cancelButton"));
            _cancelButton.label = "NO";
            _cancelButton.addViewListener(MouseEvent.CLICK, onCancelClickHandler);
        }


        private function onConfirmClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ALERT_CONFIRM));
        }


        private function onCancelClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ALERT_CANCEL));
        }
    }
}