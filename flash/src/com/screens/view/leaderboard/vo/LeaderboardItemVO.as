/**
 * Created by Paul on 7/25/16.
 */
package com.screens.view.leaderboard.vo {
public class LeaderboardItemVO {
    
    private var _position:int;
    private var _name:String;
    private var _score:int;
    private var _isHighlighted:Boolean;
    private var _timePlayed:int;

    public function LeaderboardItemVO(position:int, name:String, score:int, timePlayed:int = 0, isHighlighted:Boolean = false) {
        _position = position;
        _name = name;
        _score = score;
        _timePlayed = timePlayed;
        _isHighlighted = isHighlighted;
    }

    public function get position():int {
        return _position;
    }

    public function get name():String {
        return _name;
    }

    public function get score():int {
        return _score;
    }

    public function get isHighlighted():Boolean {
        return _isHighlighted;
    }

    public function get timePlayed():int {
        return _timePlayed;
    }
}
}
