package com.screens.view.leaderboard.vo {
public class PeroidVO {
    
    private var _type:String;
    private var _items:Array;
    private var _myResults:LeaderboardItemVO;
    
    public function PeroidVO(type:String, items:Array, myResults:LeaderboardItemVO = null) {
        _type = type;
        _items = items;
        _myResults = myResults;
    }

    public function get type():String {
        return _type;
    }

    public function get items():Array {
        return _items;
    }

    public function get myResults():LeaderboardItemVO {
        return _myResults;
    }
}
}
