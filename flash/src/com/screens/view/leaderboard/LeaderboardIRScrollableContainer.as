package com.screens.view.leaderboard {
import by.lord_xaoca.helpers.StageReference;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.MouseEvent;

public class LeaderboardIRScrollableContainer extends Sprite {

    private var _content:DisplayObject;
    private var _contentHolderMask:Sprite;
    private var _contentHolder:Sprite;
    private var _topOverlay:Sprite;
    private var _bottomOverlay:Sprite;
    private var _background:Sprite;

    private var _visibleWidth:Number;
    private var _visibleHeight:Number;

    private var _startY:Number;
    private var _currentY:Number;
    private var _initY:Number;

    public function LeaderboardIRScrollableContainer() {
        super();

        init();
    }

    public function set content(value:DisplayObject):void {
        _content = value;
        _contentHolder.addChild(_content);
        
        onContentSetted();
    }

    public function resetScroll():void {
        _content.y = _initY;
        _topOverlay.alpha = 0;
        _bottomOverlay.alpha = 1;
    }
    
    private function onContentSetted():void {
        _initY = _content.y;
    }

    private function init():void {

        _background = new View_ScrollableContainerBackground();
        addChild(_background);


        _visibleWidth = 606;
        _visibleHeight = 460;

        _contentHolder = new Sprite();
        _contentHolder.x = 32;
        _contentHolder.y = 28;
        addChild(_contentHolder);

        _contentHolderMask = new Sprite();
        _contentHolderMask.graphics.beginFill(0xFF0000,1);
        _contentHolderMask.graphics.drawRect(0,0,_visibleWidth, _visibleHeight);
        _contentHolderMask.graphics.endFill();
        _contentHolderMask.x = _contentHolder.x;
        _contentHolderMask.y = _contentHolder.y;
        addChild(_contentHolderMask);
        _contentHolder.mask = _contentHolderMask;

        _topOverlay = new View_LeaderboardOverlay();
        _topOverlay.mouseChildren = _topOverlay.mouseEnabled = false;
        _topOverlay.rotation = 180;
        _topOverlay.x = _background.width;
        _topOverlay.y = _topOverlay.height;
        _topOverlay.alpha = 0;
        addChild(_topOverlay);
        
        _bottomOverlay = new View_LeaderboardOverlay();
        _bottomOverlay.mouseChildren = _bottomOverlay.mouseEnabled = false;
        _bottomOverlay.x = 0;
        _bottomOverlay.y = _background.height - _bottomOverlay.height;
        _bottomOverlay.alpha = 0;
        addChild(_bottomOverlay);

        addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
    }

    private function onMouseDown(event:MouseEvent):void {
        if (_content.height > _visibleHeight) {
            StageReference.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
            StageReference.stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);

            _startY = event.stageY;
            _currentY = _content.y;
        }
    }

    private function onMouseMove(event:MouseEvent):void {
        var delta:Number = event.stageY - _startY;

        _content.y = _currentY + delta;

        _topOverlay.alpha = 1;
        _bottomOverlay.alpha = 1;
        
        if (_content.y > _initY) {
            _content.y = _initY;
            
            _topOverlay.alpha = 0;
        }

        if (_content.y < _initY - (_content.height - _visibleHeight)) {
            _content.y = _initY - (_content.height - _visibleHeight);

            _bottomOverlay.alpha = 0;
        }
    }

    private function onMouseUp(event:MouseEvent):void {
        StageReference.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
        StageReference.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
    }

    public function destroy():void {
        removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
        StageReference.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
        StageReference.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
    }
}
}
