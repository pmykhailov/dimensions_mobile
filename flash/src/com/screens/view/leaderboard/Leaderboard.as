package com.screens.view.leaderboard {
import by.lord_xaoca.ui.components.buttons.LabelButton;
import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

import flash.display.MovieClip;

import flash.display.Sprite;
import flash.events.MouseEvent;

public class Leaderboard extends BaseDisplayObjectContainer{

    private var _itemsScrolableContainer:LeaderboardIRScrollableContainer;
    private var _itemsContainer:Sprite;
    private var _items:Array;
    [ArrayElementType("by.lord_xaoca.ui.components.buttons.LabelButton")]
    private var _periodButtons:Array;
    private var _footer:LeaderboradFooter;

    [ArrayElementType("com.screens.view.leaderboard.vo.PeroidVO")]
    private var _periodsVOs:Array;
    private var _selectedPeriod:int;

    public function Leaderboard() {
        super(new View_Leaderboard());
    }

    override protected function initView():void {
        super.initView();

        _itemsContainer = new Sprite();

        _itemsScrolableContainer = new LeaderboardIRScrollableContainer();
        _itemsScrolableContainer.content = _itemsContainer;
        _itemsScrolableContainer.resetScroll();
        _itemsScrolableContainer.x = 0;
        _itemsScrolableContainer.y = 0;
        addChild(_itemsScrolableContainer);

        _footer = new LeaderboradFooter();
        _footer.x = 0;
        _footer.y = _itemsScrolableContainer.y + _itemsScrolableContainer.height - 1;
        (_footer.view as MovieClip).gotoAndStop("_default");

        addChild(_footer.view);
    }

    public function set data(value:Object):void {
        _periodsVOs = value.periods;

        _periodButtons = [];
        for (var i:int = 0; i < _periodsVOs.length; i++) {
            _periodButtons[i] = new LabelButton(new View_PeriodButton());
            _periodButtons[i].x = _itemsScrolableContainer.width - (_periodsVOs.length - i) * _periodButtons[i].width - 1;
            _periodButtons[i].y = 0;
            _periodButtons[i].label = (_periodsVOs[i].type as String).toUpperCase();
            _periodButtons[i].addEventListener(MouseEvent.CLICK, onPeriodButtonClick);

            addChild(_periodButtons[i].view);
        }

        _itemsScrolableContainer.y = 70;
        _footer.y = _itemsScrolableContainer.y + _itemsScrolableContainer.height - 1;

        selectedPeriod = value.hasOwnProperty("selectedPeriod") ? value.selectedPeriod : 0;
    }

    override public function destroy():void {
        super.destroy();

        for (var i:int = 0; i < _periodsVOs.length; i++) {
            _periodButtons[i].removeEventListener(MouseEvent.CLICK, onPeriodButtonClick);
        }
    }

    private function set selectedPeriod(value:int):void {
        _selectedPeriod = value;

        // Select button
        for (var i:int = 0; i < _periodsVOs.length; i++) {
           (_periodButtons[i].view as MovieClip).gotoAndStop("_unselected");
        }
        (_periodButtons[_selectedPeriod].view as MovieClip).gotoAndStop("_selected");

        // Clear items
        if (_items != null) {
            for (var i:int = 0; i < _items.length; i++) {
                _itemsContainer.removeChild(_items[i].view);
            }
        }

        // Add items
        _items = [];

        for (i = 0; i < _periodsVOs[value].items.length; i++) {
            _items[i] = new LeaderboardIR();
            _items[i].data = _periodsVOs[value].items[i];

            _items[i].x = 0;
            _items[i].y = i * _items[i].height;

            _itemsContainer.addChild(_items[i].view);
        }

        // My results
        if (_periodsVOs[_selectedPeriod].myResults) {
            (_footer.view as MovieClip).gotoAndStop("_userInfo");
            _footer.data = _periodsVOs[_selectedPeriod].myResults;
        } else {
            (_footer.view as MovieClip).gotoAndStop("_default");
        }

    }

    private function onPeriodButtonClick(event:MouseEvent):void {
        var label:String = event.target.label;
        
        for (var i:int = 0; i < _periodsVOs.length; i++) {
            if (_periodsVOs[i].type.toUpperCase() == label && _selectedPeriod != i) {
                selectedPeriod = i;
            }
        }

        _itemsScrolableContainer.resetScroll();
    }


}
}
