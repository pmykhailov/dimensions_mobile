package com.screens.view.leaderboard {
import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

import com.game_wrapper.misc.Format;

import flash.display.Sprite;
import flash.text.TextField;

public class LeaderboardIR extends BaseDisplayObjectContainer {

    public static const MAX_FITABLE_NAME_LENGHT:int = 15;

    public function LeaderboardIR(ViewClass:Class = null) {
        if (ViewClass == null) {
            ViewClass = View_LeaderboardIR;
        }
        super(new ViewClass());
    }

    public function set data(value:Object):void {
        // TODO make text smaller if it is not fit
        positionTf.text = value.position;
        nameTf.text = value.name.length > MAX_FITABLE_NAME_LENGHT ? value.name.substr(0,MAX_FITABLE_NAME_LENGHT - 3) + "..." :  value.name;
        scoreTf.text = value.score;
        timePlayedTf.text = Format.time(value.timePlayed);
        isHighlighted = value.isHighlighted;
    }

    private function set isHighlighted(value:Boolean) {
        highlight.visible = value;
    }

    private function get highlight():Sprite {
        return _view["highlight"] as Sprite;
    }
    
    private function get positionTf():TextField {
        return getTextField("position");
    }
    
    private function get nameTf():TextField {
        return getTextField("name");
    }

    private function get scoreTf():TextField {
        return getTextField("score");
    }

    private function get timePlayedTf():TextField {
        return getTextField("timePlayed");
    }

}
}
