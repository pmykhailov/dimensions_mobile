package com.screens.view.screens.tutorial {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

import com.game_wrapper.model.level.LevelModel;

import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.base.BaseScreen;
    import com.screens.view.screens.enum.ScreensTypeEnum;

    import flash.events.MouseEvent;

    import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    /**
     * LevelSelectionScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class TutorialScreen extends BaseScreen {
        protected var _okButton: LabelButton;


        public function TutorialScreen() {
            super(new View_TutorialScreen());
        }


        override protected function initView(): void {
            super.initView();
            _initTextFields();
            _initButtons();
        }


        protected function _initTextFields(): void {
            getTextField("title").text = Language.SCREEN_GAME_RULES_CAPTION;
            getTextField("rules").htmlText = "Draw a line to match figures of the same color or shape. Lines that consists of figures of the same shape give more scores. Long lines provide you with additional time bonus and scores. Draw lines quickly to activate x" + LevelModel.MULTIPLIER_VALUE +" score multiplier. <br/> Have fun and good luck!";
        }


        protected function _initButtons(): void {
            _okButton = new LabelButton(getSprite("okButton"));
            _okButton.addViewListener(MouseEvent.CLICK, onOkButtonClickHandler);
            _okButton.label = "PLAY";
        }


        protected function onOkButtonClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.MAIN_MENU_PLAY));
        }
    }
}