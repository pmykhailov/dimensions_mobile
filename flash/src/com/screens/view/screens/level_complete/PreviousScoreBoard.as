package com.screens.view.screens.level_complete {
    import by.lord_xaoca.ui.BaseUIElement;

    import com.game_wrapper.misc.Format;

    import flash.display.Sprite;

    /**
     * PreviousScoreBoard class.
     * User: Paul Makarenko
     * Date: 16.11.13
     */
    public class PreviousScoreBoard extends BaseUIElement {

        public function PreviousScoreBoard(view: Sprite) {
            super(view);
        }


        override protected function initView(): void {
            super.initView();

            getTextField("scoreColumnTitle").text = "SCORE";
            getTextField("timeColumnTitle").text = "TIME PLAYED";
        }


        override protected function _fillData(): void {
            super._fillData();

            var scores:Array = _data as Array;
            for (var i: int = 0; i < scores.length; i++) {
                getTextField("score_" + i).text = Format.score(scores[i].score);
                getTextField("time_" + i).text = Format.time(scores[i].time);
            }

            for (var j: int = scores.length; j < 5; j++) {
                getTextField("score_" + j).text = "-";
                getTextField("time_" + j).text = "-";
            }
        }
    }
}