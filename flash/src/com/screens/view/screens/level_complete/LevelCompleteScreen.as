package com.screens.view.screens.level_complete {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.game_wrapper.misc.Format;
    import com.greensock.TweenLite;

    import com.screens.events.ScreensContextEvent;
import com.screens.view.leaderboard.Leaderboard;
import com.screens.view.screens.base.BaseScreen;
    import com.screens.view.screens.enum.ScreensTypeEnum;
import com.share.ApplicationDimensions;

import flash.display.MovieClip;
import flash.display.Sprite;

import flash.events.MouseEvent;
import flash.filters.GlowFilter;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    /**
     * LevelSelectionScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class LevelCompleteScreen extends BaseScreen {
        private const SELECTED_FRAME_LABEL:String = "_selected";
        private const UNSELECTED_FRAME_LABEL:String = "_unselected";

        private var _playAgain: LabelButton;
        private var _home: LabelButton;
        private var _myHistory:LabelButton;
        private var _world:LabelButton;

        private var _previousScores:PreviousScoreBoard;
        private var _leaderboard:Leaderboard;

        private var _isLeaderboardLoaded:Boolean;
        private var _isLeaderboardLoadingFailed:Boolean;

        public function LevelCompleteScreen() {
            super(new View_LevelCompleteScreen());
        }


        public function onLoadingFailed():void {
            _isLeaderboardLoadingFailed = true;

           // _loading.y -= 20;

            _loading.filters = [errorTextFilter];
            //_loadingTextField.textColor = 0xFF0000;
            _loadingTextField.text = Language.LEADERBOARD_LOADING_FAILED;
        }

        override protected function initView(): void {
            super.initView();
            _initButtons();

            _previousScores = new PreviousScoreBoard(getSprite("previousScores"))
            _initLeaderboard();
        }


        override protected function _fillData(): void {
            super._fillData();

            if (data.hasOwnProperty("scores")) {
                _resultTextField.htmlText = "In " + Format.time(_data.time as int) + " you scored" + "<br/>" + Format.score(_data.score as int); //9999999
                _previousScores.data = _data.scores;
            }

            if (data.hasOwnProperty("periods")) {
                _leaderboard.data = _data;

                _leaderboard.x = (ApplicationDimensions.WIDTH - _leaderboard.width) / 2;
                _leaderboard.y = 275; //(ApplicationDimensions.HEIGHT - _leaderboard.height) / 2;

                _isLeaderboardLoaded = true;
                _loading.visible = false;
            }
        }

        private function _initLeaderboard():void {
            _leaderboard = new Leaderboard();

            _leaderboard.x = (ApplicationDimensions.WIDTH - _leaderboard.width) / 2;
            _leaderboard.y = (ApplicationDimensions.HEIGHT - _leaderboard.height) / 2;

            //_loadingTextField.textColor = 0xFFFFFF;
            _loading.filters = null;
            _loadingTextField.autoSize = TextFieldAutoSize.CENTER;
            _loadingTextField.text = Language.LEADERBOARD_LOADING_PROGRESS;
            addChild(_leaderboard.view);

            _isLeaderboardLoaded = false;

            hideLeaderboard();
        }

        private function _initButtons(): void {
            _myHistory = new LabelButton(getMovieClip("myHistoryButton"));
            _myHistory.addViewListener(MouseEvent.CLICK, onMyHistoryButtonClickHandler);
            getMovieClip("icon",_myHistory.view).gotoAndStop("my");
            (_myHistory.view as MovieClip).gotoAndStop(SELECTED_FRAME_LABEL);
            _myHistory.label = "MY HISTORY";

            _world = new LabelButton(getMovieClip("worldButton"));
            _world.addViewListener(MouseEvent.CLICK, onWorldButtonClickHandler);
            getMovieClip("icon",_world.view).gotoAndStop("world");
            (_world.view as MovieClip).gotoAndStop(UNSELECTED_FRAME_LABEL);
            _world.label = "WORLD";

            _home = new LabelButton(getSprite("home"));
            _home.addViewListener(MouseEvent.CLICK, onHomeClickHandler);
            _home.label = "HOME";

            _playAgain = new LabelButton(getSprite("playAgain"));
            _playAgain.addViewListener(MouseEvent.CLICK, onPlayAgainClickHandler);
            _playAgain.label = "REPLAY";
        }


        private function showLeaderboard():void {
            if (!_isLeaderboardLoaded || _isLeaderboardLoadingFailed) {
                _loading.visible = true;
            }

            _leaderboard.visible = true;
            _leaderboard.mouseChildren = _leaderboard.mouseEnabled = true;
        }

        private function hideLeaderboard():void {
            _loading.visible = false;
            _leaderboard.visible = false;
            _leaderboard.mouseChildren = _leaderboard.mouseEnabled = false;
        }

        private function showMyHistoryView():void {
            _resultTextField.visible = true;
            _previousScores.visible = true;
        }

        private function hideMyHistoryView():void {
            _resultTextField.visible = false;
            _previousScores.visible = false;
        }

        private function onMyHistoryButtonClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);


            (_myHistory.view as MovieClip).gotoAndStop(SELECTED_FRAME_LABEL);
            (_world.view as MovieClip).gotoAndStop(UNSELECTED_FRAME_LABEL);


            hideLeaderboard();
            showMyHistoryView();
        }

        private function onWorldButtonClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);


            (_myHistory.view as MovieClip).gotoAndStop(UNSELECTED_FRAME_LABEL);
            (_world.view as MovieClip).gotoAndStop(SELECTED_FRAME_LABEL);

            showLeaderboard();
            hideMyHistoryView();
        }

        private function onHomeClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.HOME));
        }

        private function onPlayAgainClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.MAIN_MENU_PLAY));
        }

        override public function destroy():void {
            super.destroy();

            _home.removeViewListener(MouseEvent.CLICK, onHomeClickHandler);
            _playAgain.removeViewListener(MouseEvent.CLICK, onPlayAgainClickHandler);
            _myHistory.removeViewListener(MouseEvent.CLICK, onMyHistoryButtonClickHandler);
            _world.removeViewListener(MouseEvent.CLICK, onWorldButtonClickHandler);
        }

        private function get _resultTextField():TextField {
            return getTextField("result");
        }

        private function get _loadingTextField():TextField {
            return getTextField("txt",_loading);
        }

        private function get _loading():Sprite {
            return getSprite("loadingAnimation");
        }

        private function get errorTextFilter():GlowFilter {
            return new GlowFilter(0xFF0000);
        }

    }
}