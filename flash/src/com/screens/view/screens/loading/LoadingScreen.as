package com.screens.view.screens.loading {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.base.BaseScreen;

import flash.display.DisplayObject;

import flash.display.MovieClip;
import flash.events.Event;

import flash.events.MouseEvent;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * StartScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class LoadingScreen extends BaseScreen {

        public static const STATE_LOADING:String = "_stateLoading";
        public static const STATE_LOADING_FAILED:String = "_stateLoadingFailed";
        public static const STATE_LOADING_COMPLETE:String = "_stateLoadingComplete";

        private var _state:String;

        public function LoadingScreen() {
            super(new View_LoadingScreen());
        }


        override protected function initView():void {
            super.initView();

            _view.addEventListener(Event.COMPLETE, onStudioLogoAnimationCompleteHandler);
        }


        override public function destroy():void {
            _view.removeEventListener(Event.COMPLETE, onStudioLogoAnimationCompleteHandler);

            super.destroy();
        }

        public function set state(value:String):void {
            beforeStateChange();

            _state = value;
            _loadingScreenView.gotoAndStop(value);

            afterStateChange();
        }

        private function beforeStateChange():void {
            if (_state == STATE_LOADING_FAILED) {
                _buttonRetry.removeEventListener(MouseEvent.CLICK, onRetryButtonClickHandler);
            }
        }

        private function afterStateChange():void {
            if (_state == STATE_LOADING_FAILED) {
                _buttonRetry.addEventListener(MouseEvent.CLICK, onRetryButtonClickHandler);
            }
        }

        protected function get _loadingScreenView():MovieClip {
            return _view as MovieClip;
        }

        private function get _buttonRetry():DisplayObject {
            return _view["_buttonRetry"] as DisplayObject;
        }

        private function onRetryButtonClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.RECONNECT));
        }

        private function onStudioLogoAnimationCompleteHandler(event:Event):void {
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LOADING_COMPLETE_ANIMTION_COMPLETE));
        }

    }
}