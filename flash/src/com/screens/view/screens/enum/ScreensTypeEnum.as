package com.screens.view.screens.enum  {

    /**
     * ScreensTypeEnum class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensTypeEnum {

        public static const START:String = "StartScreen";
        public static const LEADERBOARD:String = "LeaderboardScreen";
        public static const SETTINGS:String = "SettingsScreen";
        public static const GAME:String = "GameScreen";
        public static const LEVEL_COMPLETE:String = "LevelCompleteScreen";
        public static const TUTORIAL:String = "TutorialScreen";
        public static const GAME_RULES:String = "GameRulesScreen";
        public static const CONGRATULATIONS:String = "CongratulationsScreen";
        public static const LOADING:String = "LoadingScreen";
        public static const ENTER_NAME:String = "EnterName";

        public function ScreensTypeEnum() {
        }

    }
}