package com.screens.view.screens.game {
    import com.screens.view.screens.base.BaseScreen;

    import flash.display.Sprite;

    /**
     * LevelSelectionScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class GameScreen extends BaseScreen {
        private var _gameContainer: Sprite;


        public function GameScreen() {
            super(new View_GameScreen());
        }


        public function get gameContainer(): Sprite {
            return _gameContainer;
        }


        override protected function initView(): void {
            super.initView();
            _initGameContainer();
        }


        private function _initGameContainer(): void {
            _gameContainer = getSprite("gameWrapperContainer");
        }

    }
}