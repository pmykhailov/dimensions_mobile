package com.screens.view.screens.enter_name {
import by.lord_xaoca.ui.components.buttons.LabelButton;

import com.app.utils.NameUtils;

import com.greensock.TweenLite;
    import com.screens.events.ScreensContextEvent;
import com.screens.view.leaderboard.LeaderboardIR;
import com.screens.view.screens.base.BaseScreen;

import flash.events.Event;

import flash.events.MouseEvent;

import flash.events.TimerEvent;
import flash.text.TextField;
import flash.utils.Timer;

import treefortress.sound.SoundAS;
import treefortress.sound.SoundModel;

use namespace SoundAS;


    public class EnterNameScreen extends BaseScreen {

        private const INVALID_NAME_TEXT_BORDER:Number = 0xFF0000;
        private const VALID_NAME_TEXT_BORDER:Number = 0x000000;
        private const DEFAULT_NAME_TEXT_COLOR:Number = 0xCCCCCC;

        private var _continueButton:LabelButton;
        private var _nameTf:TextField;
        private var _generatedName:String;
        private var _index:int;

        public function EnterNameScreen() {
            super(new View_EnterNameScreen());
        }

        override protected function initView(): void {
            super.initView();
            _initButtons();
            _initTextFields();

            _view.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
        }

        private function _initTextFields():void {
            _nameTf = getTextField("name");

            do {
                _generatedName = NameUtils.generateDefaultName();
            } while (_generatedName.length > LeaderboardIR.MAX_FITABLE_NAME_LENGHT);

            _nameTf.text = "";
            //_nameTf.text = generatedName;
        }

        private function onAddedToStage(event:Event):void {
            _view.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

            _index = 0;
            _view.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
        }

        private function onEnterFrameHandler(event:Event):void {
            if (_index < _generatedName.length) {
                _index++;
                _nameTf.text = _generatedName.substr(0, _index);
            } else {
                _view.removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
            }
        }


        private function _initButtons(): void {
            _continueButton = new LabelButton(getMovieClip("continueButton"));
            _continueButton.label = "CONTINUE";
            _continueButton.addViewListener(MouseEvent.CLICK, onPlayClickHandler);
        }

        private function onPlayClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);

            if (_nameTf.text != "" && _nameTf.text.length > 1) {
                dispatchEvent(new ScreensContextEvent(ScreensContextEvent.NAME_ENTERED, {name:_nameTf.text}));
            } else {
                getMovieClip("backgroundAnimation").gotoAndPlay(2);
            }
        }

        override public function destroy(): void {
            _continueButton.removeEventListener(MouseEvent.CLICK, onPlayClickHandler);
            super.destroy()
        }
}
}