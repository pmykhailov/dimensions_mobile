package com.screens.view.screens.game_rules {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;

    import com.screens.view.screens.tutorial.TutorialScreen;

    import flash.events.MouseEvent;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * GameRulesScreen class.
     * User: Paul Makarenko
     * Date: 08.11.13
     */
    public class GameRulesScreen extends TutorialScreen {

        public function GameRulesScreen() {
            super();
        }


        override protected function _initButtons(): void {
            _okButton = new LabelButton(getSprite("okButton"));
            _okButton.addViewListener(MouseEvent.CLICK, onOkButtonClickHandler);
            _okButton.label = "BACK";
        }


        override protected function onOkButtonClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.GAME_RULES_CLOSE));
        }



    }
}