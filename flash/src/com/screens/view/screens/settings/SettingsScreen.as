package com.screens.view.screens.settings {
    import by.lord_xaoca.ui.components.buttons.LabelButton;
    import by.lord_xaoca.ui.components.scrolls.BaseScroll;
    import by.lord_xaoca.ui.components.scrolls.HorizontalScroller;

    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.base.BaseScreen;

    import flash.events.Event;
    import flash.events.MouseEvent;
import flash.text.TextField;

import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class SettingsScreen extends BaseScreen {

        protected var _okButton: LabelButton;
        private var _soundSettings:SoundSettings;
        private var _nameTf:TextField;
        private var _captionTf:TextField;

        public function SettingsScreen() {
            super(new View_SettingsScreen());
        }


        override protected function _fillData(): void {
            super._fillData();
            _soundSettings.data = data;
            _nameTf.text = data.name;
        }


        override protected function initialize(): void {
            super.initialize();
            _initTextFields();
            _initButtons();
            _initSoundSettings();
        }


        protected function _initButtons(): void {
            _okButton = new LabelButton(getSprite("backButton"));
            _okButton.label = "BACK";
            _okButton.addEventListener(MouseEvent.CLICK, onOKButtonClickHandler);
        }


        protected function _initTextFields(): void {
            _nameTf = getTextField("name");
            _nameTf.addEventListener(Event.CHANGE, onNameChange);

            _captionTf = getTextField("caption");
            _captionTf.text = Language.SCREEN_SETTINGS_CAPTION;
        }


        private function _initSoundSettings(): void {

            _soundSettings = new SoundSettings(getSprite("soundSettings"));
            _soundSettings.addEventListener(ScreensContextEvent.VOLUME_VALUE_CHANGED, dispatchEvent);
        }


        private function onNameChange(event:Event):void {
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.NAME_CHANGED, {name:_nameTf.text}));
        }

        protected function onOKButtonClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SETTINGS_CLOSE));
        }


        override public function destroy():void {
            super.destroy();

            _nameTf.removeEventListener(Event.CHANGE, onNameChange);

            _soundSettings.removeEventListener(ScreensContextEvent.VOLUME_VALUE_CHANGED, dispatchEvent);
            _soundSettings.destroy();
            _soundSettings = null;
        }
    }
}
