/**
 * Created by Pasha on 29.07.2016.
 */
package com.screens.view.screens.settings {
import by.lord_xaoca.ui.BaseUIElement;
import by.lord_xaoca.ui.components.scrolls.BaseScroll;
import by.lord_xaoca.ui.components.scrolls.HorizontalScroller;
import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

import com.screens.events.ScreensContextEvent;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;

import treefortress.sound.SoundAS;
import treefortress.sound.SoundModel;

use namespace SoundAS;

public class SoundSettings extends BaseUIElement {

    private var _musicScroller: HorizontalScroller;
    private var _soundScroller: HorizontalScroller;

    public function SoundSettings(view:Sprite) {
        super(view);
    }

    override protected function initialize():void {
        super.initialize();

        _musicScroller = new HorizontalScroller(getSprite("musicScroller"));
        _soundScroller = new HorizontalScroller(getSprite("soundScroller"));

        _musicScroller.addEventListener(Event.CHANGE, onMusicVolumeChangeHandler);
        _soundScroller.addEventListener(Event.CHANGE, onSoundVolumeChangeHandler);

        _soundScroller.addEventListener(BaseScroll.STOP_DRAG, onSoundStopDragHandler);
    }

    override protected function _fillData(): void {
        super._fillData();

        _musicScroller.value = data.musicVolume;
        _soundScroller.value = data.soundVolume;
        updateIcons();
    }


    private function onSoundStopDragHandler(event: Event): void {
        SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEMS_MATCHED);
    }

    private function onMusicVolumeChangeHandler(event: Event): void {
        dispatchEvent(new ScreensContextEvent(ScreensContextEvent.VOLUME_VALUE_CHANGED, {soundVolume: _soundScroller.value, musicVolume: _musicScroller.value}));
        updateIcons();
    }


    private function onSoundVolumeChangeHandler(event: Event): void {
        dispatchEvent(new ScreensContextEvent(ScreensContextEvent.VOLUME_VALUE_CHANGED, {soundVolume: _soundScroller.value, musicVolume: _musicScroller.value}));
        updateIcons();
    }


    private function updateIcons():void {
        getSprite("musicIcon").alpha = _musicScroller.value == 0 ? 0.5 : 1;
        getSprite("soundIcon").alpha = _soundScroller.value == 0 ? 0.5 : 1;
    }

    override public function destroy():void {
        super.destroy();

        _musicScroller.removeEventListener(Event.CHANGE, onMusicVolumeChangeHandler);
        _soundScroller.removeEventListener(Event.CHANGE, onSoundVolumeChangeHandler);

        _soundScroller.removeEventListener(BaseScroll.STOP_DRAG, onSoundStopDragHandler);
    }
}
}
