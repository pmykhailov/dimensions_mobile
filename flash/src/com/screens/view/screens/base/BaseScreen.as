package com.screens.view.screens.base {
    import by.lord_xaoca.ui.BaseUIElement;

    import flash.display.Sprite;

    /**
     * Screen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class BaseScreen extends BaseUIElement {

        public function BaseScreen(view: Sprite) {
            super(view);
        }


        public function show(): void {
            visible = true;
        }


        public function hide(): void {
            visible = false;
        }

    }
}