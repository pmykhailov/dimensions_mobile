package com.screens.view.screens.congratulations {
    import com.greensock.TweenLite;
    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.base.BaseScreen;

    import flash.events.TimerEvent;
    import flash.utils.Timer;

    /**
     * CongratulationsScreen class.
     * User: Paul Makarenko
     * Date: 16.11.13
     */
    public class CongratulationsScreen extends BaseScreen {

        private var _timer: Timer;


        public function CongratulationsScreen() {
            super(new View_CongratulationsScreen());
        }


        override public function destroy(): void {
            super.destroy()

            _stopTimer();
            TweenLite.killTweensOf(this);
        }


        override protected function initView(): void {
            super.initView();

            getTextField("result").htmlText = Language.CONGRATULATIONS;
        }


        override protected function initialize(): void {
            super.initialize();

            _timer = new Timer(3000);
            _timer.addEventListener(TimerEvent.TIMER, onTimerHandler);
            _timer.start();
        }


        private function _stopTimer(): void {
            _timer.removeEventListener(TimerEvent.TIMER, onTimerHandler);
            _timer.stop();
        }


        private function onTimerHandler(event: TimerEvent): void {
            _stopTimer();
            TweenLite.to(this, 0.7, {alpha:0, onComplete:onAnimationCompelte});
        }

        private function onAnimationCompelte():void{
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_LEVEL_COMPLETE_SCREEN));
        }
    }
}