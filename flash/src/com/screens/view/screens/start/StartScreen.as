package com.screens.view.screens.start {
    import by.lord_xaoca.ui.components.buttons.LabelButton;

    import com.screens.events.ScreensContextEvent;
    import com.screens.view.screens.base.BaseScreen;

    import flash.events.MouseEvent;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * StartScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class StartScreen extends BaseScreen {

        private var _playButton: LabelButton;
        private var _settingsButton: LabelButton;
        private var _leaderboardButton: LabelButton;
        private var _moreGamesButton: LabelButton;
        private var _rulesButton: LabelButton;


        public function StartScreen() {
            super(new View_StartScreen());
        }


        override protected function initView(): void {
            super.initView();
            _initButtons();
        }


        private function _initButtons(): void {
            _playButton = new LabelButton(getMovieClip("playButton"));
            _playButton.label = "PLAY";
            _playButton.addViewListener(MouseEvent.CLICK, onPlayClickHandler);

            _leaderboardButton = new LabelButton(getMovieClip("leaderboardButton"));
            _leaderboardButton.label = "LEADERS";
            _leaderboardButton.addViewListener(MouseEvent.CLICK, onLeaderboardClickHandler);

            _settingsButton = new LabelButton(getMovieClip("settingsButton"));
            _settingsButton.label = "SETTINGS";
            _settingsButton.addViewListener(MouseEvent.CLICK, onSettingsClickHandler);

            _rulesButton = new LabelButton(getMovieClip("rulesButton"));
            _rulesButton.label = "GAME RULES";
            _rulesButton.addViewListener(MouseEvent.CLICK, onGameRulesClickHandler);

            _moreGamesButton = new LabelButton(getMovieClip("moreGamesButton"));
            _moreGamesButton.label = "MORE GAMES";
            _moreGamesButton.addViewListener(MouseEvent.CLICK, onMoreGameClickHandler);

        }


        private function onLeaderboardClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.MAIN_MENU_LEADERBOARD));
        }

         private function onPlayClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.MAIN_MENU_PLAY));
        }


        private function onSettingsClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.MAIN_MENU_SETTINGS));
        }

        private function onGameRulesClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.MAIN_MENU_GAME_RULES));
        }


        private function onMoreGameClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.MAIN_MENU_MORE_GAMES));
        }

        override public function destroy():void {
            super.destroy();

            _playButton.removeViewListener(MouseEvent.CLICK, onPlayClickHandler);
            _leaderboardButton.removeViewListener(MouseEvent.CLICK, onLeaderboardClickHandler);
            _settingsButton.removeViewListener(MouseEvent.CLICK, onSettingsClickHandler);
            _rulesButton.removeViewListener(MouseEvent.CLICK, onGameRulesClickHandler);
            _moreGamesButton.removeViewListener(MouseEvent.CLICK, onMoreGameClickHandler);
        }
    }
}