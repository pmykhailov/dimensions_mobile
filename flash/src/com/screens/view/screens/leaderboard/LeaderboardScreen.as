package com.screens.view.screens.leaderboard {
    import by.lord_xaoca.ui.components.buttons.LabelButton;
    import by.lord_xaoca.ui.components.scrolls.BaseScroll;
    import by.lord_xaoca.ui.components.scrolls.HorizontalScroller;

    import com.screens.events.ScreensContextEvent;
import com.screens.view.leaderboard.Leaderboard;
import com.screens.view.leaderboard.vo.LeaderboardItemVO;
import com.screens.view.leaderboard.vo.PeroidVO;
import com.screens.view.screens.base.BaseScreen;
import com.share.ApplicationDimensions;

import flash.display.Sprite;

import flash.events.Event;
    import flash.events.MouseEvent;
import flash.filters.BitmapFilterQuality;
import flash.filters.DropShadowFilter;
import flash.filters.GlowFilter;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;


import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class LeaderboardScreen extends BaseScreen {

        protected var _okButton: LabelButton;
        private var _leaderboard:Leaderboard;

        public function LeaderboardScreen() {
            super(new View_LeaderboardScreen());
        }

        public function onLoadingFailed():void {
            //_loadingTextField.textColor = 0xFF0000;
            _loading.filters = [errorTextFilter];
            _loading.y -= 20;
            _loadingTextField.text = Language.LEADERBOARD_LOADING_FAILED;
        }


        override protected function _fillData(): void {
            super._fillData();

            _leaderboard.data = _data;

            _leaderboard.x = (ApplicationDimensions.WIDTH - _leaderboard.width) / 2;
            _leaderboard.y = (ApplicationDimensions.HEIGHT - _leaderboard.height) / 2;

            _loading.visible = false;
        }


        override protected function initialize(): void {
            super.initialize();
            _initTextFields();
            _initButtons();
            _initLeaderboard();
        }

        private function _initLeaderboard():void {
            _leaderboard = new Leaderboard();

            _leaderboard.x = (ApplicationDimensions.WIDTH - _leaderboard.width) / 2;
            _leaderboard.y = (ApplicationDimensions.HEIGHT - _leaderboard.height) / 2;

            //_loadingTextField.textColor = 0xFFFFFF;
            _loadingTextField.text = Language.LEADERBOARD_LOADING_PROGRESS;
            _loading.filters = null;
            addChild(_leaderboard.view);
        }

        private function get errorTextFilter():GlowFilter {
            return new GlowFilter(0xFF0000);
        }


        private function get _loadingTextField():TextField {
            return getTextField("txt",_loading);
        }

        private function get _loading():Sprite {
            return getSprite("loadingAnimation");
        }

        protected function _initButtons(): void {
            _okButton = new LabelButton(getSprite("backButton"));
            _okButton.label = "BACK";
            _okButton.addEventListener(MouseEvent.CLICK, onOKButtonClickHandler);
        }


        protected function _initTextFields(): void {
            getTextField("caption").text = Language.SCREEN_LEADERBOARD_CAPTION;

            _loadingTextField.autoSize = TextFieldAutoSize.CENTER;
        }


        protected function onOKButtonClickHandler(event: MouseEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.LEADERBOARD_CLOSE));
        }

    }
}
