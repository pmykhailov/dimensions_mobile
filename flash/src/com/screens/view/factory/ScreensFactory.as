package com.screens.view.factory {
    import com.screens.view.screens.base.BaseScreen;
    import com.screens.view.screens.congratulations.CongratulationsScreen;
import com.screens.view.screens.enter_name.EnterNameScreen;
import com.screens.view.screens.enum.ScreensTypeEnum;
    import com.screens.view.screens.game.GameScreen;
    import com.screens.view.screens.game_rules.GameRulesScreen;
import com.screens.view.screens.leaderboard.LeaderboardScreen;
import com.screens.view.screens.level_complete.LevelCompleteScreen;
import com.screens.view.screens.loading.LoadingScreen;
import com.screens.view.screens.settings.SettingsScreen;
    import com.screens.view.screens.start.StartScreen;
    import com.screens.view.screens.tutorial.TutorialScreen;

    import flash.utils.Dictionary;

    /**
     * ScreensFactory class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensFactory {

        private var screens: Dictionary;


        public function ScreensFactory() {
            screens = new Dictionary();

            screens[ScreensTypeEnum.START] = StartScreen;
            screens[ScreensTypeEnum.SETTINGS] = SettingsScreen;
            screens[ScreensTypeEnum.GAME] = GameScreen;
            screens[ScreensTypeEnum.LEVEL_COMPLETE] = LevelCompleteScreen;
            screens[ScreensTypeEnum.TUTORIAL] = TutorialScreen;
            screens[ScreensTypeEnum.GAME_RULES] = GameRulesScreen;
            screens[ScreensTypeEnum.LEADERBOARD] = LeaderboardScreen;
            screens[ScreensTypeEnum.CONGRATULATIONS] = CongratulationsScreen;
            screens[ScreensTypeEnum.LOADING] = LoadingScreen;
            screens[ScreensTypeEnum.ENTER_NAME] = EnterNameScreen;
        }


        public function createScreen(type: String): BaseScreen {
            return new screens[type]();
        }

    }
}