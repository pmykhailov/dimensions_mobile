package com.screens {
import com.game.events.GameContextEvent;
import com.game.events.GameControllEvent;
import com.game_wrapper.event.GameWrapperContextEvent;
    import com.screens.controller.LevelCompleteCommand;
    import com.screens.controller.game.PlayMultiplierActivatedAnimationCommand;
    import com.screens.controller.game.PlayMultiplierDeactivatedAnimationCommand;
    import com.screens.controller.header.BaseToMainMenuCommand;
import com.screens.controller.screens.start.MoreScreenCommand;
import com.screens.controller.windows.game_paused.GamePausedContinueCommand;
import com.screens.controller.windows.game_paused.GamePausedHomeCommand;
import com.screens.controller.windows.game_paused.HideGamePausedWindowCommand;
import com.screens.controller.windows.game_paused.ShowGamePausedWindowCommand;
import com.screens.controller.windows.quit_alert.HideQuitAlertCommand;
    import com.screens.controller.windows.quit_alert.QuitAlertCancelCommand;
    import com.screens.controller.windows.quit_alert.QuitAlertConfirmCommand;
    import com.screens.controller.windows.quit_alert.ShowQuitAlertCommand;
    import com.screens.controller.screens.InitScreensCommand;
    import com.screens.controller.header.ChangeMuteCommand;
    import com.screens.controller.screens.ShowLevelCompleteScreen;
    import com.screens.controller.screens.game_rules.GameRulesCloseCommand;
import com.screens.controller.screens.leaderboard.LeaderboardCloseCommand;
import com.screens.controller.screens.level_complete.HomeCommand;
import com.screens.controller.screens.settings.ChangeNameCommand;
import com.screens.controller.screens.settings.SettingsCloseCommand;
    import com.screens.controller.screens.settings.ChangeVolumeCommand;
    import com.screens.controller.screens.start.GameRulesOpenCommand;
import com.screens.controller.screens.common.GetLeaderboardDataCommand;
import com.screens.controller.screens.start.LeaderboardCommand;
import com.screens.controller.screens.start.PlayGameCommand;
    import com.screens.controller.screens.start.SettingsOpenCommand;
    import com.screens.controller.sounds.InitSoundsCommand;
    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;
    import com.screens.view.windows.alert.AlertViewMediator;
    import com.screens.view.windows.alert.AlertView;

    import com.screens.view.layer.ScreenLayerMediator;
    import com.screens.view.layer.ScreensLayerView;
    import com.game.events.GameExternalEvent;
import com.screens.view.windows.game_paused.GamePausedView;
import com.screens.view.windows.game_paused.GamePausedViewMediator;

import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;

    public class ScreensConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {
            // init
            eventCommandMap.map(ScreensContextEvent.INIT).toCommand(InitScreensCommand);
            eventCommandMap.map(ScreensContextEvent.INIT_SOUNDS).toCommand(InitSoundsCommand);

            // header
            eventCommandMap.map(ScreensContextEvent.BACK_TO_MAIN_MENU).toCommand(BaseToMainMenuCommand);
            eventCommandMap.map(ScreensContextEvent.MUTE_VALUE_CHANGED).toCommand(ChangeMuteCommand);

            eventCommandMap.map(ScreensContextEvent.SHOW_LEVEL_COMPLETE_SCREEN).toCommand(ShowLevelCompleteScreen);

            // screens
            eventCommandMap.map(ScreensContextEvent.GET_LEADERBOARD_DATA).toCommand(GetLeaderboardDataCommand);

            eventCommandMap.map(ScreensContextEvent.MAIN_MENU_SETTINGS).toCommand(SettingsOpenCommand);
            eventCommandMap.map(ScreensContextEvent.MAIN_MENU_MORE_GAMES).toCommand(MoreScreenCommand);
            eventCommandMap.map(ScreensContextEvent.MAIN_MENU_GAME_RULES).toCommand(GameRulesOpenCommand);
            eventCommandMap.map(ScreensContextEvent.MAIN_MENU_PLAY).toCommand(PlayGameCommand);
            eventCommandMap.map(ScreensContextEvent.MAIN_MENU_LEADERBOARD).toCommand(LeaderboardCommand);

            eventCommandMap.map(ScreensContextEvent.GAME_RULES_CLOSE).toCommand(GameRulesCloseCommand);
            eventCommandMap.map(ScreensContextEvent.SETTINGS_CLOSE).toCommand(SettingsCloseCommand);
            eventCommandMap.map(ScreensContextEvent.LEADERBOARD_CLOSE).toCommand(LeaderboardCloseCommand);
            eventCommandMap.map(ScreensContextEvent.VOLUME_VALUE_CHANGED).toCommand(ChangeVolumeCommand);
            eventCommandMap.map(ScreensContextEvent.NAME_CHANGED).toCommand(ChangeNameCommand);

            eventCommandMap.map(ScreensContextEvent.HOME).toCommand(HomeCommand);

            // alert
            eventCommandMap.map(ScreensContextEvent.SHOW_ALERT).toCommand(ShowQuitAlertCommand);
            eventCommandMap.map(ScreensContextEvent.HIDE_ALERT).toCommand(HideQuitAlertCommand);
            eventCommandMap.map(ScreensContextEvent.ALERT_CONFIRM).toCommand(QuitAlertConfirmCommand);
            eventCommandMap.map(ScreensContextEvent.ALERT_CANCEL).toCommand(QuitAlertCancelCommand);

            // game paused
            eventCommandMap.map(GameControllEvent.PAUSE_GAME).toCommand(ShowGamePausedWindowCommand);
            eventCommandMap.map(ScreensContextEvent.GAME_PAUSED_CONTINUE).toCommand(GamePausedContinueCommand);
            eventCommandMap.map(ScreensContextEvent.GAME_PAUSED_HOME).toCommand(GamePausedHomeCommand);
            eventCommandMap.map(ScreensContextEvent.GAME_PAUSED_HIDE).toCommand(HideGamePausedWindowCommand);

            // game
            eventCommandMap.map(GameExternalEvent.GAME_HIDE_COMPLETE).toCommand(LevelCompleteCommand);

            // game wrapper
            eventCommandMap.map(GameWrapperContextEvent.PLAY_MULTIPLIER_ACTIVATE_ANIMATION).toCommand(PlayMultiplierActivatedAnimationCommand);
            eventCommandMap.map(GameWrapperContextEvent.PLAY_MULTIPLIER_DEACTIVATE_ANIMATION).toCommand(PlayMultiplierDeactivatedAnimationCommand);
        }


        private function mapMediators(): void {
            mediatorMap.map(ScreensLayerView).toMediator(ScreenLayerMediator);
            mediatorMap.map(AlertView).toMediator(AlertViewMediator);
            mediatorMap.map(GamePausedView).toMediator(GamePausedViewMediator);
        }


        private function mapInjections(): void {
            injector.map(ScreensLayerView).toValue(new ScreensLayerView());
        }


        private function init(): void {

        }
    }
}
