package com.game_wrapper.model.level {
    import com.game_wrapper.event.GameWrapperContextEvent;

    /**
     * LevelModel class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class LevelModel extends MultiplierSupportLevelModel {

        public static const MULTIPLIER_VALUE:int = 5;

        private var _score: int;
        private var _isPaused: Boolean;

        public function LevelModel() {
            super();

            _init();
        }


        public function get score(): int {
            return _score;
        }


        public function set score(value: int): void {
            _score = value;
            dispatch(new GameWrapperContextEvent(GameWrapperContextEvent.SCORE_CHANGED, value));
        }


        public function get isPaused(): Boolean {
            return _isPaused;
        }


        public function set isPaused(value: Boolean): void {
            _isPaused = value;
        }


        override public function reset(): void {
            super.reset();
            _score = 0;
        }
    }
}