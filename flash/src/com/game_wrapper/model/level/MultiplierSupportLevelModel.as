package com.game_wrapper.model.level {
    import com.game_wrapper.event.GameWrapperContextEvent;

    /**
     * MultiplierSupportLevelModel class.
     * User: Paul Makarenko
     * Date: 15.11.13
     */
    public class MultiplierSupportLevelModel extends TimeLimitedLevelModel {

        private var _quicklyDrawnLinesCount: int;
        private var _lastQuicklyDrawnLineTime: Number;
        private var _multiplierMaxSteps: int;
        private var _multiplierCurrentStep: int;
        private var _allowMultiplierStepChanging: Boolean;
        private var _isMultiplierActivated: Boolean;
        private var _multiplierActiveTime: int;
        private var _multiplierCurrentTime: int;
        private var _multiplierValue: int;
        private var _multiplierDafaultValue: int;
        private var _shapeMatchMultiplierValue: int;


        public function MultiplierSupportLevelModel() {
            super();
        }


        public function get quicklyDrawnLinesCount(): int {
            return _quicklyDrawnLinesCount;
        }


        public function set quicklyDrawnLinesCount(value: int): void {
            _quicklyDrawnLinesCount = value;
        }


        public function get lastQuicklyDrawnLineTime(): Number {
            return _lastQuicklyDrawnLineTime;
        }


        public function set lastQuicklyDrawnLineTime(value: Number): void {
            _lastQuicklyDrawnLineTime = value;
        }


        public function get multiplierMaxSteps(): int {
            return _multiplierMaxSteps;
        }


        public function set multiplierMaxSteps(value: int): void {
            _multiplierMaxSteps = value;
        }


        public function get multiplierCurrentStep(): int {
            return _multiplierCurrentStep;
        }


        public function set multiplierCurrentStep(value: int): void {

            if (!_allowMultiplierStepChanging) return;

            if (value < 0) {
                value = 0;
            }

            if (value > _multiplierMaxSteps) {
                value = _multiplierMaxSteps;
            }

            _multiplierCurrentStep = value;

            dispatch(new GameWrapperContextEvent(GameWrapperContextEvent.MULTIPLIER_STEP_CHANGED, value));
        }


        public function get allowMultiplierStepChanging(): Boolean {
            return _allowMultiplierStepChanging;
        }


        public function set allowMultiplierStepChanging(value: Boolean): void {
            _allowMultiplierStepChanging = value;
        }


        public function get multiplierActiveTime(): int {
            return _multiplierActiveTime;
        }


        public function set multiplierActiveTime(value: int): void {
            _multiplierActiveTime = value;
        }


        public function get multiplierCurrentTime(): int {
            return _multiplierCurrentTime;
        }


        public function set multiplierCurrentTime(value: int): void {
            _multiplierCurrentTime = value;
            dispatch(new GameWrapperContextEvent(GameWrapperContextEvent.MULTIPLIER_CURRENT_TIME_CHANGED, value));
        }


        public function get isMultiplierActivated(): Boolean {
            return _isMultiplierActivated;
        }


        public function set isMultiplierActivated(value: Boolean): void {
            _isMultiplierActivated = value;
        }


        public function get multiplierValue(): int {
            return _multiplierValue;
        }


        public function set multiplierValue(value: int): void {
            _multiplierValue = value;
        }


        public function get multiplierDafaultValue(): int {
            return _multiplierDafaultValue;
        }


        public function set multiplierDafaultValue(value: int): void {
            _multiplierDafaultValue = value;
        }

        public function get shapeMatchMultiplierValue():int {
            return _shapeMatchMultiplierValue;
        }

        public function set shapeMatchMultiplierValue(value:int):void {
            _shapeMatchMultiplierValue = value;
        }

        override public function reset(): void {
            super.reset();
            _quicklyDrawnLinesCount = 0;
            _lastQuicklyDrawnLineTime = 0;
            _multiplierCurrentStep = 0;
            _allowMultiplierStepChanging = true;
            _isMultiplierActivated = false;
        }

    }
}