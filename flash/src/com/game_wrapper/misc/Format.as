package com.game_wrapper.misc {
    public class Format {
        /**
         * Format to MM:SS time
         * @param seconds
         * @return
         *
         */
        public static function time(seconds: uint): String {
            var sec: int = seconds % 60;
            var min: int = int(seconds / 60);

            return toXXFormat(min) + ":" + toXXFormat(sec);
        }


        public static function score(score:uint): String{
            var scoreString:String = score + "";
            var resReversed:String = "";
            var res:String = "";
            var counter:int;

            for (var i: int = scoreString.length - 1; i >= 0 ; i--) {
                resReversed += scoreString.charAt(i);
                counter++;
                if (counter == 3 && i!=0){
                    counter = 0;
                    resReversed += " ";
                }
            }

            for (var j: int = resReversed.length - 1; j >= 0; j--) {
                res += resReversed.charAt(j);
            }

            return res;
        }


        /**
         * Converts any 2 digit number to XX format
         * @param number
         * @return
         *
         */
        public static function toXXFormat(number: uint): String {
            var res: String;

            if (number > 9) {
                res = number + "";
            } else {
                res = "0" + number;
            }

            return res;
        }


        public function Format() {

        }

    }
}