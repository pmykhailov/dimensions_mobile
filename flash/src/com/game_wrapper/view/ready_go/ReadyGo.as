package com.game_wrapper.view.ready_go {
    import by.lord_xaoca.robotlegs2.BaseView;

    import com.game_wrapper.event.GameWrapperContextEvent;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.events.DataEvent;
    import flash.events.Event;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class ReadyGo extends BaseView {

        private var _isPaused:Boolean;

        public function ReadyGo(view: DisplayObject) {
            super(view);
        }


        private function get currentView(): MovieClip {
            return _view as MovieClip;
        }


        public function play(): void {
            addViewListener(Event.COMPLETE, onAnimationCompleteHandler);
            addViewListener("playSound", onPlaySoundHandler);

            currentView.gotoAndPlay(1);
            visible = true;
        }

        public function pause():void{
            if (visible){
                _isPaused = true;
                currentView.stop();
            }
        }


        public function resume():void{
            if (_isPaused){
                _isPaused = false;
                currentView.play();
            }
        }


        override protected function initView(): void {
            super.initView();

            currentView.gotoAndStop(1);
            visible = false;
        }


        private function onAnimationCompleteHandler(event: Event): void {
            removeEventListener(Event.COMPLETE, onAnimationCompleteHandler);
            removeEventListener("playSound", onPlaySoundHandler);

            currentView.gotoAndStop(1);
            visible = false;

            dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.READY_GO_ANIMATION_COMPLETE));
        }

        private function onPlaySoundHandler(event: DataEvent): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(event.data, 0.5);
        }

    }
}
