package com.game_wrapper.view.ready_go {
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.screens.events.ScreensContextEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * TopBarMediator class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class ReadyGoMediator extends Mediator {

        [Inject]
        public var readyGo: ReadyGo;


        public function ReadyGoMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();

            addContextListener(GameWrapperControllEvent.PLAY_READY_GO_ANIMATION, onPlayReadyGoAnimationHandler);
            addContextListener(GameWrapperControllEvent.PAUSE_READY_GO_ANIMATION, onPauseReadyGoAnimationHandler);
            addContextListener(GameWrapperControllEvent.RESUME_READY_GO_ANIMATION, onResumeReadyGoAnimationHandler);

            addViewListener(GameWrapperContextEvent.READY_GO_ANIMATION_COMPLETE, dispatch);
        }


        private function onPauseReadyGoAnimationHandler(event: GameWrapperControllEvent): void {
            readyGo.alpha = 0.5;
            readyGo.pause();
        }

        private function onResumeReadyGoAnimationHandler(event: GameWrapperControllEvent): void {
            readyGo.alpha = 1;
            readyGo.resume();
        }

        private function onPlayReadyGoAnimationHandler(event: GameWrapperControllEvent): void {
            readyGo.play();
        }

    }
}