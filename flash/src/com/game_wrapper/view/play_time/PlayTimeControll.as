package com.game_wrapper.view.play_time {
import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

import com.game_wrapper.misc.Format;
    import com.game_wrapper.view.top_bar.bars.base.BaseTimeBar;
    import com.greensock.TweenLite;
    import com.greensock.TweenMax;

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.text.TextField;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * TimeBar class.
     * User: Paul Makarenko
     * Date: 03.11.13
     */
    public class PlayTimeControll extends BaseDisplayObjectContainer {

        private var _timeBonus: TimeBonusView;
        private var _prevValue: int;
        private var _maxTime: int;
        private var _time:TextField;

        public function PlayTimeControll(view: DisplayObject) {
            super(view);
        }

        public function set maxTime(value: int): void {
            _maxTime = value;
            _prevValue = _maxTime;
            _time.text = Format.toXXFormat(value);
        }

        public function set time(value: int): void {

            _time.text = Format.toXXFormat(value);

            if (_prevValue < value) {
                // playTimeAddedAnimation will be called
            } else if (value < 10) {
                //TweenMax.to(this.view, 1, {glowFilter: {color: 0xFF0000, alpha: 1, blurX: 30, blurY: 30, remove: true}});
                TweenMax.to(this.view, 1, {colorMatrixFilter:{colorize:0xFF0000, amount:1, remove: true}});
            }

            if (value == 0) {
                //
            } else if (value < 4) {
                SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.TICK_SOUND);
            }

            _prevValue = value;
        }

        public function playTimeAddedAnimation(amount:int):void {
            TweenMax.to(this.view, 1, {colorMatrixFilter:{colorize:0x00FF00, amount:1, remove: true}});
            //TweenMax.to(this.view, 1, {glowFilter: {color: 0x00FF00, alpha: 1, blurX: 30, blurY: 30, remove: true}});

            _timeBonus.time = amount;
            _timeBonus.alpha = 0;
            _timeBonus.x = 120;

            TweenLite.to(_timeBonus, 0.5, {alpha: 1, onComplete: TweenLite.to, onCompleteParams: [_timeBonus, 1, {alpha: 0, x:0}]});
        }


        override protected function initView(): void {
            super.initView();

            _timeBonus = new TimeBonusView(getSprite("timeBonus"));
            _timeBonus.alpha = 0;

            _time = getTextField("time");
        }

    }
}