package com.game_wrapper.view.play_time {
import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

public class TimeBonusView extends BaseDisplayObjectContainer{

    private var _icon:Sprite;
    private var _amount:TextField;

    public function TimeBonusView(view: DisplayObject) {
        super(view);
    }

    override protected function initView():void {
        super.initView();

        _icon = getSprite("icon");
        _amount = getTextField("amount");
        _amount.autoSize = TextFieldAutoSize.LEFT;
    }

    public function set time(value:int):void{
        _amount.text = "+" + value + "";
        _icon.x = _amount.textWidth + iconAndTextGap;
    }

    public function get iconAndTextGap():int{
        return 8;
    }
}
}
