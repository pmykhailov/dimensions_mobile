package com.game_wrapper.view.game_warapper {
    import by.lord_xaoca.robotlegs2.BaseView;

    import com.game_wrapper.view.ready_go.ReadyGo;
    import com.game_wrapper.view.top_bar.TopBarView;

    import flash.display.MovieClip;

    /**
     * GameWrapper class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class GameWrapperView extends BaseView {

        private var _topBar: TopBarView;
        private var _readyGo: ReadyGo;
        private var _multiplierActivated: MovieClip;


        public function GameWrapperView() {
            super(new View_GameWrapper());
        }


        public function get topBar(): TopBarView {
            return _topBar;
        }


        public function get multiplierActivated(): MovieClip {
            return _multiplierActivated;
        }


        override public function destroy(): void {
            super.destroy();

            _topBar.destroy();
            _readyGo.destroy();
        }


        override protected function initView(): void {
            super.initView();

            mouseEnabled = false;

            _topBar = new TopBarView(getSprite("topBar"));

            _readyGo = new ReadyGo(getMovieClip("readyGo"));
            _readyGo.mouseChildren = false;
            _readyGo.mouseEnabled = false;

            _multiplierActivated = getMovieClip("multiplierActivated");
            _multiplierActivated.mouseChildren = false;
            _multiplierActivated.mouseEnabled = false;
        }
    }
}