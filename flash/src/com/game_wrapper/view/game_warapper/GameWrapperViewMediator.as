package com.game_wrapper.view.game_warapper {
    import com.game_wrapper.event.GameWrapperContextEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * GameWrapperViewMediator class.
     * User: Paul Makarenko
     * Date: 15.11.13
     */
    public class GameWrapperViewMediator extends Mediator {

        [Inject]
        public var gameWrapperView: GameWrapperView;


        public function GameWrapperViewMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();

            addContextListener(GameWrapperContextEvent.PLAY_MULTIPLIER_ACTIVATE_ANIMATION, onPlayMultiplierActivateAnimationHandler);
        }


        private function onPlayMultiplierActivateAnimationHandler(event: GameWrapperContextEvent): void {
            gameWrapperView.multiplierActivated.gotoAndPlay(2);
        }

    }
}