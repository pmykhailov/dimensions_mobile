package com.game_wrapper.view.top_bar {
    import by.lord_xaoca.robotlegs2.BaseView;
    import by.lord_xaoca.ui.components.buttons.DoubleStateButton;
import by.lord_xaoca.ui.components.buttons.MovieClipButton;

import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.misc.Format;
    import com.game_wrapper.view.top_bar.bars.multiplier.MultiplierBar;
    import com.game_wrapper.view.play_time.PlayTimeControll;
    import com.greensock.TweenLite;
    import com.share.ApplicationDimensions;

    import flash.display.DisplayObject;
    import flash.events.MouseEvent;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * TopBarView class.
     * User: Paul Makarenko
     * Date: 03.10.13
     */
    public class TopBarView extends BaseView {
        private var _pauseGameButton: MovieClipButton;
        private var _score: TextField;
        private var _bonusScore: TextField;
        private var _multiplier: TextField;
        private var _highScore: TextField;
        private var _timeControll: PlayTimeControll;
        private var _multiplierBar: MultiplierBar;


        public function TopBarView(view: DisplayObject) {
            super(view);
        }


        public function set score(value: uint): void {
            _score.text = Format.score(value);
        }

        public function set highScore(value: uint): void {
            _highScore.text = Format.score(value);
        }


        public function set multiplier(value: int): void {
            _multiplier.text = "X" + value;
        }


        public function set time(value: int): void {
            _timeControll.time = value;
        }

        public function playTimeAddedAnimation(amount:int): void {
            _timeControll.playTimeAddedAnimation(amount);
        }


        public function get timeControll(): PlayTimeControll {
            return _timeControll;
        }


        public function get multiplierBar(): MultiplierBar {
            return _multiplierBar;
        }


        public function playBonusScoreAnimation(scoreBonusAmount: int): void {
            _bonusScore.text = "+" + scoreBonusAmount;
            _bonusScore.x = _score.x + _score.textWidth + 5;
            _bonusScore.alpha = 0;
            TweenLite.to(_bonusScore, 0.5, {alpha: 1, onComplete:
                    function (): void {
                        TweenLite.to(_bonusScore, 0.7, {onComplete:
                                function():void{
                                    TweenLite.to(_bonusScore, 0.5, {alpha:0, x:_score.x + _score.textWidth - _bonusScore.textWidth});
                                }
                        });
                    }
            });
        }


        override protected function initView(): void {
            super.initView();

            _initButtons();
            _initTextFields();
            _initTimeBar();
            _initMultiplierBar();
        }


        private function _initMultiplierBar(): void {
            _multiplierBar = new MultiplierBar(getSprite("multiplierBar"));
        }


        private function _initTimeBar(): void {
            _timeControll = new PlayTimeControll(getSprite("timeControll"));
            _timeControll.mouseChildren = false;
            _timeControll.mouseEnabled = false;
        }


        private function _initTextFields(): void {
            _score = getTextField("score");
            _score.autoSize = TextFieldAutoSize.CENTER;
            _score.text = "";

            _bonusScore = getTextField("bonusScore");
            _bonusScore.autoSize = TextFieldAutoSize.CENTER;
            _bonusScore.alpha = 0;

            _multiplier = getTextField("multiplier");
            _multiplier.autoSize = TextFieldAutoSize.CENTER;

            _highScore = getTextField("highScore");
        }


        private function _initButtons(): void {
            _pauseGameButton = new MovieClipButton(getMovieClip("pauseGameButton"));
            _pauseGameButton.addEventListener(MouseEvent.CLICK, onPauseClickHandler)
        }


        private function onPauseClickHandler(event: MouseEvent): void {

            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.BUTTON_CLICK);

            dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.TOP_BAR_PAUSE_CLICK));
        }
    }
}