package com.game_wrapper.view.top_bar {
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperContextEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    /**
     * TopBarMediator class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class TopBarMediator extends Mediator {

        [Inject]
        public var topBarView: TopBarView;


        public function TopBarMediator() {
            super();
        }


        override public function initialize(): void {
            super.initialize();

            addContextListener(GameWrapperContextEvent.SCORE_CHANGED, onScoreChangedHandler);
            addContextListener(GameWrapperContextEvent.TIME_CHANGED, onTimeChangedHandler);
            addContextListener(GameWrapperContextEvent.TIME_ADDED, onTimeAddedHandler);
            addContextListener(GameWrapperContextEvent.MULTIPLIER_STEP_CHANGED, onMultiplierStepsChangedHandler);
            addContextListener(GameWrapperContextEvent.MULTIPLIER_CURRENT_TIME_CHANGED, onMultiplierCurrentTimeChangedHandler);
            addContextListener(GameWrapperContextEvent.PLAY_SCORE_BONUS_ANIMATION, onPlayScoreBonusAnimationHandler);
            addContextListener(GameControllEvent.CHANGE_MULTIPLIER, onMultiplierChangeHandler);

            addViewListener(GameWrapperContextEvent.TOP_BAR_PAUSE_CLICK, dispatch);
            addViewListener(GameWrapperContextEvent.TOP_BAR_RESUME_CLICK, dispatch);

            addViewListener(GameControllEvent.START_GAME, dispatch);
            addViewListener(GameControllEvent.PAUSE_GAME, dispatch);
            addViewListener(GameControllEvent.RESUME_GAME, dispatch);

            addViewListener(GameWrapperContextEvent.CLOSE_GAME, dispatch);
            addViewListener(GameWrapperContextEvent.RESTART_GAME, dispatch);
        }


        private function onPlayScoreBonusAnimationHandler(event: GameWrapperContextEvent): void {
            topBarView.playBonusScoreAnimation(event.data as int);
        }


        private function onScoreChangedHandler(event: GameWrapperContextEvent): void {
            topBarView.score = event.data as int;
        }


        private function onTimeChangedHandler(event: GameWrapperContextEvent): void {
            topBarView.time = event.data as int;
        }

        private function onTimeAddedHandler(event: GameWrapperContextEvent): void {
            topBarView.playTimeAddedAnimation(event.data as int);
        }


        private function onMultiplierStepsChangedHandler(event: GameWrapperContextEvent): void {
            topBarView.multiplierBar.currentStep = event.data as int;
        }


        private function onMultiplierCurrentTimeChangedHandler(event: GameWrapperContextEvent): void {
            topBarView.multiplierBar.time = event.data as int;
        }


        private function onMultiplierChangeHandler(event: GameControllEvent): void {
            topBarView.multiplier = event.data as int;
        }

    }
}