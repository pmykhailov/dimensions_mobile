package com.game_wrapper.view.top_bar.bars.multiplier {
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import com.game_wrapper.view.top_bar.bars.base.BaseTimeBar;

    import com.greensock.TweenLite;
    import com.greensock.easing.Linear;

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.text.TextField;

    /**
     * MultiplierBar class.
     * User: Paul Makarenko
     * Date: 15.11.13
     */
    public class MultiplierBar extends BaseTimeBar {

        private var _eraser: Sprite;
        private var _maxEraserWidth: int;

        private var _maxStep: int;

        public function MultiplierBar(view: DisplayObject) {
            super(view);
        }


        public function set maxStep(value: int): void {
            _maxStep = value;
        }


        public function set currentStep(value: int): void {
            var targetWidth: Number = (1 - value / _maxStep) * _maxEraserWidth;
            TweenLite.to(_eraser, 1, {width: targetWidth, ease: Linear.easeNone});
        }


        override protected function initView(): void {
            super.initView();

            _eraser = getSprite("eraser");
            _maxEraserWidth = _eraser.width;
        }


        override protected function _initTimeHolder(): void {
            super._initTimeHolder();
            _timeTextHolder.alpha = 0;
        }
    }
}