package com.game_wrapper.view.top_bar.bars.base {
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import com.game_wrapper.misc.Format;
    import com.greensock.TweenLite;

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.text.TextField;

    /**
     * BaseTimeBar class.
     * User: Paul Makarenko
     * Date: 15.11.13
     */
    public class BaseTimeBar extends BaseDisplayObjectContainer {

        protected var _timeTextHolder: Sprite;
        protected var _timeTextField: TextField;
        protected var _minX: int;
        protected var _maxX: int;
        protected var _maxTime: int;
        protected var _prevValue: int;
        protected var _targetXForAnimation: Number;


        public function BaseTimeBar(view: DisplayObject) {
            super(view);
        }


        public function set maxTime(value: int): void {
            _maxTime = value;
            _prevValue = _maxTime;
            _timeTextField.text = Format.toXXFormat(value);
        }


        public function set time(value: int): void {
            _timeTextField.text = Format.toXXFormat(value);

            _targetXForAnimation = _minX + (_maxX - _minX) * (value / _maxTime);

            TweenLite.to(_timeTextHolder, 0.3, {x: _targetXForAnimation, alpha: value == 0 ? 0 : 1, onComplete: value == 0 ? setStartPosition : null});
        }


        override protected function initView(): void {
            super.initView();

            _initTimeHolderCoordinates();
            _initTimeHolder();
        }


        protected function _initTimeHolder(): void {
            _timeTextHolder = getSprite("timeTextHolder");
            _timeTextField = getTextField("time", _timeTextHolder);
            setStartPosition();
        }


        protected function setStartPosition(): void {
            _timeTextHolder.x = _maxX;
        }


        protected function _initTimeHolderCoordinates(): void {
            _minX = 18;
            _maxX = 504;
        }
    }
}