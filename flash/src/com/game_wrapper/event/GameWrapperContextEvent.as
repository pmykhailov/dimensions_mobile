package com.game_wrapper.event {
    import com.game.events.*;

    import flash.events.Event;

public class GameWrapperContextEvent extends Event {

    public static const SCORE_CHANGED:String = "scoreChanged";
    public static const TIME_CHANGED:String = "timeChanged";
    public static const TIME_IS_UP:String = "timeIsUp";
    public static const TIME_ADDED:String = "timeAdded";
    public static const CLOSE_GAME:String = "closeGame";
    public static const RESTART_GAME:String = "restartGame";
    public static const READY_GO_ANIMATION_COMPLETE:String = "readyGoAnimationComplete";
    public static const TOP_BAR_PAUSE_CLICK:String = "topBarPauseClick";
    public static const TOP_BAR_RESUME_CLICK:String = "topBarResumeClick";
    public static const MULTIPLIER_STEP_CHANGED:String = "multiplierStepsChanged";
    public static const PLAY_MULTIPLIER_ACTIVATE_ANIMATION:String = "playMultiplierActivateAnimation";
    public static const PLAY_MULTIPLIER_DEACTIVATE_ANIMATION:String = "playMultiplierDectivateAnimation";
    public static const PLAY_SCORE_BONUS_ANIMATION:String = "playScoreBonusAnimation";
    public static const MULTIPLIER_CURRENT_TIME_CHANGED:String = "multiplierCurrentTimeChanged";

    private var _data:Object;

    public function GameWrapperContextEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }

    override public function clone():Event {
        return new GameContextEvent(type, _data, bubbles, cancelable);
    }
}
}
