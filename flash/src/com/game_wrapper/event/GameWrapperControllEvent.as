package com.game_wrapper.event {
    import flash.events.Event;

    public class GameWrapperControllEvent extends Event {
        public static const ADD_GAME_WRAPPER: String = "addGameWrapper";
        public static const REMOVE_GAME_WRAPPER: String = "removeGameWrapper";

        public static const PLAY_READY_GO_ANIMATION:String = "playReadyGoAnimationComplete";
        public static const PAUSE_READY_GO_ANIMATION:String = "pauseReadyGoAnimationComplete";
        public static const RESUME_READY_GO_ANIMATION:String = "resumeReadyGoAnimationComplete";

        public static const START_TIME_CALCULATING_COMMAND:String = "startTimeCalculatingCommand";
        public static const STOP_TIME_CALCULATING_COMMAND:String = "stopTimeCalculatingCommand";

        private var _data: Object;


        public function GameWrapperControllEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new GameWrapperControllEvent(type, _data, bubbles, cancelable);
        }
    }
}