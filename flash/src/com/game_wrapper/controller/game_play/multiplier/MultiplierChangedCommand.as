package com.game_wrapper.controller.game_play.multiplier {
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.level.LevelModel;
    import com.greensock.TweenLite;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * MultiplierChanged class.
     * User: Paul Makarenko
     * Date: 15.11.13
     */
    public class MultiplierChangedCommand extends Command {

        [Inject]
        public var levelModel: LevelModel;
        [Inject]
        public var event: GameControllEvent;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function MultiplierChangedCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            if (event.data as int == levelModel.multiplierDafaultValue) {
                onDeactivate();
            } else {
                onActivate();
            }
        }


        private function onActivate(): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.MULTIPLIER_ACTIVATED);

            // Play animations
            dispatcher.dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.PLAY_MULTIPLIER_ACTIVATE_ANIMATION));

            // Multiplier will be really activated after animation ends
            TweenLite.delayedCall(1, activateMultiplier);
        }


        private function activateMultiplier(): void {

            levelModel.isMultiplierActivated = true;
            levelModel.multiplierCurrentTime = levelModel.multiplierActiveTime;

        }


        private function onDeactivate(): void {
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.MULTIPLIER_DEACTIVATED);

            dispatcher.dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.PLAY_MULTIPLIER_DEACTIVATE_ANIMATION));

            levelModel.allowMultiplierStepChanging = true;
            levelModel.multiplierCurrentStep = 0;
            levelModel.allowMultiplierStepChanging = false;

            TweenLite.delayedCall(1, deactivateMultiplier);
        }


        private function deactivateMultiplier(): void {
            levelModel.allowMultiplierStepChanging = true;
        }
    }
}