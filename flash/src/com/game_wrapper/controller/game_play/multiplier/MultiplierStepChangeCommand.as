package com.game_wrapper.controller.game_play.multiplier {
    import com.game_wrapper.model.level.LevelModel;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * MultiplierStepChange class.
     * User: Paul Makarenko
     * Date: 15.11.13
     */
    public class MultiplierStepChangeCommand extends Command {

        [Inject]
        public var levelModel: LevelModel;


        public function MultiplierStepChangeCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            if (levelModel.multiplierCurrentStep == levelModel.multiplierMaxSteps) {
                levelModel.multiplierCurrentTime = levelModel.multiplierActiveTime;
                levelModel.allowMultiplierStepChanging = false;
            }
        }
    }
}