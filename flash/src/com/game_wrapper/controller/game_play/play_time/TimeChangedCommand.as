package com.game_wrapper.controller.game_play.play_time {
    import com.game.events.GameControllEvent;
    import com.game_wrapper.model.level.LevelModel;
    import com.greensock.TweenLite;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * TimeChangedCommand class.
     * User: Paul Makarenko
     * Date: 15.11.13
     */
    public class TimeChangedCommand extends Command {

        [Inject]
        public var levelModel: LevelModel;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function TimeChangedCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            // Reduce steps of multiplier
            if (levelModel.allowMultiplierStepChanging) {

                levelModel.multiplierCurrentStep--;

            } else
            // or if multiplier bar is full when reduce "live" time of active multiplier
            if (levelModel.isMultiplierActivated) {
                levelModel.multiplierCurrentTime--;

                if (levelModel.multiplierCurrentTime == 0) {
                    levelModel.isMultiplierActivated = false;
                    dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.CHANGE_MULTIPLIER, levelModel.multiplierDafaultValue))
                }
            }

      }
    }
}