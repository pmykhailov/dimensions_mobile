package com.game_wrapper.controller.game_play.play_time {
    import com.game_wrapper.model.level.LevelModel;
    import com.game_wrapper.view.game_warapper.GameWrapperView;
    import com.greensock.TweenLite;
    import com.game.events.GameControllEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * TimeIsUpCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class TimeIsUpCommand extends Command {

        [Inject]
        public var levelModel: LevelModel;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var gameWrapperView: GameWrapperView;


        public function TimeIsUpCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            levelModel.stopTimeCalculating();
            gameWrapperView.mouseChildren = false;
            gameWrapperView.mouseEnabled = false;

            dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.STOP_GAME));
        }
    }
}