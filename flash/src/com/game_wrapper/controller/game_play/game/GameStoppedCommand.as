package com.game_wrapper.controller.game_play.game {
    import com.game.events.GameControllEvent;
    import com.game_wrapper.view.game_warapper.GameWrapperView;
    import com.greensock.TweenLite;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * GameStoppedCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class GameStoppedCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var gameWrapperView: GameWrapperView;

        public function GameStoppedCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            TweenLite.to(gameWrapperView, 0.5, {alpha:0});

            dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.HIDE_GAME));
        }
    }
}