package com.game_wrapper.controller.game_play.game {
    import com.game_wrapper.model.level.LevelModel;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * PauseGameCommand class.
     * User: Paul Makarenko
     * Date: 07.10.13
     */
    public class PauseGameCommand extends Command {

        [Inject]
        public var levelModel:LevelModel;

        public function PauseGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            levelModel.stopTimeCalculating();
        }
    }
}