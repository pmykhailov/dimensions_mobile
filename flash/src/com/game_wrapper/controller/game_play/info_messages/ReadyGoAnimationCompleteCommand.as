package com.game_wrapper.controller.game_play.info_messages {
import com.game.events.GameContextEvent;
import com.game.events.GameControllEvent;
import com.game_wrapper.event.GameWrapperControllEvent;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class ReadyGoAnimationCompleteCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    public function ReadyGoAnimationCompleteCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.GAME_MOUSE_CLICKABLE, true));
        dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.START_TIME_CALCULATING_COMMAND));
    }
}
}
