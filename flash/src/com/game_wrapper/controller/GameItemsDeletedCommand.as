package com.game_wrapper.controller {
    import com.game.events.GameControllEvent;
    import com.game.events.GameExternalEvent;
import com.game.model.game_field.grid.item.GridItemModel;
import com.game.model.logic.win_combination.WinCombination;
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.model.level.LevelModel;
    import com.greensock.TweenLite;

    import flash.events.IEventDispatcher;
    import flash.utils.getTimer;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * GameItemsDeletedCommand class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class GameItemsDeletedCommand extends Command {

        [Inject]
        public var levelModel: LevelModel;
        [Inject]
        public var event: GameExternalEvent;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function GameItemsDeletedCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var combinations: Array = event.data as Array;
            var combination: WinCombination;
            var combinationsLenght: int = 0;
            var totalScore: int = 0;
            var totalBonusScore: int = 0;
            var i: int

            // Update score
            combination = combinations[0] as WinCombination;
            totalScore += combination.totalScore;
            totalBonusScore += combination.totalBonusScore;
            combinationsLenght += combination.gridItems.length;

            levelModel.score += totalScore;

            // Show bonus score animation
            if (totalBonusScore > 0){
                dispatcher.dispatchEvent(new GameWrapperContextEvent(GameWrapperContextEvent.PLAY_SCORE_BONUS_ANIMATION, totalBonusScore));
            }

            // Give time bonus
            // TODO: CONFIGURABLE ITEMS IN ONE PLACE
            if (combinationsLenght >= 6) {
                var bonusTime:int;

                if (combinationsLenght == 7) {
                    bonusTime = 2;
                } else
                if (combinationsLenght == 8) {
                    bonusTime = 3;
                } else
                if (combinationsLenght == 9) {
                    bonusTime = 5;
                } else
                if (combinationsLenght == 10) {
                    bonusTime = 7;
                } else
                if (combinationsLenght > 10) {
                    bonusTime = 5 + Math.floor(combinationsLenght*combinationsLenght*combinationsLenght*0.003);
                    //11 :8
                    //12 :10
                    //13 :11
                    //14 :13
                    //15 :15
                    //16 :17
                    //17 :19
                    //18 :22
                    //19 :25
                }

                if (combination.isAllSymbolsHaveTheSameShape) {
                     if (combinationsLenght <= 9 ) {
                         bonusTime += 1;
                     } else {
                         bonusTime += 2;
                     }
                }

                levelModel.addBonusTime(bonusTime);
            }

            // Multiplier bonus logic
            var prevTime: Number = levelModel.lastQuicklyDrawnLineTime;
            var currentTime: Number = getTimer();

            var quickTime: int = 2*(850 + combinationsLenght * 250);

            //trace((currentTime - prevTime) + " < " + quickTime + " " + combinationsLenght);

            if (currentTime - prevTime < quickTime ) {
                levelModel.quicklyDrawnLinesCount++;
            } else {
                levelModel.quicklyDrawnLinesCount = 0;
            }

            levelModel.lastQuicklyDrawnLineTime = currentTime;

            if (levelModel.allowMultiplierStepChanging && levelModel.quicklyDrawnLinesCount > 0) {
                levelModel.multiplierCurrentStep += combinationsLenght;

                // Multiplier is activated
                if (levelModel.multiplierCurrentStep == levelModel.multiplierMaxSteps) {
                    levelModel.allowMultiplierStepChanging = false;

                    dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.CHANGE_MULTIPLIER, levelModel.multiplierValue))
                }
            }
        }

    }
}