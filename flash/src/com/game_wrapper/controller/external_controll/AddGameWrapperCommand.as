package com.game_wrapper.controller.external_controll {
import com.ad.AdMobManager;
import com.game.events.GameContextEvent;
    import com.game.settings.GameSettings;
    import com.game_wrapper.model.level.LevelModel;
    import com.game_wrapper.view.game_warapper.GameWrapperView;
    import com.greensock.TweenLite;
    import com.share.ApplicationDimensions;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.display.DisplayObject;

    import flash.display.DisplayObjectContainer;
    import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

    /**
     * InitTopBarCommand class.
     * User: Paul Makarenko
     * Date: 03.10.13
     */
    public class AddGameWrapperCommand extends Command {

        [Inject]
        public var event: GameWrapperControllEvent;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        private var levelModel: LevelModel;


        public function AddGameWrapperCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            setupModel();
            setupView();
        }


        public function setupModel(): void {
            levelModel = new LevelModel();

            // TODO: CONFIGURABLE ITEMS IN ONE PLACE
            levelModel.timeToCompleteLevel = 60;
            levelModel.multiplierMaxSteps = 15;
            levelModel.multiplierActiveTime = 15;
            levelModel.multiplierDafaultValue = 1;
            levelModel.multiplierValue = LevelModel.MULTIPLIER_VALUE;
            levelModel.shapeMatchMultiplierValue = 3;

            injector.injectInto(levelModel);
            injector.map(LevelModel).toValue(levelModel);
        }


        private function setupView(): void {
            var container: DisplayObjectContainer = (event.data.container as DisplayObjectContainer);

            dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.ADD_GAME, {container: container}));

            container.getChildAt(0).x = (ApplicationDimensions.WIDTH - GameSettings.instance.itemWidth * GameSettings.instance.gameFieldCols) / 2;
            container.getChildAt(0).y = (ApplicationDimensions.HEIGHT - GameSettings.instance.itemHeight * GameSettings.instance.gameFieldRows) / 2 + 8;

            var gameView:DisplayObject = container.getChildAt(0);
            var yy:Number = gameView.y;
            gameView.alpha = 0;
            gameView.y = yy - 20;
            TweenLite.to(gameView, 0.5, {alpha:1, y:yy});

            var gameWrapperView: GameWrapperView = new GameWrapperView();
            gameWrapperView.topBar.timeControll.maxTime = levelModel.timeToCompleteLevel;
            gameWrapperView.topBar.multiplierBar.maxStep = levelModel.multiplierMaxSteps;
            gameWrapperView.topBar.multiplierBar.maxTime = levelModel.multiplierActiveTime;
            gameWrapperView.topBar.multiplier = levelModel.multiplierDafaultValue;
            gameWrapperView.topBar.highScore = event.data.highScore;

            injector.map(GameWrapperView).toValue(gameWrapperView);
            container.addChild(gameWrapperView.view);

            AdMobManager.instance.visible = false;
        }
    }
}