package com.game_wrapper.controller.external_controll {
import com.ad.AdMobManager;
import com.game_wrapper.model.level.LevelModel;
    import com.game_wrapper.view.game_warapper.GameWrapperView;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

    /**
     * InitTopBarCommand class.
     * User: Paul Makarenko
     * Date: 03.10.13
     */
    public class RemoveGameWrapperCommand extends Command {

        [Inject]
        public var event: GameWrapperControllEvent;
        [Inject]
        public var gameWrapperView: GameWrapperView;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var levelModel: LevelModel;


        public function RemoveGameWrapperCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            releaseModel();
            releaseView();
        }


        public function releaseView(): void {
            dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.REMOVE_GAME, event.data));

            injector.unmap(GameWrapperView);
            gameWrapperView.destroy();

            AdMobManager.instance.visible = true;
        }


        private function releaseModel(): void {
            injector.unmap(LevelModel);
        }
    }
}