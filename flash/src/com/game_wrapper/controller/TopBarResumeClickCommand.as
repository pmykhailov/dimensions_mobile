package com.game_wrapper.controller {
    import com.game_wrapper.model.level.LevelModel;
    import com.game.events.GameControllEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * TopBarPauseClickCommand class.
     * User: Paul Makarenko
     * Date: 09.11.13
     */
    public class TopBarResumeClickCommand extends Command {

        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var levelModel: LevelModel;

        public function TopBarResumeClickCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            levelModel.isPaused = false;

            dispatcher.dispatchEvent(new GameWrapperControllEvent(GameWrapperControllEvent.RESUME_READY_GO_ANIMATION));
            dispatcher.dispatchEvent(new GameControllEvent(GameControllEvent.RESUME_GAME));
        }
    }
}