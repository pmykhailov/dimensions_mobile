package com.game_wrapper {
    import com.game_wrapper.controller.GameItemsDeletedCommand;
    import com.game_wrapper.controller.TopBarPauseClickCommand;
    import com.game_wrapper.controller.TopBarResumeClickCommand;
    import com.game_wrapper.controller.external_controll.AddGameWrapperCommand;
    import com.game_wrapper.controller.external_controll.RemoveGameWrapperCommand;
    import com.game_wrapper.controller.external_controll.time.StartTimeCalculatingCommand;
    import com.game_wrapper.controller.external_controll.time.StopTimeCalculatingCommand;
    import com.game_wrapper.controller.game_play.game.GameStoppedCommand;
    import com.game_wrapper.controller.game_play.game.PauseGameCommand;
    import com.game_wrapper.controller.game_play.info_messages.ReadyGoAnimationCompleteCommand;
    import com.game_wrapper.controller.game_play.game.ResumeGameCommand;
    import com.game_wrapper.controller.game_play.game.StartGameCommand;
    import com.game_wrapper.controller.game_play.multiplier.MultiplierChangedCommand;
    import com.game_wrapper.controller.game_play.play_time.TimeChangedCommand;
    import com.game_wrapper.controller.game_play.play_time.TimeIsUpCommand;
    import com.game_wrapper.event.GameWrapperContextEvent;
    import com.game_wrapper.view.game_warapper.GameWrapperView;
    import com.game_wrapper.view.game_warapper.GameWrapperViewMediator;
    import com.game_wrapper.view.ready_go.ReadyGo;
    import com.game_wrapper.view.ready_go.ReadyGoMediator;
    import com.game_wrapper.view.top_bar.TopBarMediator;
    import com.game_wrapper.view.top_bar.TopBarView;
    import com.game.events.GameControllEvent;
    import com.game.events.GameExternalEvent;
    import com.game_wrapper.event.GameWrapperControllEvent;

    import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;

    public class GameWrapperConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {
            eventCommandMap.map(GameExternalEvent.ITEMS_DELETED).toCommand(GameItemsDeletedCommand);
            eventCommandMap.map(GameExternalEvent.GAME_STOPPED).toCommand(GameStoppedCommand);
            eventCommandMap.map(GameControllEvent.START_GAME).toCommand(StartGameCommand);
            eventCommandMap.map(GameControllEvent.PAUSE_GAME).toCommand(PauseGameCommand);
            eventCommandMap.map(GameControllEvent.RESUME_GAME).toCommand(ResumeGameCommand);
            eventCommandMap.map(GameControllEvent.CHANGE_MULTIPLIER).toCommand(MultiplierChangedCommand);
            eventCommandMap.map(GameWrapperContextEvent.TIME_IS_UP).toCommand(TimeIsUpCommand);
            eventCommandMap.map(GameWrapperContextEvent.TIME_CHANGED).toCommand(TimeChangedCommand);
            eventCommandMap.map(GameWrapperContextEvent.READY_GO_ANIMATION_COMPLETE).toCommand(ReadyGoAnimationCompleteCommand);
            eventCommandMap.map(GameWrapperContextEvent.TOP_BAR_PAUSE_CLICK).toCommand(TopBarPauseClickCommand);
            eventCommandMap.map(GameWrapperContextEvent.TOP_BAR_RESUME_CLICK).toCommand(TopBarResumeClickCommand);

            eventCommandMap.map(GameWrapperControllEvent.ADD_GAME_WRAPPER).toCommand(AddGameWrapperCommand);
            eventCommandMap.map(GameWrapperControllEvent.REMOVE_GAME_WRAPPER).toCommand(RemoveGameWrapperCommand);

            eventCommandMap.map(GameWrapperControllEvent.START_TIME_CALCULATING_COMMAND).toCommand(StartTimeCalculatingCommand);
            eventCommandMap.map(GameWrapperControllEvent.STOP_TIME_CALCULATING_COMMAND).toCommand(StopTimeCalculatingCommand);
        }


        private function mapMediators(): void {
            mediatorMap.map(GameWrapperView).toMediator(GameWrapperViewMediator);
            mediatorMap.map(TopBarView).toMediator(TopBarMediator);
            mediatorMap.map(ReadyGo).toMediator(ReadyGoMediator);
        }


        private function mapInjections(): void {
        }


        private function init(): void {

        }
    }
}
