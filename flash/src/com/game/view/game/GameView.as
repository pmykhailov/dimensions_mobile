package com.game.view.game {
    import by.lord_xaoca.robotlegs2.BaseView;

    import com.game.view.game_filed.grid.GridView;
    import com.game.view.items_connection.ItemsConnection;
    import com.game.view.score.manager.ScoreViewManager;
import com.greensock.TimelineLite;

import flash.display.Sprite;

    public class GameView extends BaseView {

        private var _gridView: GridView;
        private var _itemsDeleteLayer: Sprite;
        private var _scoreViewManager: ScoreViewManager;
        private var _itemsConnection: ItemsConnection;
        private var _isDeletingItems: Boolean;
        private var _isSpawningItems: Boolean;

        public var spawnTimeline: TimelineLite;

        public function GameView() {
            super(new Sprite());
        }


        public function get gridView(): GridView {
            return _gridView;
        }


        public function get scoreViewManager(): ScoreViewManager {
            return _scoreViewManager;
        }


        public function set scoreViewManager(value: ScoreViewManager): void {
            _scoreViewManager = value;
        }


        public function get itemsDeleteLayer(): Sprite {
            return _itemsDeleteLayer;
        }


        public function get itemsConnection(): ItemsConnection {
            return _itemsConnection;
        }


        public function get isDeletingItems(): Boolean {
            return _isDeletingItems;
        }


        public function set isDeletingItems(value: Boolean): void {
            _isDeletingItems = value;
        }


        public function get isSpawningItems(): Boolean {
            return _isSpawningItems;
        }


        public function set isSpawningItems(value: Boolean): void {
            _isSpawningItems = value;
        }


        override protected function initialize(): void {
            super.initialize();

            createScoreViewManager();
            createItemsDeleteLayer();
            createGridView();
            createItemsConnection();

            addChild(_gridView);
            addChild(_itemsConnection);
            addChild(_itemsDeleteLayer);
            addChild(_scoreViewManager.scoreLayerView);
        }


        protected function createGridView(): void {
            _gridView = new GridView();
        }


        protected function createScoreViewManager(): void {
            _scoreViewManager = new ScoreViewManager();
        }


        private function createItemsConnection(): void {
            _itemsConnection = new ItemsConnection();
        }


        private function createItemsDeleteLayer(): void {
            _itemsDeleteLayer = new Sprite();
            _itemsDeleteLayer.mouseChildren = false;
            _itemsDeleteLayer.mouseEnabled = false;
        }
    }
}
