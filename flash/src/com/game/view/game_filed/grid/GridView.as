package com.game.view.game_filed.grid {
    import by.lord_xaoca.utils.ArrayUtils;

    import com.game.model.game_field.grid.GridModel;
    import com.game.model.game_field.grid.item.GridItemModel;
    import com.game.settings.GameSettings;
    import com.game.view.game_filed.grid.item.base.GridItemView;
    import com.game.view.game_filed.grid.item.test.SampleGridItemView;

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.geom.Point;

    public class
    GridView extends Sprite {
        private var _rows: int;
        private var _cols: int;
        private var _itemWidth: int;
        private var _itemHeight: int;
        private var _items: Array;
        private var _selectedItems: Array;


        public function GridView() {
            super();

            init();
        }


        public function get itemHeight(): int {
            return _itemHeight;
        }


        public function get itemWidth(): int {
            return _itemWidth;
        }


        public function get selectedItems(): Array {
            return _selectedItems;
        }


        public function set selectedItems(value: Array): void {
            _selectedItems = value;
        }


        public function set enable(value: Boolean): void {
            mouseChildren = mouseEnabled = value;
        }


        public function createNewGridItem(gridModel: GridModel, row: int, col: int): GridItemView {
            var itemModel: GridItemModel = gridModel.getGridItem(row, col);
            var itemCoordinates: Point = getItemDesiredCoordinates(row, col);
            var item: GridItemView = new SampleGridItemView();

            item.x = itemCoordinates.x;
            item.y = itemCoordinates.y;
            item.row = row;
            item.col = col;
            item.id = itemModel.id;
            item.type = itemModel.type;

            return item;
        }


        public function addGridItem(item: GridItemView): void {
            _items.push(item);
            addChild(item.view);
        }


        public function removeGridItem(item: GridItemView): void {
            if (ArrayUtils.removeEntry(_items, item)) {
                removeChild(item.view);
            }
        }


        public function getItemDesiredCoordinates(row: int, col: int): Point {
            return new Point(_itemWidth / 2 + col * _itemWidth, _itemHeight / 2 + row * _itemHeight);
        }


        public function getItemViewByID(id: uint): GridItemView {
            for (var i: int = 0; i < _items.length; i++) {
                var item: GridItemView = _items[i] as GridItemView;
                if (item.id == id) {
                    return item;
                }
            }

            return null;
        }


        public function getItemViewByView(view: DisplayObject): GridItemView {
            for (var i: int = 0; i < _items.length; i++) {
                var item: GridItemView = _items[i] as GridItemView;
                if (item.view == view) {
                    return item;
                }
            }

            return null;
        }


        public function createGridItems(gridModel: GridModel): void {
            _rows = gridModel.rows;
            _cols = gridModel.cols;

            _items = [];

            for (var i: int = 0; i < gridModel.rows; i++) {
                for (var j: int = 0; j < gridModel.cols; j++) {
                    addGridItem(createNewGridItem(gridModel, i, j));
                }
            }
        }


        public function removeGridItems(): void {
            if (!_items) return;

            while (_items.length > 0) {
                removeGridItem(_items[0]);
            }
        }


        private function init(): void {
            _itemWidth = GameSettings.instance.itemWidth;
            _itemHeight = GameSettings.instance.itemHeight;
        }
    }
}