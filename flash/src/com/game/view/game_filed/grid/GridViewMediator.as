package com.game.view.game_filed.grid {
    import by.lord_xaoca.helpers.StageReference;

    import com.game.events.GameContextEvent;
    import com.game.view.events.GameViewEvent;
    import com.game.events.GameControllEvent;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    import robotlegs.bender.bundles.mvcs.Mediator;

    public class GridViewMediator extends Mediator {
        [Inject]
        public var gridView: GridView;


        public function GridViewMediator() {
        }


        override public function initialize(): void {
            super.initialize();

            addViewListener(MouseEvent.MOUSE_DOWN, onItemMouseDownHandler);
            StageReference.stage.addEventListener(MouseEvent.MOUSE_UP, onItemMouseUpHandler);

            addContextListener(GameContextEvent.START_ITEM_MOUSE_OVER_LISTENING, onStartItemMouseOverListeningHandler);
            addContextListener(GameContextEvent.STOP_ITEM_MOUSE_OVER_LISTENING, onStopItemMouseOverListeningHandler);
            addContextListener(GameControllEvent.STOP_GAME, onStopGameHandler);
        }


        override public function destroy(): void {
            super.destroy();
            removeListeners();
        }


        private function removeListeners(): void {
            StageReference.stage.removeEventListener(MouseEvent.MOUSE_UP, onItemMouseUpHandler);
        }


        protected function onItemMouseDownHandler(event: MouseEvent): void {
            if (event.target is View_GridItem){
                dispatch(new GameViewEvent(GameViewEvent.ITEM_MOUSE_DOWN, gridView.getItemViewByView(event.target as DisplayObject)));
            }
        }


        protected function onItemMouseUpHandler(event: MouseEvent): void {
            dispatch(new GameViewEvent(GameViewEvent.ITEM_MOUSE_UP, gridView.getItemViewByView(event.target as MovieClip)));
        }


        protected function onItemMouseOverHandler(event: MouseEvent): void {
            if (event.target is View_GridItem){
                dispatch(new GameViewEvent(GameViewEvent.ITEM_MOUSE_OVER, gridView.getItemViewByView(event.target as MovieClip)));
            }
        }


        private function onStartItemMouseOverListeningHandler(event: GameContextEvent): void {
            addViewListener(MouseEvent.MOUSE_OVER, onItemMouseOverHandler)
        }


        private function onStopItemMouseOverListeningHandler(event: GameContextEvent): void {
            removeViewListener(MouseEvent.MOUSE_OVER, onItemMouseOverHandler)
        }


        private function onStopGameHandler(event: GameControllEvent): void {
            removeListeners();

            removeViewListener(MouseEvent.MOUSE_OVER, onItemMouseOverHandler);
            removeViewListener(MouseEvent.MOUSE_DOWN, onItemMouseDownHandler);
        }
    }
}