package com.game.view.game_filed.grid.item.test {
import com.greensock.TweenLite;
import com.game.view.game_filed.grid.item.base.GridItemView;

import flash.display.DisplayObject;

import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Timer;
import flash.utils.getDefinitionByName;

import org.flintparticles.common.actions.Age;

import org.flintparticles.common.actions.Fade;
import org.flintparticles.common.counters.Blast;

import org.flintparticles.common.counters.Steady;
import org.flintparticles.common.displayObjects.Dot;
    import org.flintparticles.common.displayObjects.Star;
    import org.flintparticles.common.events.EmitterEvent;
import org.flintparticles.common.initializers.ColorsInit;
import org.flintparticles.common.initializers.ImageClass;
import org.flintparticles.common.initializers.Lifetime;
import org.flintparticles.twoD.actions.Move;
    import org.flintparticles.twoD.actions.Rotate;
    import org.flintparticles.twoD.emitters.Emitter2D;
import org.flintparticles.twoD.initializers.Position;
    import org.flintparticles.twoD.initializers.RotateVelocity;
    import org.flintparticles.twoD.initializers.Rotation;
    import org.flintparticles.twoD.initializers.Velocity;
import org.flintparticles.twoD.renderers.BitmapRenderer;
import org.flintparticles.twoD.renderers.DisplayObjectRenderer;
import org.flintparticles.twoD.zones.DiscZone;
    import org.flintparticles.twoD.zones.LineZone;
    import org.flintparticles.twoD.zones.PointZone;
    import org.flintparticles.twoD.zones.RectangleZone;

    public class SampleGridItemView extends GridItemView {

    private var _over:MovieClip;
    private var _selected:MovieClip;
    private var _deleteAnimation:MovieClip;
    private var _deleteAnimationTimer:Timer;
    public function SampleGridItemView() {
        super();
    }

    override public function set type(value:int):void {
        super.type = value;

        _currentView.gotoAndStop(value);

        _over = _view["_over"];
        _selected = _view["_selected"];

        _over.stop();
        _selected.stop();
    }

    override public function set isSelected(value:Boolean):void {
        super.isSelected = value;

        _isSelected ? _selected.gotoAndPlay(1) : _selected.gotoAndStop(1);

        if (value){
            alpha = 0.3;
        }else{
            alpha = 1;
        }
    }

    private function get _currentView():MovieClip {
        return _view as MovieClip;
    }

    override protected function initView():void {
        super.initView();

        _over = _view["_over"];
        _selected = _view["_selected"];

        View_DeleteAnimation;
        View_DeleteAnimation_0;
        View_DeleteAnimation_1;
        View_DeleteAnimation_2;

        var className:String;

        if (FPSCounter.instance.isLowPerfomanceDevice) {
            className = "View_DeleteAnimation";
        } else {
            var rnd:int = int(Math.random()*3);
            className = "View_DeleteAnimation_" + rnd;
        }

        var DeleteAnimationClass = getDefinitionByName(className);
        _deleteAnimation = new DeleteAnimationClass() as MovieClip;
        _deleteAnimation.x = -_deleteAnimation.width/2;
        _deleteAnimation.y = -_deleteAnimation.height/2;
        _deleteAnimation.gotoAndStop(1);
        _deleteAnimation.visible = false;
        addChildAt(_deleteAnimation,0);
    }

    override public function runDeleteAnimation():void {

        while (_currentView.numChildren > 1) {
            _currentView.removeChildAt(1);
        }

        _deleteAnimation.gotoAndPlay(1);
        _deleteAnimation.visible = true;
        _deleteAnimation.addEventListener(Event.COMPLETE, onComplete);

    }

    private function onComplete(event:Event):void {
        _deleteAnimation.removeEventListener(Event.COMPLETE, onComplete);
        notifyDeleteAnimationComplete();
    }


    /*
    override public function runDeleteAnimation():void {
        var emitter:Emitter2D = new Emitter2D();

        emitter.counter = new Blast(20);

        emitter.addInitializer(new ImageClass(Dot, [8]));
        emitter.addInitializer(new ColorsInit([0xFFFF0000, 0xFF00FF00, 0xFF0000FF, 0xFFFFFF00, 0xFF00FFFF]));
        emitter.addInitializer(new Lifetime(0.2, 1.5));
        emitter.addInitializer(new Position(new PointZone(new Point())));
        emitter.addInitializer(new Velocity(new RectangleZone(-100,-100,100,100)));

        emitter.addAction(new Move());
        emitter.addAction(new Age());
        emitter.addAction(new Fade());

        var renderer:BitmapRenderer = new BitmapRenderer(new Rectangle(-100,-100,200,200));
        renderer.addEmitter(emitter);
        addChild(renderer);

        emitter.addEventListener(EmitterEvent.EMITTER_EMPTY, onEmitterEmptyHandler);
        emitter.start();

        for (var i:int = 0; i < _currentView.numChildren; i++) {
            var child:DisplayObject = _currentView.getChildAt(i);

            if (child != renderer)
            {
                TweenLite.to(child, 0.3, {alpha:0});
            }
        }
    }


    private function onEmitterEmptyHandler(event:EmitterEvent):void {
        event.currentTarget.removeEventListener(EmitterEvent.EMITTER_EMPTY, onEmitterEmptyHandler);
        notifyDeleteAnimationComplete();
    }
    */

    override protected function onMouseOverHandler(event:MouseEvent):void {
        _over.gotoAndPlay(1);
    }

    override protected function onMouseOutHandler(event:MouseEvent):void {
        _over.gotoAndStop(1);
    }
}
}