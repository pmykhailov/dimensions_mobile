package com.game.view.game_filed.grid.item.base {
    import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

    import com.game.model.game_field.grid.item.enum.GridItemTypeEnum;
    import com.game.view.events.GameViewEvent;

    import flash.events.Event;
    import flash.events.MouseEvent;

    public class GridItemView extends BaseDisplayObjectContainer {

        protected var _row: int;
        protected var _col: int;
        protected var _id: int;
        protected var _type: int;
        protected var _color: int;
        protected var _shape: int;
        protected var _isSelected: Boolean;


        public function GridItemView() {
            super(new View_GridItem());
            init();
        }


        public function get col(): int {
            return _col;
        }


        public function set col(value: int): void {
            _col = value;
        }


        public function get row(): int {
            return _row;
        }


        public function set row(value: int): void {
            _row = value;
        }


        public function get id(): int {
            return _id;
        }


        public function set id(value: int): void {
            _id = value;
        }


        public function get type(): int {
            return _type;
        }


        public function set type(value: int): void {
            _type = value;
            if (_type == GridItemTypeEnum.NON_EXISTING) {
                mouseChildren = mouseEnabled = false;
                visible = false;
            }

            _color = _type % GridItemTypeEnum.COLORS_COUNT;
            _shape = int((_type - 1) / GridItemTypeEnum.COLORS_COUNT );
        }


        public function get isSelected(): Boolean {
            return _isSelected;
        }


        public function set isSelected(value: Boolean): void {
            _isSelected = value;
        }


        public function get color(): int {
            return _color;
        }


        public function get shape(): int {
            return _shape;
        }


        public function runDeleteAnimation(): void {
            // Run notifyDeleteAnimationComplete in delete animation complete event handler
            notifyDeleteAnimationComplete();
        }


        protected function init(): void {
            initElements();
            initHitArea();
            addListeners();
        }


        protected function initElements(): void {
            _isSelected = false;
        }


        protected function initHitArea(): void {
            mouseChildren = false;
            _view.hitArea = getSprite("_hitArea");
            getSprite("_hitArea").visible = false;
        }


        protected function addListeners(): void {
            addViewListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage);

            addViewListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
            addViewListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);
            addViewListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
        }


        protected function removeListeners(): void {
            removeViewListener(Event.REMOVED_FROM_STAGE, onRemoveFromStage);

            removeViewListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
            removeViewListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);
            removeViewListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
        }


        protected function notifyDeleteAnimationComplete(): void {
            dispatchEvent(new GameViewEvent(GameViewEvent.DELETE_ITEM_ANIMATION_COMPLETE));
        }


        protected function onMouseOverHandler(event: MouseEvent): void {

        }


        protected function onMouseOutHandler(event: MouseEvent): void {

        }


        protected function onMouseDownHandler(event: MouseEvent): void {
            isSelected = !isSelected;
        }


        protected function onRemoveFromStage(event: Event): void {
            removeListeners();
        }
    }
}