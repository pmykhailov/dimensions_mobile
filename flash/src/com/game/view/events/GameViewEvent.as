package com.game.view.events  {
import flash.events.Event;

public class GameViewEvent extends Event {

    public static const ITEM_MOUSE_DOWN:String = "itemMouseDown";
    public static const ITEM_MOUSE_UP:String = "itemMouseUp";
    public static const ITEM_MOUSE_OVER:String = "itemMouseOver";
    public static const DELETE_ITEM_ANIMATION_COMPLETE:String = "deleteItemsAnimationComplete";
    public static const SCORE_TEXT_ANIMATION_COMPLETE:String = "scoreTextAnimationComplete";

    private var _data:Object;

    public function GameViewEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }

    override public function clone():Event {
        return new GameViewEvent(type, _data, bubbles, cancelable);
    }

}
}