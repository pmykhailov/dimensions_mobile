package com.game.view.score.text.test {
import com.greensock.TweenLite;
import com.game.view.score.text.base.ScoreTextView;
import com.greensock.easing.Linear;

public class SampleScoreTextView extends ScoreTextView {
    public function SampleScoreTextView() {
        super();
    }

    override public function runAnimation():void {
        var animationTime:Number = 0.7;

        if (FPSCounter.instance.isLowPerfomanceDevice) {
            TweenLite.to(this, animationTime, {y:y-40, onComplete: notifyAnimationComplete, ease:Linear.easeOut});
        } else {
            alpha = 0;
            TweenLite.to(this, animationTime, {alpha: 1, scaleX: 1.2, scaleY: 1.2, onComplete: notifyAnimationComplete});
        }
    }

}
}