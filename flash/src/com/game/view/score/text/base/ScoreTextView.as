package com.game.view.score.text.base  {
import by.lord_xaoca.utils.ClassUtils;
import by.lord_xaoca.views.base.BaseDisplayObjectContainer;

import com.game.view.events.GameViewEvent;

import flash.text.TextField;

public class ScoreTextView extends BaseDisplayObjectContainer {
    private var _scoreTf:TextField;

    public function ScoreTextView() {
        super(new View_ScoreText());
    }

    public function set score(value:int):void {
        _scoreTf.text = value + "";
    }

    override protected function initView():void {
        super.initView();

        _scoreTf = _view["_scoreTf"];
    }

    /** [ABSTRACT] **/
    public function runAnimation():void {
        // Customize this animation to your own needs
        // and call notifyAnimationComplete at the end of it
    }

    protected function notifyAnimationComplete():void {
        dispatchEvent(new GameViewEvent(GameViewEvent.SCORE_TEXT_ANIMATION_COMPLETE));
    }
}
}