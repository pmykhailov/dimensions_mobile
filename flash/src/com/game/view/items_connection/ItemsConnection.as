package com.game.view.items_connection {
    import com.game.view.game_filed.grid.item.base.GridItemView;

    import flash.display.Sprite;

    /**
     * Connection class.
     * User: Paul Makarenko
     * Date: 03.11.13
     */
    public class ItemsConnection extends Sprite {

        public function ItemsConnection() {
            super();

            mouseChildren = false;
            mouseEnabled = false;
        }

        public function draw(selectedItems:Array):void{
            var firstItem:GridItemView = selectedItems[0] as GridItemView;

            graphics.clear();
            graphics.lineStyle(3, 0xFFFFFF);
            graphics.moveTo(firstItem.view.x, firstItem.view.y);

            for (var i: int = 1; i < selectedItems.length; i++) {
                var gridItemView: GridItemView = selectedItems[i];
                graphics.lineTo(gridItemView.view.x, gridItemView.view.y);
            }
        }

        public function clear():void{
            graphics.clear();
        }

    }
}