﻿package com.game.controller.internal {
    import com.game.events.GameContextEvent;
    import com.game.model.GameModel;
    import com.game.model.game_field.grid.GridModel;
    import com.game.model.game_field.grid.item.GridItemModel;
    import com.game.model.game_field.grid.item.GridItemPosition;
    import com.game.settings.GameSettings;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grid.GridView;
    import com.game.view.game_filed.grid.item.base.GridItemView;
    import com.greensock.TimelineLite;
    import com.greensock.TweenLite;
    import com.greensock.easing.Bounce;
    import com.greensock.easing.Linear;

    import flash.display.DisplayObject;
    import flash.events.IEventDispatcher;
import flash.events.KeyboardEvent;
import flash.geom.Point;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class SpawnItemsCommand extends Command {

        [Inject]
        public var event: GameContextEvent;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function SpawnItemsCommand() {
            super();
        }


        private function get gridView(): GridView {
            return gameView.gridView;
        }


        private function get gridModel(): GridModel {
            return gameModel.gridModel;
        }


        override public function execute(): void {
            gameModel.logicFacade.logicGameFlow.spawnItems(gameModel);

            spawnItems();
        }


        public function spawnItems(): void {
            var timeline: TimelineLite = new TimelineLite({onComplete: onSpawnItemsAnimationComplete});
            var i: int;
            var j: int;

            var itemViewView: DisplayObject;
            var itemView: GridItemView;

            if (gameView.spawnTimeline)
            {
                gameView.spawnTimeline.stop();
                gameView.spawnTimeline.clear();

                for (i = 0; i < gridView.numChildren; i++) {
                    itemViewView = gridView.getChildAt(i);
                    itemView = gridView.getItemViewByView(itemViewView);

                    var coordinates: Point = gridView.getItemDesiredCoordinates(itemView.row, itemView.col);
                    itemView.x = coordinates.x;
                    itemView.y = coordinates.y;
                    // alpha is not used for better performance on mobile devices
                    //itemView.alpha = 1;

                    enableItem(itemView);
                }
            }

            gameView.isSpawningItems = true;
            gameView.spawnTimeline = timeline;

            // Fall existing items
            var existingItems: Array = [];
            var minRowDifferense: int = gridModel.rows;

            for (i = 0; i < gridView.numChildren; i++) {
                itemViewView = gridView.getChildAt(i);
                itemView = gridView.getItemViewByView(itemViewView);

                if (itemView) {
                    var itemPosition: GridItemPosition = gridModel.getGridItemPosition(itemView.id);

                    // If item's view is differ from model than we gonna move it to new position
                    if (itemPosition.row != itemView.row || itemPosition.col != itemView.col) {
                        if (Math.abs(itemView.row - itemPosition.row) < minRowDifferense) {
                            minRowDifferense = Math.abs(itemView.row - itemPosition.row);
                        }

                        itemView.row = itemPosition.row;
                        itemView.col = itemPosition.col;

                        existingItems.push(itemView);
                    }
                }
            }

            for (i = 0; i < existingItems.length; i++) {
                timeline.insert(getItemMoveTween(existingItems[i] as GridItemView, /*minRowDifferense * */GameSettings.instance.timeSettings.existing_items_fall_time));
            }

            // Create new items
            var newItems: Array = [];
            var itemModel: GridItemModel;
            for (i = 0; i < gridModel.rows; i++) {
                for (j = 0; j < gridModel.cols; j++) {
                    itemModel = gridModel.getGridItem(i, j);

                    if (!gridView.getItemViewByID(itemModel.id)) {
                        var item: GridItemView = gridView.createNewGridItem(gridModel, i, j);

                        gridView.addGridItem(item);

                        item.y = -item.y - gridView.itemWidth;
                        // alpha is not used for better performance on mobile devices
                        item.alpha = 0;

                        newItems.push(item);
                    }
                }
            }

            newItems.sortOn(["row", "col"], Array.DESCENDING | Array.NUMERIC);

            for (i = 0; i < newItems.length; i++) {
                timeline.append(getItemFallTween(newItems[i] as GridItemView), -0.05);
            }

        }


        protected function onSpawnItemsAnimationComplete(): void {
            gameView.spawnTimeline = null;
            gameView.isSpawningItems = false;
        }


        private function onItemFallComplete(item: GridItemView): void {
            enableItem(item);
            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEM_FALL);
        }


        private function getItemMoveTween(item: GridItemView, time: Number): TweenLite {
            var coordinates: Point = gridView.getItemDesiredCoordinates(item.row, item.col);
            disableItem(item);
            return new TweenLite(item, time, {x: coordinates.x, y: coordinates.y, ease: Bounce.easeOut, onComplete:enableItem, onCompleteParams:[item]});
        }


        private function getItemFallTween(item: GridItemView): TweenLite {
            var coordinates: Point = gridView.getItemDesiredCoordinates(item.row, item.col);
            disableItem(item);

            // alpha is not used for better performance on mobile devices
            return new TweenLite(item, GameSettings.instance.timeSettings.new_items_fall_time, {x: coordinates.x, y: coordinates.y, alpha: 1, ease: Linear.easeNone, onComplete: onItemFallComplete, onCompleteParams:[item]});
        }


        private function disableItem(item: GridItemView):void{
            item.view.mouseEnabled = false;
        }


        private function enableItem(item: GridItemView):void{
            item.view.mouseEnabled = true;
        }
    }
}