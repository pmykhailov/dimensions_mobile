package com.game.controller.internal {
    import com.game.model.GameModel;
    import com.game.model.game_field.grid.item.GridItemModel;
    import com.game.view.events.GameViewEvent;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grid.GridView;
    import com.game.view.game_filed.grid.item.base.GridItemView;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    public class ItemMouseOverCommand extends Command {

        [Inject]
        public var event: GameViewEvent;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function ItemMouseOverCommand() {
            super();
        }


        override public function execute(): void {

            var gridView: GridView = gameView.gridView;
            var overedItem: GridItemView = event.data as GridItemView;
            var sItems: Array = gridView.selectedItems;
            var lastSelectedItem: GridItemView = sItems[sItems.length - 1] as GridItemView;
            var isSelectionTypeDefined: Boolean;
            var selectionByColor: Boolean;
            var selectionByShape: Boolean;
            var i: int;

            // Unselection check
            if (sItems.length >= 2 && sItems[sItems.length - 2] == overedItem){
                (sItems.pop() as GridItemView).isSelected = false;
                SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEM_UNSELECT);
                gameView.itemsConnection.draw(sItems);
                return;
            }

            if (sItems.length >= 2) {

                var isAllItemsAreEqualByColorAndType: Boolean = true;
                for (i = 1; i < sItems.length; i++) {
                    if (sItems[i - 1].color == sItems[i].color && sItems[i - 1].shape == sItems[i].shape) {
                        continue;
                    } else {
                        isAllItemsAreEqualByColorAndType = false;
                        break;
                    }
                }

                if (isAllItemsAreEqualByColorAndType) {
                    isSelectionTypeDefined = false;
                } else {
                    isSelectionTypeDefined = true;
                    selectionByColor = true;
                    for (i = 1; i < sItems.length ; i++) {
                        if (sItems[i - 1].color != sItems[i].color){
                            selectionByColor = false;
                            selectionByShape = true;
                            break;
                        }
                    }

                }
            }

            if (lastSelectedItem && overedItem) {
                var isAlreadySelected:Boolean;
                for (var i: int = 0; i < sItems.length; i++) {
                    var gridItemView: GridItemView = sItems[i];
                    if (gridItemView == overedItem){
                        isAlreadySelected = true;
                        break;
                    }
                }

                if (isAlreadySelected) return;

                if (lastSelectedItem != overedItem && isNeighbors(lastSelectedItem, overedItem)) {

                    if (
                            (isSelectionTypeDefined && selectionByColor && lastSelectedItem.color == overedItem.color) ||
                            (isSelectionTypeDefined && selectionByShape && lastSelectedItem.shape == overedItem.shape) ||
                            (!isSelectionTypeDefined && ((lastSelectedItem.color == overedItem.color) || (lastSelectedItem.shape == overedItem.shape)) )
                        )
                    {
                        SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEM_SELECT);
                        sItems.push(overedItem);
                        gameView.itemsConnection.draw(sItems);
                        overedItem.isSelected = true;
                    }
                }
            }
        }


        private function isNeighbors(item1: GridItemView, item2: GridItemView): Boolean {
            var itemModel1: GridItemModel = gameModel.gridModel.getGridItemByID(item1.id);
            var itemModel2: GridItemModel = gameModel.gridModel.getGridItemByID(item2.id);

            return gameModel.logicFacade.logicInfo.isNeighbors(gameModel, itemModel1, itemModel2);
        }
    }
}
