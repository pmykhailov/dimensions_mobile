﻿package com.game.controller.internal {
    import com.game.events.GameContextEvent;
    import com.game.model.GameModel;
    import com.game.model.game_field.grid.item.GridItemModel;
    import com.game.model.game_field.grid.item.GridItemPosition;
    import com.game.model.logic.win_combination.WinCombination;
    import com.game.view.events.GameViewEvent;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grid.GridView;
    import com.game.view.game_filed.grid.item.base.GridItemView;
    import com.game.view.score.text.base.ScoreTextView;
    import com.game.view.score.text.test.SampleScoreTextView;
    import com.game.events.GameExternalEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    /**
     * Actions with win combinations
     */
    public class DeleteItemsCommand extends Command {

        [Inject]
        public var event: GameContextEvent;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var dispatcher: IEventDispatcher;
        private var _combinations: Array;
        private var _items: Array;
        private var _scores: Array;
        private var _finished: int;

        public function DeleteItemsCommand() {
            super();
        }


        private function get gridView(): GridView {
            return gameView.gridView;
        }


        override public function execute(): void {

            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEMS_MATCHED);

            _combinations = event.data as Array;

            // NOTE: if event for some reasons have to be dispatched later
            // some changes needed, cuz deleteWinCombinationsItems invoke that will come after
            // calling this method sets all symbol types to EMPTY

            var combinaton: WinCombination = _combinations[0] as WinCombination;
            var baseScore:int;
            var isAllSymbolsHaveTheSameShape:Boolean = combinaton.isAllSymbolsHaveTheSameShape;

            // TODO: CONFIGURABLE ITEMS IN ONE PLACE
            isAllSymbolsHaveTheSameShape ? baseScore = 3*10 : baseScore = 10;

            for (var j: int = 0; j < combinaton.gridItems.length; j++) {
                var item: GridItemModel = combinaton.gridItems[j] as GridItemModel;
                item.score = baseScore * gameModel.multiplier;
                item.bonusScore = (int(j / 4)*(baseScore + 10) + int(j / 8)*(baseScore + 20) + int(j / 12) * (baseScore + 30)) * gameModel.multiplier;
            }

            dispatcher.dispatchEvent(new GameExternalEvent(GameExternalEvent.ITEMS_DELETED, _combinations));
            gameModel.logicFacade.logicGameFlow.deleteWinCombinationsItems(_combinations);

            // View changes
            gameView.isDeletingItems = true;
            deleteItems();
        }


        public function deleteItems(): void {

            _items = [];
            _scores = [];

            for (var i: int = 0; i < _combinations.length; i++) {
                var combinaton: WinCombination = _combinations[i] as WinCombination;

                // Mem items that will be animated
                for (var j: int = 0; j < combinaton.gridItems.length; j++) {
                    var item: GridItemModel = combinaton.gridItems[j] as GridItemModel;
                    var itemView: GridItemView = gridView.getItemViewByID(item.id);
                    var isInArray: Boolean = false;

                    for (var k: int = 0; k < _items.length; k++) {
                        if (_items[k] == itemView) {
                            isInArray = true;
                            break;
                        }
                    }

                    if (!isInArray) {
                        _items.push(itemView);
                        // Score
                        var scoreTextView:SampleScoreTextView = new SampleScoreTextView();
                        scoreTextView.score = item.score;
                        scoreTextView.x = itemView.x;
                        scoreTextView.y = itemView.y;
                        _scores.push(scoreTextView);
                        gameView.scoreViewManager.scoreLayerView.addChild(scoreTextView.view);
                    }

                }

            }

            // Add items to another container
            for (var l: int = 0; l < _items.length; l++) {
                var gridItemView: GridItemView = _items[l];
                gameView.gridView.removeGridItem(gridItemView);
                gameView.itemsDeleteLayer.addChild(gridItemView.view);
            }

            _finished = 0;

            // Run items animations
            for (var i2: int = 0; i2 < _items.length; i2++) {
                (_items[i2] as GridItemView).addEventListener(GameViewEvent.DELETE_ITEM_ANIMATION_COMPLETE, onDeleteItemAnimationComplete);
                (_items[i2] as GridItemView).runDeleteAnimation();
            }

            // Run scrore text animations
            for (var i3: int = 0; i3 < _scores.length; i3++) {
                (_scores[i3] as ScoreTextView).addEventListener(GameViewEvent.SCORE_TEXT_ANIMATION_COMPLETE, onScoreTextViewAnimationComplete);
                (_scores[i3] as ScoreTextView).runAnimation();
            }

            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.SPAWN_ITEMS));
        }



        private function onScoreTextViewAnimationComplete(event: GameViewEvent): void {
            var scoreTextView: ScoreTextView = event.target as ScoreTextView;

            scoreTextView.removeEventListener(GameViewEvent.SCORE_TEXT_ANIMATION_COMPLETE, onScoreTextViewAnimationComplete);

            gameView.scoreViewManager.scoreLayerView.removeChild(scoreTextView.view);
        }


        private function onDeleteItemAnimationComplete(event: GameViewEvent): void {
            var gridItemView: GridItemView = event.target as GridItemView;
            gridItemView.removeEventListener(GameViewEvent.DELETE_ITEM_ANIMATION_COMPLETE, onDeleteItemAnimationComplete);

            gameView.itemsDeleteLayer.removeChild(gridItemView.view);

            _finished++;

            if (_finished == _items.length) {
                gameView.isDeletingItems = false;
            }
        }

    }
}