package com.game.controller.internal {
    import com.game.events.GameContextEvent;
    import com.game.model.GameModel;
    import com.game.model.game_field.grid.GridModel;
import com.game.model.game_field.grid.item.GridItemModel;
import com.game.model.logic.win_combination.WinCombination;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grid.GridView;
    import com.game.view.game_filed.grid.item.base.GridItemView;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    public class ItemMouseUpCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var gameModel: GameModel;

        public function ItemMouseUpCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var gridView: GridView = gameView.gridView;
            var sItems: Array = gridView.selectedItems;

            if (!sItems || sItems.length == 0) return;

            if (sItems.length > 0) {

                gameView.itemsConnection.clear();

                for (var i: int = 0; i < sItems.length; i++) {
                    var gridItemView: GridItemView = sItems[i];
                    gridItemView.isSelected = false;
                }

                gridView.selectedItems = [];
            }

            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.STOP_ITEM_MOUSE_OVER_LISTENING));

            if (sItems.length > 2){
                //dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.LOCK_GAME_WHILE_ANIMATIONS))

                var itemsModel:Array = [];
                var gridModel:GridModel = gameModel.gridModel;
                var i:int;

                for (i = 0; i < sItems.length; i++) {
                    var gridItemView: GridItemView = sItems[i] as GridItemView;
                    itemsModel.push(gridModel.getGridItemByID(gridItemView.id));
                }

                var isAllSymbolsHaveTheSameShape:Boolean = true;

                for (i = 1; i < itemsModel.length; i++) {
                    var itemModel0: GridItemModel = itemsModel[i-1] as GridItemModel;
                    var itemModel1: GridItemModel = itemsModel[i] as GridItemModel;

                    if (gridView.getItemViewByID(itemModel0.id).shape != gridView.getItemViewByID(itemModel1.id).shape){
                        isAllSymbolsHaveTheSameShape = false;
                        break;
                    }
                }

                dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.DELETE_ITEMS, [new WinCombination(itemsModel,isAllSymbolsHaveTheSameShape)]))
            }
        }
    }
}
