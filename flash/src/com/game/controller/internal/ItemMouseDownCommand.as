package com.game.controller.internal {
    import com.game.events.GameContextEvent;
    import com.game.model.GameModel;
    import com.game.view.events.GameViewEvent;
    import com.game.view.game.GameView;
    import com.game.view.game_filed.grid.GridView;
    import com.game.view.game_filed.grid.item.base.GridItemView;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;
    import treefortress.sound.SoundModel;

    public class ItemMouseDownCommand extends Command {

        [Inject]
        public var event: GameViewEvent;
        [Inject]
        public var gameView: GameView;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var dispatcher: IEventDispatcher;


        public function ItemMouseDownCommand() {
            super();
        }


        override public function execute(): void {
            var selectedItem: GridItemView = event.data as GridItemView;
            var gridView: GridView = gameView.gridView;
            var sItems: Array = gridView.selectedItems;

            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.ITEM_SELECT);

            gridView.selectedItems = [selectedItem];
            selectedItem.isSelected = true;
            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.START_ITEM_MOUSE_OVER_LISTENING));

        }
    }
}
