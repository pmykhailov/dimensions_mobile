package com.game.controller.external_controll {
    import com.game.view.game.GameView;
    import com.greensock.TweenLite;
    import com.game.events.GameExternalEvent;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    import treefortress.sound.SoundAS;

    import treefortress.sound.SoundModel;

    /**
     * HideGameCommand class.
     * User: Paul Makarenko
     * Date: 03.11.13
     */
    public class HideGameCommand extends Command {

        [Inject]
        public var dispatcher:IEventDispatcher;
        [Inject]
        public var gameView:GameView;

        public function HideGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            SoundAS.group(SoundModel.GROUP_SFX).playFx(SoundModel.LEVEL_COMPLETESOUND);
            TweenLite.to(gameView, 0.5, {alpha:0, y:gameView.y - 20, onComplete:onHideComplte});
        }

        protected function onHideComplte():void{
            dispatcher.dispatchEvent(new GameExternalEvent(GameExternalEvent.GAME_HIDE_COMPLETE));
        }
    }
}