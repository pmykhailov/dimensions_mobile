package com.game.controller.external_controll {
import com.game.view.game.GameView;
import com.game.events.GameControllEvent;

import robotlegs.bender.bundles.mvcs.Command;

public class GameMouseClickableCommand extends Command {

    [Inject]
    public var gameView: GameView;
    [Inject]
    public var event: GameControllEvent;

    public function GameMouseClickableCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        var enable:Boolean = event.data as Boolean;
        gameView.mouseChildren = gameView.mouseEnabled = enable;
    }
}
}
