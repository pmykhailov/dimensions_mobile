package com.game.controller.external_controll {
    import com.game.view.game.GameView;

    import flash.filters.BlurFilter;
    import flash.filters.ColorMatrixFilter;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * PauseGameCommand class.
     * User: Paul Makarenko
     * Date: 06.10.13
     */
    public class PauseGameCommand extends Command {

        [Inject]
        public var gameView:GameView;

        public function PauseGameCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            gameView.mouseChildren = false;
            gameView.mouseEnabled = false;

            var r:Number=0.212671;
            var g:Number=0.715160;
            var b:Number=0.072169;

            var matrix:Array = [r, g, b, 0, 0,
                r, g, b, 0, 0,
                r, g, b, 0, 0,
                0, 0, 0, 1, 0];

            gameView.view.filters = [new BlurFilter(20,20,2), new ColorMatrixFilter(matrix)];
        }
    }
}