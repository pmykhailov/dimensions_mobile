package com.game.controller.external_controll {
    import com.game.model.GameModel;
    import com.game.settings.GameSettings;
    import com.game.view.game.GameView;
    import com.game.events.GameControllEvent;

    import flash.display.DisplayObjectContainer;
    import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.bundles.mvcs.Command;

    public class AddGameCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var event: GameControllEvent;
        [Inject]
        public var injector: Injector;


        override public function execute(): void {

            GameSettings.instance.initDefaultSettings();

            injector.map(GameModel).asSingleton();
            var gameModel: GameModel = injector.getInstance(GameModel);

            var gameView: GameView = new GameView();
            injector.map(GameView).toValue(gameView);
            (event.data.container as DisplayObjectContainer).addChild(gameView.view);
        }

    }
}
