package com.game.controller.external_controll {
import com.game.model.GameModel;
import com.game.view.game.GameView;
    import com.game.events.GameControllEvent;

    import flash.display.DisplayObjectContainer;

import org.swiftsuspenders.Injector;

import robotlegs.bender.bundles.mvcs.Command;

    /**
     * RemoveGameCommand class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class RemoveGameCommand extends Command {

        [Inject]
        public var gameView: GameView;
        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var event: GameControllEvent;
        [Inject]
        public var injector: Injector;

        public function RemoveGameCommand() {
            super();
        }


        override public function execute(): void {

            injector.unmap(GameModel);
            // needed? injector.destroyInstance(gameModel);

            injector.unmap(GameView);

           gameView.destroy();
        }
    }
}