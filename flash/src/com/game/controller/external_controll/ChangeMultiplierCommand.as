package com.game.controller.external_controll {
    import com.game.events.GameControllEvent;
    import com.game.model.GameModel;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * ChangeMultiplierCommand class.
     * User: Paul Makarenko
     * Date: 15.11.13
     */
    public class ChangeMultiplierCommand extends Command {

        [Inject]
        public var gameModel: GameModel;
        [Inject]
        public var event:GameControllEvent;

        public function ChangeMultiplierCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            gameModel.multiplier = event.data as int;
        }
    }
}