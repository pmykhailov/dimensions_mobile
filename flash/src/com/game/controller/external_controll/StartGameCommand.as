package com.game.controller.external_controll {
    import com.game.events.GameContextEvent;
    import com.game.model.GameModel;
    import com.game.view.game.GameView;

    import flash.events.IEventDispatcher;

    import robotlegs.bender.bundles.mvcs.Command;

    /**
     * StartGameCommand class.
     * User: Paul Makarenko
     * Date: 02.10.13
     */
    public class StartGameCommand extends Command {

        [Inject]
        public var gameModel:GameModel;
        [Inject]
        public var gameView:GameView;
        [Inject]
        public var dispatcher:IEventDispatcher;

        public function StartGameCommand() {
            super();
        }


        override public function execute(): void {
            gameModel.logicFacade.logicGeneration.randomizeGameFiled(gameModel);

            gameView.gridView.removeGridItems();
            gameView.gridView.createGridItems(gameModel.gridModel);

            dispatcher.dispatchEvent(new GameContextEvent(GameContextEvent.UNLOCK_GAME_AFTER_ANIMATIONS))
        }
    }
}