package com.game.model {
    import by.lord_xaoca.robotlegs2.BaseActor;

    import com.game.model.game_field.grid.GridModel;
    import com.game.model.logic.LogicFacade;

    public class GameModel extends BaseActor {

        protected var _gridModel: GridModel;
        protected var _logicFacade: LogicFacade;
        protected var _multiplier: int;


        public function GameModel() {

            super();

            createGridModel();
            createLogicFacade();
            _multiplier = 1;
        }


        public function get gridModel(): GridModel {
            return _gridModel;
        }


        public function get logicFacade(): LogicFacade {
            return _logicFacade;
        }


        public function get multiplier(): int {
            return _multiplier;
        }


        public function set multiplier(value: int): void {
            _multiplier = value;
        }


        protected function createLogicFacade(): void {
            _logicFacade = new LogicFacade();
        }


        protected function createGridModel(): void {
            _gridModel = new GridModel();
        }

    }
}
