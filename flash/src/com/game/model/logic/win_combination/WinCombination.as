package com.game.model.logic.win_combination {
    import com.game.model.game_field.grid.item.GridItemModel;

    public class WinCombination {
    private var _gridItems:Array;
    private var _isAllSymbolsHaveTheSameShape:Boolean;

    public function WinCombination(gridItems:Array, isAllSymbolsHaveTheSameShape:Boolean) {
        _gridItems = gridItems;
        _isAllSymbolsHaveTheSameShape = isAllSymbolsHaveTheSameShape;
    }

    public function get gridItems():Array /*GridItemModel*/ {
        return _gridItems;
    }

    public function get totalScore():int {
        var sum:int = 0;

        for (var i: int = 0; i < _gridItems.length; i++) {
            var gridItemModel: GridItemModel = _gridItems[i];
            sum += gridItemModel.totalScore;
        }

        return sum;
    }

    public function get totalBonusScore():int {
        var sum:int = 0;

        for (var i: int = 0; i < _gridItems.length; i++) {
            var gridItemModel: GridItemModel = _gridItems[i];
            sum += gridItemModel.bonusScore;
        }

        return sum;
    }

        public function get isAllSymbolsHaveTheSameShape():Boolean {
            return _isAllSymbolsHaveTheSameShape;
        }
    }
}