package com.game.model.logic.grid {
    import com.game.model.GameModel;
    import com.game.model.game_field.grid.GridModel;
    import com.game.model.game_field.grid.item.GridItemModel;
    import com.game.model.game_field.grid.item.enum.GridItemTypeEnum;
    import com.game.model.logic.win_combination.WinCombination;
    import com.game.model.misc.IDGenerator;

    /**
     * Game flow logic
     */
    public class LogicGameFlow {
        public function LogicGameFlow() {
        }


        public function swapItems(gridModel: GridModel, item1ID: uint, item2ID: uint): void {
            gridModel.swapItems(gridModel.getGridItemByID(item1ID), gridModel.getGridItemByID(item2ID));
        }


        public function deleteWinCombinationsItems(combinations: Array): void {

            for (var i: int = 0; i < combinations.length; i++) {
                var combination: WinCombination = combinations[i] as WinCombination;

                for (var j: int = 0; j < combination.gridItems.length; j++) {
                    (combination.gridItems[j] as GridItemModel).type = GridItemTypeEnum.EMPTY;
                }
            }
        }


        public function spawnItems(gameModel: GameModel): void {
            var gridModel: GridModel = gameModel.gridModel;
            var TMP_ITEM_TYPE: int = GridItemTypeEnum.TEMPORARY;
            var i: int;
            var j: int;

            // Bullbe deleted items (they all have EMPTY type) to the top of grid matrix
            while (gameModel.logicFacade.logicInfo.containesEmptyItems(gameModel)) {
                var isEmptItemFound: Boolean = false;

                for (i = gridModel.rows - 1; i >= 0; i--) {
                    for (j = 0; j < gridModel.cols; j++) {
                        if (gridModel.getGridItem(i, j).type == GridItemTypeEnum.EMPTY) {
                            var isItemToSwapWithEmptyFound: Boolean = false;

                            isEmptItemFound = true;

                            for (var ii: int = i - 1; ii >= 0; ii--) {
                                var itemForSwap: GridItemModel = gridModel.getGridItem(ii, j);

                                if (itemForSwap.type != GridItemTypeEnum.EMPTY && itemForSwap.type != GridItemTypeEnum.NON_EXISTING) {
                                    gridModel.swapItems(itemForSwap, gridModel.getGridItem(i, j));
                                    isItemToSwapWithEmptyFound = true;
                                    break;
                                }
                            }

                            if (!isItemToSwapWithEmptyFound) {
                                gridModel.getGridItem(i, j).type = TMP_ITEM_TYPE;
                            }
                        }
                    }

                    if (isEmptItemFound) {
                        break;
                    }
                }

            }

            // Generate type for deleted items
            for (i = 0; i < gridModel.rows; i++) {
                for (j = 0; j < gridModel.cols; j++) {
                    if (gridModel.getGridItem(i, j).type == TMP_ITEM_TYPE) {
                        gridModel.getGridItem(i, j).id = IDGenerator.id;
                        gridModel.getGridItem(i, j).type = gameModel.logicFacade.logicGeneration.getRandomItemType(gridModel);
                    }
                }
            }
        }

    }
}