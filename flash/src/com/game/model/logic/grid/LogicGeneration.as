package com.game.model.logic.grid {
    import com.game.model.GameModel;
    import com.game.model.game_field.grid.GridModel;
    import com.game.model.game_field.grid.item.GridItemModel;
    import com.game.model.game_field.grid.item.enum.GridItemTypeEnum;

    /**
     * Here localed all logic that deals with items generation
     */
    public class LogicGeneration {
        public function LogicGeneration() {

        }


        /**
         * Only empty items receives new random type
         */
        public function randomizeEmptyGridItems(gameModel: GameModel): void {
            var gridModel: GridModel = gameModel.gridModel;

            for (var i: int = 0; i < gridModel.rows; i++) {
                for (var j: int = 0; j < gridModel.cols; j++) {
                    var gridItemModel: GridItemModel = gridModel.getGridItem(i, j);
                    if (gridItemModel.type == GridItemTypeEnum.EMPTY) {
                        gridItemModel.type = getRandomItemType(gridModel);
                    }
                }
            }
        }


        /**
         * Fill game field with random type items
         */
        public function randomizeGameFiled(gameModel: GameModel): void {
            var gridModel: GridModel = gameModel.gridModel;

            for (var i: int = 0; i < gridModel.rows; i++) {
                for (var j: int = 0; j < gridModel.cols; j++) {
                    var gridItemModel: GridItemModel = gridModel.getGridItem(i, j);
                    if (gridItemModel.type != GridItemTypeEnum.NON_EXISTING) {
                        gridItemModel.type = getRandomItemType(gridModel);
                    }
                }
            }
        }


        /**
         * Random item type generation
         */
        public function getRandomItemType(gridModel: GridModel): int {
            var symbols: Array;

            symbols = gridModel.usualGridItemTypes;

            return symbols[int(Math.random() * symbols.length)];
        }

    }
}