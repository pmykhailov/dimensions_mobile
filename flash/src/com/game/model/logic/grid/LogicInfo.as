package com.game.model.logic.grid {
    import com.game.model.GameModel;
    import com.game.model.game_field.grid.GridModel;
    import com.game.model.game_field.grid.item.GridItemModel;
    import com.game.model.game_field.grid.item.GridItemPosition;
    import com.game.model.game_field.grid.item.enum.GridItemTypeEnum;

    /**
     * Methods that provide some information about game field
     */
    public class LogicInfo {

        public function LogicInfo() {
        }


        /**
         * Defines if grid has empty items
         */
        public function containesEmptyItems(gameModel: GameModel): Boolean {
            var gridModel: GridModel = gameModel.gridModel;

            for (var i: int = 0; i < gridModel.rows; i++) {
                for (var j: int = 0; j < gridModel.cols; j++) {
                    if (gridModel.getGridItem(i, j).type == GridItemTypeEnum.EMPTY) {
                        return true;
                    }
                }
            }

            return false;
        }


        /**
         * Checks if cells are neighbors
         */
        public function isNeighbors(gameModel: GameModel, item1: GridItemModel, item2: GridItemModel): Boolean {
            var gridModel: GridModel = gameModel.gridModel;
            var pos1: GridItemPosition = gridModel.getGridItemPosition(item1.id);
            var pos2: GridItemPosition = gridModel.getGridItemPosition(item2.id);

            return Math.abs(pos1.row - pos2.row) <= 1 && Math.abs(pos1.col - pos2.col) <= 1;
        }
    }
}