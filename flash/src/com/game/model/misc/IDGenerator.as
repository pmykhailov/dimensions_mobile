package com.game.model.misc  {
public class IDGenerator {
    private static var _id:uint = 0;

    public static function get id():uint {
        _id++;
        return _id;
    }

    public static function reset():void {
        _id = 0;
    }

    public function IDGenerator() {

    }
}
}