package com.game.model.game_field.grid {

import com.game.model.game_field.grid.item.GridItemModel;
import com.game.model.game_field.grid.item.GridItemPosition;
import com.game.model.game_field.grid.item.enum.GridItemTypeEnum;
import com.game.model.misc.IDGenerator;
import com.game.settings.GameSettings;

public class GridModel {
    private var _rows:int;
    private var _cols:int;
    private var _grid:Array;

    public function GridModel() {
        init();
    }

    public function get rows():int {
        return _rows;
    }

    public function get cols():int {
        return _cols;
    }

    /**
     * Type of usual items that are currently used in game
     */
    public function get usualGridItemTypes():Array {
        return GameSettings.instance.usualGridItemTypes;
    }

    public function getGridItem(row:int, col:int):GridItemModel {
        if (row < 0 || row >= _rows || col < 0 || col >= _cols) return null;
        return _grid[row][col] as GridItemModel;
    }

    public function getGridItemByID(id:uint):GridItemModel {
        for (var i:int = 0; i < _rows; i++) {
            for (var j:int = 0; j < _cols; j++) {
                var item:GridItemModel = _grid[i][j] as GridItemModel;
                if (item.id == id) {
                    return item;
                }
            }
        }

        return null;
    }

    public function getGridItemPosition(id:uint):GridItemPosition {
        for (var i:int = 0; i < _rows; i++) {
            for (var j:int = 0; j < _cols; j++) {
                var item:GridItemModel = _grid[i][j] as GridItemModel;
                if (item.id == id) {
                    return new GridItemPosition(i, j);
                }
            }
        }

        return null;
    }

    public function swapItems(item1:GridItemModel, item2:GridItemModel):void {
        var tmp:GridItemModel;
        var pos1:GridItemPosition = getGridItemPosition(item1.id);
        var pos2:GridItemPosition = getGridItemPosition(item2.id);

        tmp = _grid[pos1.row][pos1.col];
        _grid[pos1.row][pos1.col] = _grid[pos2.row][pos2.col];
        _grid[pos2.row][pos2.col] = tmp;
    }

    protected function init():void {
        initDimensions();
        initBlankGameFiled();
    }

    protected function initDimensions():void {
        _rows = GameSettings.instance.gameFieldRows;
        _cols = GameSettings.instance.gameFieldCols;
    }

    protected function initBlankGameFiled():void {
        _grid = new Array();

        for (var i:int = 0; i < _rows; i++) {
            _grid[i] = new Array();
            for (var j:int = 0; j < _cols; j++) {
                _grid[i][j] = new GridItemModel(IDGenerator.id, GridItemTypeEnum.EMPTY);

                // If we have some shape of the grid
                if (GameSettings.instance.shape){
                    _grid[i][j].type = GameSettings.instance.shape[i][j];
                }
            }
        }
    }

}
}