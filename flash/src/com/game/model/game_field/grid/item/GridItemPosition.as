package com.game.model.game_field.grid.item {
public class GridItemPosition {
    private var _row:int;
    private var _col:int;

    public function GridItemPosition(row:int, col:int) {
        _row = row;
        _col = col;
    }

    public function get row():int {
        return _row;
    }

    public function get col():int {
        return _col;
    }

}
}