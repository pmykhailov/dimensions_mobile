package com.game.model.game_field.grid.item {
    public class GridItemModel {
        private var _id: uint;
        private var _type: int;
        private var _score: int;
        private var _bonusScore: int;
        private var _totalScore: int;

        public function GridItemModel(id: uint, type: int) {
            _id = id;
            _type = type;
        }


        public function get id(): uint {
            return _id;
        }


        public function set id(value: uint): void {
            _id = value;
        }


        public function get type(): int {
            return _type;
        }


        public function set type(value: int): void {
            _type = value;
        }


        public function get score(): int {
            return _score;
        }


        public function set score(value: int): void {
            _score = value;
        }


        public function get bonusScore(): int {
            return _bonusScore;
        }


        public function set bonusScore(value: int): void {
            _bonusScore = value;
        }


        public function get totalScore(): int {
            return _score + _bonusScore;
        }
    }
}