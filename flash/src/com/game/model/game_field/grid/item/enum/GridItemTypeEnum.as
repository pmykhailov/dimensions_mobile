package com.game.model.game_field.grid.item.enum {
public class GridItemTypeEnum {

    /** Used by model for calculations **/
    public static const TEMPORARY:int = -3;
    /** Defines cell where items can't be spawned **/
    public static const NON_EXISTING:int = -2;
    /** Defines empty cell **/
    public static const EMPTY:int = -1;

    public static const TYPE_MAX_ID:int = 25;

    public static const COLORS_COUNT:int = 5;

    public function GridItemTypeEnum() {

    }
}
}
