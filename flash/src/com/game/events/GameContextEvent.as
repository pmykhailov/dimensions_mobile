package com.game.events {
import flash.events.Event;

/**
 *  This type of event is send from commands
 */
public class GameContextEvent extends Event {

    public static const START_ITEM_MOUSE_OVER_LISTENING:String = "startItemMouseOverListening";
    public static const STOP_ITEM_MOUSE_OVER_LISTENING:String = "stopItemMouseOverListening";
    public static const DELETE_ITEMS:String = "deleteItems";
    public static const SPAWN_ITEMS:String = "spawnItems";
    public static const LOCK_GAME_WHILE_ANIMATIONS:String = "lockGameWhileAnimations";
    public static const UNLOCK_GAME_AFTER_ANIMATIONS:String = "unlockGameAfterAnimations";

    private var _data:Object;

    public function GameContextEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }

    override public function clone():Event {
        return new GameContextEvent(type, _data, bubbles, cancelable);
    }
}
}
