package com.game.events
{
	import flash.events.Event;

    /**
     * Events to external system
     */
	public class GameExternalEvent extends Event
	{

		public static const ITEMS_DELETED:String = "itemsDeleted";
		public static const GAME_NO_MORE_MOVES:String = "gameNoMoreMoves";
		public static const GAME_STOPPED:String = "gameStopped";
		public static const GAME_HIDE_COMPLETE:String = "gameHideComplete";

		private var _data:Object;
		
		public function GameExternalEvent(type:String, data:Object = null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_data = data;
		}
		
		public function get data():Object
		{
			return _data;
		}

		public function set data(value:Object):void
		{
			_data = value;
		}

		override public function clone():Event
		{
			return new GameExternalEvent(type, _data, bubbles, cancelable);
		}
		
	}
}