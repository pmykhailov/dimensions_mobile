package com.game.events {
    import flash.events.Event;

    /**
     * Commands for game controlling
     */
    public class GameControllEvent extends Event {
        /** Starts the game **/
        public static const START_GAME: String = "startGame";
        /** Pauses game **/
        public static const PAUSE_GAME: String = "pauseGame";
        /** Game will be unpaused. All changes that were applied by pause  will be reverted **/
        public static const RESUME_GAME: String = "resumeGame";
        /** Stops game. Used for instance when we quit to main menu and doesn't need game any more, or when time is up**/
        public static const STOP_GAME: String = "stopGame";

        public static const HIDE_GAME: String = "hideGame";

        public static const ADD_GAME: String = "addGame";
        public static const REMOVE_GAME: String = "removeGame";
        public static const GAME_MOUSE_CLICKABLE: String = "gameMouseClickable";

        public static const CHANGE_MULTIPLIER: String = "changeMultiplier";

        private var _data: Object;

        public function GameControllEvent(type: String, data:Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new GameControllEvent(type, _data, bubbles, cancelable);
        }
    }
}