package com.game.settings {
public class TimeSettings {

    private var k:int = 1;

    public var existing_items_fall_time:Number = 0.5*k;
    public var new_items_fall_time:Number = 0.2*k;

    public function TimeSettings() {
    }
}
}
