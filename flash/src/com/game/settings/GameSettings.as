package com.game.settings {
    import com.game.model.game_field.grid.item.enum.GridItemTypeEnum;

    public class GameSettings {

        private static var _instance: GameSettings;

        public static function get instance(): GameSettings {
            if (!_instance) _instance = new GameSettings();
            return _instance;
        }


        private var _itemWidth: int;
        private var _itemHeight: int;
        /** 2D matrix with elements than can be GridItemTypeEnum.NON_EXISTING or zero**/
        private var _shape:Array;
        /** Data for background of the items **/
        private var _gameFieldRows: int;
        private var _gameFieldCols: int;
        private var _usualGridItemTypes:Array;
        private var _timeSettings: TimeSettings;

        public function GameSettings() {
            initDefaultSettings();
        }


        public function get timeSettings(): TimeSettings {
            return _timeSettings;
        }


        public function get itemWidth(): int {
            return _itemWidth;
        }


        public function set itemWidth(value: int): void {
            _itemWidth = value;
        }


        public function get itemHeight(): int {
            return _itemHeight;
        }


        public function set itemHeight(value: int): void {
            _itemHeight = value;
        }


        public function get gameFieldRows(): int {
            return _gameFieldRows;
        }


        public function set gameFieldRows(value: int): void {
            _gameFieldRows = value;
        }


        public function get gameFieldCols(): int {
            return _gameFieldCols;
        }


        public function set gameFieldCols(value: int): void {
            _gameFieldCols = value;
        }


        public function initDefaultSettings(): void {
            _timeSettings = new TimeSettings();

            _itemWidth = 90;
            _itemHeight = 90;

            _gameFieldCols = 8;
            _gameFieldRows = 8;

            _shape = [];
            for (var i: int = 0; i < _gameFieldRows; i++) {
                _shape[i] = [];
                for (var j: int = 0; j < _gameFieldCols; j++) {
                    _shape[i][j] = GridItemTypeEnum.EMPTY;
                }
            }

            _usualGridItemTypes = [];
            for (var i: int = 0; i < GridItemTypeEnum.TYPE_MAX_ID; i++) {
                _usualGridItemTypes[i] = i + 1;

            }

        }


        public function get usualGridItemTypes(): Array {
            return _usualGridItemTypes;
        }


        public function set usualGridItemTypes(value: Array): void {
            _usualGridItemTypes = value;
        }


        public function get shape():Array {
            return _shape;
        }

        public function set shape(value:Array):void {
            _shape = value;
        }

    }
}