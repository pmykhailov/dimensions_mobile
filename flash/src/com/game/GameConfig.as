package com.game {
    import com.game.controller.external_controll.AddGameCommand;
    import com.game.controller.external_controll.ChangeMultiplierCommand;
    import com.game.controller.external_controll.GameMouseClickableCommand;
    import com.game.controller.external_controll.HideGameCommand;
    import com.game.controller.external_controll.PauseGameCommand;
    import com.game.controller.external_controll.RemoveGameCommand;
    import com.game.controller.external_controll.ResumeGameCommand;
    import com.game.controller.external_controll.StartGameCommand;
    import com.game.controller.external_controll.StopGameCommand;
    import com.game.controller.internal.DeleteItemsCommand;
    import com.game.controller.internal.ItemMouseDownCommand;
    import com.game.controller.internal.ItemMouseOverCommand;
    import com.game.controller.internal.ItemMouseUpCommand;
    import com.game.controller.internal.SpawnItemsCommand;
    import com.game.events.GameContextEvent;
    import com.game.events.GameControllEvent;
    import com.game.view.events.GameViewEvent;
    import com.game.view.game_filed.grid.GridView;
    import com.game.view.game_filed.grid.GridViewMediator;

    import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;

    public class GameConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {

            eventCommandMap.map(GameViewEvent.ITEM_MOUSE_DOWN).toCommand(ItemMouseDownCommand);
            eventCommandMap.map(GameViewEvent.ITEM_MOUSE_OVER).toCommand(ItemMouseOverCommand);
            eventCommandMap.map(GameViewEvent.ITEM_MOUSE_UP).toCommand(ItemMouseUpCommand);

            eventCommandMap.map(GameContextEvent.DELETE_ITEMS).toCommand(DeleteItemsCommand);
            eventCommandMap.map(GameContextEvent.SPAWN_ITEMS).toCommand(SpawnItemsCommand);

            eventCommandMap.map(GameControllEvent.START_GAME).toCommand(StartGameCommand);
            eventCommandMap.map(GameControllEvent.STOP_GAME).toCommand(StopGameCommand);
            eventCommandMap.map(GameControllEvent.HIDE_GAME).toCommand(HideGameCommand);
            eventCommandMap.map(GameControllEvent.PAUSE_GAME).toCommand(PauseGameCommand);
            eventCommandMap.map(GameControllEvent.RESUME_GAME).toCommand(ResumeGameCommand);
            eventCommandMap.map(GameControllEvent.ADD_GAME).toCommand(AddGameCommand);
            eventCommandMap.map(GameControllEvent.REMOVE_GAME).toCommand(RemoveGameCommand);
            eventCommandMap.map(GameControllEvent.GAME_MOUSE_CLICKABLE).toCommand(GameMouseClickableCommand);
            eventCommandMap.map(GameControllEvent.CHANGE_MULTIPLIER).toCommand(ChangeMultiplierCommand);
        }


        private function mapMediators(): void {
            mediatorMap.map(GridView).toMediator(GridViewMediator);
        }


        private function mapInjections(): void {
        }


        private function init(): void {
        }
    }
}
