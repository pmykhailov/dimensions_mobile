/**
 * Created by Pasha on 24.07.2016.
 */
package com.app.controller {
import com.app.events.ApplicationEvent;
import com.app.model.ApplicationModel;
import com.app.model.UserModel;
import com.app.model.service.RequestVO;
import com.screens.view.layer.ScreensLayerView;
import com.screens.view.screens.loading.LoadingScreen;

import flash.events.IEventDispatcher;
import flash.net.URLVariables;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;

public class SyncUserDataCommand extends Command {

    [Inject]
    public var contextView: ContextView;
    [Inject]
    public var screensLayerView: ScreensLayerView;
    [Inject]
    public var dispatcher: IEventDispatcher;
    [Inject]
    public var applicationModel: ApplicationModel;
    [Inject]
    public var context:IContext;

    public function SyncUserDataCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        applicationModel.httpService.updateUserData(new RequestVO(null, null), applicationModel.userModel.id, applicationModel.userModel.name);
        applicationModel.httpService.submitScore(new RequestVO(onSuccessCreated, onErrorHandler), applicationModel.userModel.id, applicationModel.userModel.highScore, applicationModel.userModel.highScoreTimeSpend);
    }

    private function onSuccessCreated(data:Object):void {
        context.release(this);
    }

    private function onErrorHandler():void {
        context.release(this);
    }
}
}
