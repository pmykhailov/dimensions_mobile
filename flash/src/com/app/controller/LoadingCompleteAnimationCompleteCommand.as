/**
 * Created by Pasha on 24.07.2016.
 */
package com.app.controller {
import com.screens.events.ScreensContextEvent;
import com.screens.view.screens.enum.ScreensTypeEnum;

import flash.events.IEventDispatcher;
import flash.text.TextField;
import flash.text.TextFormat;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;

public class LoadingCompleteAnimationCompleteCommand extends Command {

    [Inject]
    public var contextView: ContextView;

    [Inject]
    public var dispatcher: IEventDispatcher;

    public function LoadingCompleteAnimationCompleteCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var fps:Number = FPSCounter.instance.minMonitoredFPS;
        FPSCounter.instance.stopMonitor();
        contextView.view.removeChild(FPSCounter.instance);
        /*
        var tf:TextField = new TextField();
        var format:TextFormat = new TextFormat();
        format.color = 0xFFFFFF;
        format.size = 40;
        tf.text = fps + "";
        tf.setTextFormat(format);
        contextView.view.addChild(tf);
        */
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.START}));
    }
}
}
