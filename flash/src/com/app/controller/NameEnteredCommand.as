/**
 * Created by Pasha on 24.07.2016.
 */
package com.app.controller {
import com.app.events.ApplicationEvent;
import com.app.model.ApplicationModel;
import com.screens.events.ScreensContextEvent;
import com.screens.view.layer.ScreensLayerView;
import com.screens.view.screens.enum.ScreensTypeEnum;
import com.screens.view.screens.loading.LoadingScreen;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

public class NameEnteredCommand extends Command {

    [Inject]
    public var event:ScreensContextEvent;

    [Inject]
    public var dispatcher: IEventDispatcher;

    [Inject]
    public var applicationModel: ApplicationModel;

    [Inject]
    public var screensLayerView: ScreensLayerView;


    public function NameEnteredCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        // Save name
        var name:String = event.data.name;
        applicationModel.userModel.name = name;
        applicationModel.soService.saveData("name", name);

        // Give id
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.LOADING}));
        (screensLayerView.activeScreen as LoadingScreen).state = LoadingScreen.STATE_LOADING;
        dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.PROVIDE_USER_ID));
    }
}
}
