/**
 * Created by Pasha on 24.07.2016.
 */
package com.app.controller {
import com.app.events.ApplicationEvent;
import com.app.model.ApplicationModel;
import com.screens.view.layer.ScreensLayerView;
import com.screens.view.screens.loading.LoadingScreen;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;

public class InitApplicationCompleteCommand extends Command {

    [Inject]
    public var contextView: ContextView;
    [Inject]
    public var screensLayerView: ScreensLayerView;
    [Inject]
    public var dispatcher: IEventDispatcher;
    [Inject]
    public var applicationModel: ApplicationModel;
    [Inject]
    public var context:IContext;

    public function InitApplicationCompleteCommand() {
        super();


    }

    override public function execute():void {
        super.execute();

        contextView.view.addChild(FPSCounter.instance);
        FPSCounter.instance.startMonitor();

        (screensLayerView.activeScreen as LoadingScreen).state = LoadingScreen.STATE_LOADING_COMPLETE;

        dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.SYNC_USER_DATA));

        //screensLayerView.view.stage.addChild(new FPSCounter());
    }
}
}
