/**
 * Created by Paul on 7/29/16.
 */
package com.app.controller {
import com.app.model.ApplicationModel;

import robotlegs.bender.bundles.mvcs.Command;

public class SaveModelToSharedObjectCommand extends Command {

    [Inject]
    public var applicationModel: ApplicationModel;

    public function SaveModelToSharedObjectCommand() {

    }

    override public function execute():void {
        super.execute();

        // Saved data will be read in InitApplicationCommand
        var modelsVOs:Array = [
            {model:applicationModel, propertyNames:["gameRulesWereShown"]},
            {model:applicationModel.userModel, propertyNames:["id","name", "localScoresHistory"]},
            {model:applicationModel.settingsModel, propertyNames:["soundVolume", "musicVolume"]}
        ];
        
        var propertyNames:Array;
        for (var i:int = 0; i < modelsVOs.length; i++) {
            propertyNames = modelsVOs[i].propertyNames;
            
            for (var j:int = 0; j < propertyNames.length; j++) {
                applicationModel.soService.saveData(propertyNames[j], modelsVOs[i].model[propertyNames[j]]);
            }
        }

        applicationModel.soService.flush();
    }
}
}
