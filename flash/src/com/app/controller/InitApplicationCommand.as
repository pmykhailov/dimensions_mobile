/**
 * Created by Pasha on 24.07.2016.
 */
package com.app.controller {
import com.app.events.ApplicationEvent;
import com.app.model.ApplicationModel;
import com.app.model.UserModel;
import com.app.model.service.RequestVO;
import com.app.model.service.SharedObjectService;
import com.screens.events.ScreensContextEvent;
import com.screens.view.layer.ScreensLayerView;
import com.screens.view.screens.enum.ScreensTypeEnum;
import com.screens.view.screens.loading.LoadingScreen;

import flash.events.IEventDispatcher;
import flash.net.URLVariables;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;

public class InitApplicationCommand extends Command {

    [Inject]
    public var contextView: ContextView;
    [Inject]
    public var screensLayerView: ScreensLayerView;
    [Inject]
    public var dispatcher: IEventDispatcher;
    [Inject]
    public var applicationModel: ApplicationModel;
    [Inject]
    public var context:IContext;

    public function InitApplicationCommand() {
        super();
    }

    override public function execute():void {
        super.execute();



        //var headerView:HeaderView = new HeaderView();
        //headerView.data = settingsModel;
        //contextView.view.addChild(headerView.view);


        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.INIT));

        // Read data from shared object
        // Data is saved in this command - SaveModelToSharedObjectCommand
        var i:int;
        var j:int;

        var modelsVOs:Array = [
            {model:applicationModel, propertyNames:["gameRulesWereShown"], defaultValues:[false]},
            {model:applicationModel.userModel, propertyNames:["id","name", "localScoresHistory"], defaultValues:[UserModel.DEFAULT_ID, UserModel.DEFAULT_NAME, []]},
            {model:applicationModel.settingsModel, propertyNames:["soundVolume", "musicVolume"], defaultValues:[1, 1]}
        ];


        var propertyNames:Array;
        var defaultValues:Array;
        var propertyName:String;
        var propertyValue:Object;

        for (i = 0; i < modelsVOs.length; i++) {
            
            propertyNames = modelsVOs[i].propertyNames;
            defaultValues = modelsVOs[i].defaultValues;
            
            for (j = 0; j < propertyNames.length; j++) {
                propertyName = propertyNames[j];
                propertyValue = defaultValues[j];

                if (applicationModel.soService.hasData(propertyName)) {
                    modelsVOs[i].model[propertyName] = applicationModel.soService.readData(propertyName);
                }else{
                    modelsVOs[i].model[propertyName] = propertyValue;
                }

            }
        }
        
        // Init sounds
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.INIT_SOUNDS));


        if (applicationModel.userModel.name == UserModel.DEFAULT_NAME) {
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.ENTER_NAME}));
        } else {
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, {type: ScreensTypeEnum.LOADING}));
            (screensLayerView.activeScreen as LoadingScreen).state = LoadingScreen.STATE_LOADING;
            dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.PROVIDE_USER_ID));
        }
    }

}
}
