/**
 * Created by Pasha on 24.07.2016.
 */
package com.app.controller {
import com.app.events.ApplicationEvent;
import com.app.model.ApplicationModel;
import com.app.model.UserModel;
import com.app.model.service.RequestVO;
import com.screens.view.layer.ScreensLayerView;
import com.screens.view.screens.loading.LoadingScreen;

import flash.events.IEventDispatcher;
import flash.net.URLVariables;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;

public class ProvideUserIDCommand extends Command {

    [Inject]
    public var contextView: ContextView;
    [Inject]
    public var screensLayerView: ScreensLayerView;
    [Inject]
    public var dispatcher: IEventDispatcher;
    [Inject]
    public var applicationModel: ApplicationModel;
    [Inject]
    public var context:IContext;

    public function ProvideUserIDCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        // Check if user is given an id
        if (applicationModel.userModel.id == UserModel.DEFAULT_ID) {
            context.detain(this);

            (screensLayerView.activeScreen as LoadingScreen).state = LoadingScreen.STATE_LOADING;;
            applicationModel.httpService.createUser(new RequestVO(onUserCreated, onErrorHandler), applicationModel.userModel.name);
        } else {
            dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.INIT_COMPLETE));
        }
    }

    private function onUserCreated(data:Object):void {
        context.release(this);
        var vars:URLVariables = new URLVariables(data as String);

        applicationModel.userModel.id = vars.id_user;

        applicationModel.soService.saveData("id", applicationModel.userModel.id);
        applicationModel.soService.saveData("name", applicationModel.userModel.name);

        dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.INIT_COMPLETE));
    }

    private function onErrorHandler():void {
        context.release(this);

        (screensLayerView.activeScreen as LoadingScreen).state = LoadingScreen.STATE_LOADING_FAILED;
    }
}
}
