/**
 * Created by Pasha on 23.05.2016.
 */
package com.app.model {
public class UserModel {
    public static const DEFAULT_ID:uint = 0;
    public static const DEFAULT_NAME:String = "";

    private var _id:uint;
    private var _name:String;
    private var _localScoresHistory:Array; // of Object {score: 999, time: 999}
    private var _lastScore:uint;
    private var _lastTime:uint;

    public function UserModel(id:uint, name:String = "") {
        _id = id;
        _name = name;
    }

    public function get id():uint {
        return _id;
    }

    public function set id(value:uint):void {
        _id = value;
    }

    public function get name():String {
        return _name;
    }

    public function set name(value:String):void {
        _name = value;
    }

    public function get localScoresHistory():Array {
        return _localScoresHistory;
    }

    public function set localScoresHistory(value:Array):void {
        _localScoresHistory = value;
    }

    public function get highScore():uint {
        if (_localScoresHistory && _localScoresHistory.length > 0) {
            return _localScoresHistory[0].score;
        }

        return 0;
    }

    public function get highScoreTimeSpend():uint {
        if (_localScoresHistory && _localScoresHistory.length > 0) {
            return _localScoresHistory[0].time;
        }

        return 0;
    }

    public function get lastScore():uint {
        return _lastScore;
    }

    public function set lastScore(value:uint):void {
        _lastScore = value;
    }

    public function get lastTime():uint {
        return _lastTime;
    }

    public function set lastTime(value:uint):void {
        _lastTime = value;
    }
}
}
