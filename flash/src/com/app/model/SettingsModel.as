/**
 * Created by Paul on 7/29/16.
 */
package com.app.model {
public class SettingsModel {

    private var _soundVolume:Number;
    private var _musicVolume:Number;

    public function SettingsModel() {

    }

    public function get soundVolume():Number {
        return _soundVolume;
    }

    public function set soundVolume(value:Number):void {
        _soundVolume = value;
    }

    public function get musicVolume():Number {
        return _musicVolume;
    }

    public function set musicVolume(value:Number):void {
        _musicVolume = value;
    }
}
}
