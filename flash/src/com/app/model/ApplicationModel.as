package com.app.model {
import by.lord_xaoca.robotlegs2.BaseActor;

import com.app.model.service.HttpService;
import com.app.model.service.SharedObjectService;

/**
 * LevelsModel class.
 * User: Paul Makarenko
 * Date: 12.10.13
 */
public class ApplicationModel extends BaseActor {

    private var _soService:SharedObjectService;
    private var _httpService:HttpService;

    private var _userModel:UserModel;
    private var _settingsModel:SettingsModel;
    private var _gameRulesWereShown:Boolean;

    public function ApplicationModel() {
        super();
        _init();
    }


    private function _init():void {
        _userModel = new UserModel(UserModel.DEFAULT_ID);
        _settingsModel = new SettingsModel();

        _httpService = new HttpService();
        _soService = new SharedObjectService();
    }

    public function get maxSavedScores():int {
        return 5;
    }
    

    public function get gameRulesWereShown():Boolean {
        return _gameRulesWereShown;
    }

    public function set gameRulesWereShown(value:Boolean):void {
        _gameRulesWereShown = value;
    }
    
    public function get httpService():HttpService {
        return _httpService;
    }

    public function get userModel():UserModel {
        return _userModel;
    }

    public function get soService():SharedObjectService {
        return _soService;
    }

    public function get settingsModel():SettingsModel {
        return _settingsModel;
    }

    public function set settingsModel(value:SettingsModel):void {
        _settingsModel = value;
    }
}
}