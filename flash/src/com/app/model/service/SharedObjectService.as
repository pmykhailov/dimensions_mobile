package com.app.model.service {
import com.app.model.*;
import com.app.model.UserModel;

import flash.net.SharedObject;

    public class SharedObjectService {

        private var so: SharedObject;


        public function SharedObjectService() {
            _init();
        }

        private function _init(): void {
            so = SharedObject.getLocal("games.dimensions");
        }

        public function readData(propertyName:String):Object {
            return so.data[propertyName];
        }

        public function hasData(propertyName:String):Object {
            return so.data.hasOwnProperty(propertyName);
        }

        public function saveData(propertyName: String, propertyValue:Object): void {
            so.data[propertyName] = propertyValue;
        }
        
        public function flush():void {
            so.flush();
        }
        
    }
}
