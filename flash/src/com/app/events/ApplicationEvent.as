package com.app.events {
import com.screens.events.*;
    import flash.events.Event;

    public class ApplicationEvent extends Event {

        public static const INIT_APPLICATION: String = "initApplication";
        public static const PROVIDE_USER_ID: String = "provideUserID";
        public static const INIT_COMPLETE: String = "initApplicationComplete";
        public static const SEND_BASE_USER_DATA_TO_SERVER: String = "sendBaseUserDataToServer";
        public static const SAVE_MODEL_TO_SHARED_OBJECT: String = "saveModelToSharedObject";
        public static const SYNC_USER_DATA: String = "syncUserData";

        private var _data: Object;


        public function ApplicationEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new ApplicationEvent(type, _data, bubbles, cancelable);
        }
    }
}