package com.app {
import com.app.controller.InitApplicationCommand;
import com.app.controller.InitApplicationCompleteCommand;
import com.app.controller.LoadingCompleteAnimationCompleteCommand;
import com.app.controller.NameEnteredCommand;
import com.app.controller.ProvideUserIDCommand;
import com.app.controller.SaveModelToSharedObjectCommand;
import com.app.controller.SyncUserDataCommand;
import com.app.events.ApplicationEvent;

    import com.screens.events.ScreensContextEvent;
    import com.app.model.ApplicationModel;


    import flash.events.IEventDispatcher;

    import org.swiftsuspenders.Injector;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;

    public class ApplicationConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: Injector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {
            // init
            eventCommandMap.map(ApplicationEvent.INIT_APPLICATION).toCommand(InitApplicationCommand);
            eventCommandMap.map(ScreensContextEvent.NAME_ENTERED).toCommand(NameEnteredCommand);
            eventCommandMap.map(ApplicationEvent.PROVIDE_USER_ID).toCommand(ProvideUserIDCommand);
            eventCommandMap.map(ScreensContextEvent.RECONNECT).toCommand(ProvideUserIDCommand);
            eventCommandMap.map(ScreensContextEvent.LOADING_COMPLETE_ANIMTION_COMPLETE).toCommand(LoadingCompleteAnimationCompleteCommand);
            eventCommandMap.map(ApplicationEvent.INIT_COMPLETE).toCommand(InitApplicationCompleteCommand);
            eventCommandMap.map(ApplicationEvent.SAVE_MODEL_TO_SHARED_OBJECT).toCommand(SaveModelToSharedObjectCommand);
            eventCommandMap.map(ApplicationEvent.SYNC_USER_DATA).toCommand(SyncUserDataCommand);
        }


        private function mapMediators(): void {
           // mediatorMap.map(ScreensLayerView).toMediator(ScreenLayerMediator);
        }


        private function mapInjections(): void {
            injector.map(ApplicationModel).asSingleton();
            injector.getInstance(ApplicationModel);
        }


        private function init(): void {
            dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.INIT_APPLICATION));
        }
    }
}
