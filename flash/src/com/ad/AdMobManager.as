/**
 * Created by Pasha on 09.07.2016.
 */
package com.ad {
import air.net.ServiceMonitor;
import air.net.URLMonitor;

import com.milkmangames.nativeextensions.AdMob;
import com.milkmangames.nativeextensions.AdMobAdType;
import com.milkmangames.nativeextensions.AdMobAlignment;
import com.milkmangames.nativeextensions.events.AdMobErrorEvent;
import com.milkmangames.nativeextensions.events.AdMobEvent;

import flash.desktop.NativeApplication;
import flash.events.Event;

import flash.events.HTTPStatusEvent;

import flash.events.StatusEvent;

import flash.net.URLRequest;

public class AdMobManager {

    private static var _instance:AdMobManager;

    private var _urlMonitor:URLMonitor;

    private var _isInited:Boolean;
    private var _isBusy:Boolean;
    private var _visible:Boolean;

    public function AdMobManager() {
        _isInited = false;
        _isBusy = false;
        _visible = true;

        NativeApplication.nativeApplication.addEventListener(Event.NETWORK_CHANGE,onNetworkChange);

        // TODO: change id
        AdMob.init("ca-app-pub-3702434353404962/1930063936");

        // TODO during TESTING, activate test IDs.  You want to REMOVE this before publishing your final application.
       // AdMob.enableTestDeviceIDs(AdMob.getCurrentTestDeviceIDs());
    }

    public static function get instance():AdMobManager {
        if (!_instance) {
            _instance = new AdMobManager();
        }
        return _instance;
    }

    public function get isSupported():Boolean {
        return AdMob.isSupported;
    }

    public function init():void {
        if (AdMobManager.instance.isSupported && !_isInited && !_isBusy) {

            _isBusy = true;

            AdMob.addEventListener(AdMobEvent.RECEIVED_AD,onReceiveAd);
            AdMob.addEventListener(AdMobErrorEvent.FAILED_TO_RECEIVE_AD,onFailedReceiveAd);

            AdMob.showAd(AdMobAdType.SMART_BANNER, AdMobAlignment.LEFT, AdMobAlignment.BOTTOM);
        }
    }

    public function set visible(value:Boolean):void {
        _visible = value;

        if (!_isInited) {
            init();
        } else {
            setVisibility();
        }
    }

    private function onReceiveAd(e:AdMobEvent):void
    {
        _isBusy = false;
        _isInited = true;

        setVisibility();
    }

    private function onFailedReceiveAd(e:AdMobErrorEvent):void
    {
        _isBusy = false;
    }

    private function setVisibility():void {
        // Use try to avoid error when we are disconnected from internet
        try {
            AdMob.setVisibility(_visible);
        } catch(e:Error) {

        }
    }

    //Checking for network connectivity
    protected function onNetworkChange(e:Event):void
    {
        var urlRequest:URLRequest = new URLRequest("http://www.adobe.com");
        urlRequest.method = "HEAD";

        _urlMonitor = new URLMonitor(urlRequest);
        _urlMonitor.addEventListener(StatusEvent.STATUS, onStatusHandler);
        _urlMonitor.start();
    }

    private function onStatusHandler( event : StatusEvent ) : void
    {
        if (!_isInited) {
            init();
        }

        _urlMonitor.stop();
    }
}
}
